require('dotenv').load();
const http = require('http');
const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
// const genpass = require('./server/passwords/generate-hash');

// genpass('123');

const api = require('./server/api');

const app = express();

app.DIST_DIR = path.join(__dirname, 'dist');
app.HTML_FILE = path.join(app.DIST_DIR, 'index.html');

app.use(express.static(app.DIST_DIR));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET, PUT, POST, DELETE, OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');
    next();
});
app.options('*', (req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');
    res.sendStatus(200);
});

app.get('/favicon.ico', (req, res) => res.status(200));

app.use('*', (req, res, next) => {
    console.log(`URL: ${req.url}`);
    next();
});

app.use('/api', api);

app.get('*', (req, res) => {
    res.set('content-type', 'text/html');
    res.sendFile(app.HTML_FILE);
});

app.use((req, res, next) => {
    const err = new Error('Not Found');
    err.status = 404;
    next(err);
});

app.use((err, req, res, next) => {
    res.status(err.status || 500)
        .json({
            status: 'error',
            message: err.message
        });
});

const httpServer = http.createServer(app);

httpServer.listen(process.env.PORT || 9090);
