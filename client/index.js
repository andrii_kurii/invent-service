import React from 'react';
import ReactDOM from 'react-dom';
import Routes from './main-router';

import 'bootstrap/dist/css/bootstrap.min.css';
import './root.css';


ReactDOM.render(React.createElement(Routes), document.getElementById('root'));
