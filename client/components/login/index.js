import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
    Button, Form, FormGroup, Label, Input, Alert
} from 'reactstrap';
import './login.css';

import { POST } from '../../api_temp';

export default class Login extends Component {
    state = { warning: '' }
    static propTypes = {
        history: PropTypes.shape({ push: PropTypes.func.isRequired }).isRequired,
        match: PropTypes.shape({ path: PropTypes.string.isRequired }).isRequired
    }
    constructor() {
        super();
        this.state = { login: '', password: '' };
        this.handleChangeLogin = this.handleChangeLogin.bind(this);
        this.handleChangePassword = this.handleChangePassword.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    response = (datas) => {
        const { data, message, statusCode } = datas;
        if (statusCode === 200) {
            const { role, token } = data; // TODO ! handle
            localStorage.setItem('token', token);
            if (role === 'all') {
                this.props.history.push('/user/inventorisation');
            }
        } else if (statusCode === 401) {
            this.setState({ warning: message });
        }
    }
    err = (data) => { console.log('err: ', data); }
    handleChangeLogin(event) { this.setState({ login: event.target.value }); }
    handleChangePassword(event) { this.setState({ password: event.target.value }); }
    handleSubmit(event) {
        event.preventDefault();
        const { login, password } = this.state;
        this.setState({ warning: '' });
        POST('login', { login, password }, this.response, this.err);
    }
    render() {
        const { login, password, warning } = this.state;
        return (
            <Form onSubmit={this.handleSubmit} className="login">
                <FormGroup>
                    <Label for="login">Login</Label>
                    <Input
                        type="login"
                        value={login}
                        onChange={this.handleChangeLogin}
                        name="login"
                        id="login"
                        placeholder="enter login"
                    />
                </FormGroup>
                <FormGroup>
                    <Label for="login-password">Password</Label>
                    <Input
                        type="password"
                        value={password}
                        onChange={this.handleChangePassword}
                        name="password"
                        id="login-password"
                        placeholder="enter password"
                    />
                </FormGroup>
                {warning
                    ? (
                        <Alert color="warning">
                            {warning}
                        </Alert>
                    )
                    : ''}
                <Button type="submit">Submit</Button>
            </Form>
        );
    }
}
