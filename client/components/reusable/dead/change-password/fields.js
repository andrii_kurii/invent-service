import React from 'react';
import PropTypes from 'prop-types';
import momentPropTypes from 'react-moment-proptypes';
import {
    Button, Dropdown, DropdownItem, Container, FormGroup, Label, Input,
    Row, Col, ListGroup, ListGroupItem, DropdownToggle, DropdownMenu
} from 'reactstrap';
// import './fields.css';

// import DatePicker from 'react-datepicker';
// import moment from 'moment';
import 'react-datepicker/dist/react-datepicker.css';

export default class Fields extends React.Component {
    static propTypes = {
        old_password: PropTypes.string.isRequired,
        new_password: PropTypes.string.isRequired,
        change: PropTypes.bool.isRequired,
        oldToggle: PropTypes.func.isRequired,
        newToggle: PropTypes.func.isRequired
    }

    render() {
        const { old_password, new_password, change } = this.props;
        return (
            <ListGroup className="form-fields col-8">
                {change
                    ? (
                        <div>
                            <ListGroupItem className="fields-smaller-paddings">
                                <FormGroup className="fields-clear-marbot">
                                    <Input
                                        type="password"
                                        value={old_password}
                                        onChange={this.props.oldToggle}
                                    />
                                </FormGroup>
                            </ListGroupItem>
                            <ListGroupItem className="fields-smaller-paddings">
                                <FormGroup className="fields-clear-marbot">
                                    <Input
                                        type="password"
                                        value={new_password}
                                        onChange={this.props.newToggle}
                                    />
                                </FormGroup>
                            </ListGroupItem>
                        </div>
                    )
                    : (
                        <div>
                            <ListGroupItem> </ListGroupItem>
                            <ListGroupItem> </ListGroupItem>
                        </div>
                    )
                }
            </ListGroup>
        );
    }
}
