
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
    Button, Dropdown, DropdownItem, Container, Form, Alert,
    Row, Col, ListGroup, ListGroupItem, DropdownToggle, DropdownMenu
} from 'reactstrap';
// import './current-admin.css';
import moment from 'moment';
import { Spinner } from '../../index';
import { TGET, PUT } from '../../../../api_temp';

import Fields from './fields';

export default class CurrentAdmin extends Component {
    static propTypes = {
        history: PropTypes.shape({ push: PropTypes.func.isRequired }).isRequired
    }

    constructor(props) {
        super(props);
        this.oldToggle = this.oldToggle.bind(this);
        this.newToggle = this.newToggle.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

        this.state = {
            change: false,
            new_password: '',
            old_password: '',
            warning: ''
        };
    }

    change = () => { const { change } = this.state; this.setState({ change: !change }); }

    oldToggle = (old_password) => { this.setState({ old_password: old_password.target.value }); }

    newToggle = (new_password) => { this.setState({ new_password: new_password.target.value }); }

    responseUpdatePassword = (datas) => {
        const { history } = this.props;
        const { statusCode } = datas;
        if (statusCode === 200) {
            this.change();
            this.setState({ warning: '', old_password: '', new_password: '' });
        } else if (statusCode === 403) {
            history.push('/login');
        } else if (statusCode === 405 || statusCode === 401) {
            const { message } = datas;
            this.setState({ warning: message });
        }
    }

    errUpdatePassword = (data) => {
        console.log('err', data);
    }

    handleSubmit(event) {
        event.preventDefault();
        this.setState({ warning: '' });
        const { old_password, new_password } = this.state;
        PUT('user/update-password', { old_password, new_password }, this.responseUpdatePassword, this.errUpdatePassword);
    }

    render() {
        const {
            old_password, new_password, change, warning
        } = this.state;
        return (
            <div>
                {change
                    ? (
                        <Form onSubmit={this.handleSubmit} className="current-admin">
                            <Row>
                                <Col>
                                    <ListGroup className="form-titles col-8">
                                        <ListGroupItem>Old password</ListGroupItem>
                                        <ListGroupItem>New password</ListGroupItem>
                                    </ListGroup>
                                </Col>
                                <div className="separator"></div>
                                <Col>
                                    <Fields
                                        old_password={old_password}
                                        new_password={new_password}
                                        change={change}
                                        oldToggle={this.oldToggle}
                                        newToggle={this.newToggle}
                                    />
                                </Col>
                            </Row>
                            {warning
                                ? (
                                    <Alert color="warning" className="fields-warning">
                                        {warning}
                                    </Alert>
                                )
                                : ''}
                            {change
                                ? (
                                    <div className="form-change-buttons">
                                        <Button type="submit">Submit</Button>
                                        <Button type="button" onClick={this.change}>Cancel</Button>
                                    </div>
                                )
                                : <Button type="button" className="form-change" onClick={this.change}>Change</Button>}
                        </Form>
                    )
                    : <Button type="button" style={{ margin: '30px auto auto', display: 'block' }} onClick={this.change}>Change Password</Button>
                }
            </div>
        );
    }
}
