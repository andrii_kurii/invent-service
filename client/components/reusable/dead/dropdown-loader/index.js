import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
    Dropdown, DropdownItem, DropdownToggle, DropdownMenu
} from 'reactstrap';
import { defaultFormat } from 'moment';

export default class DropdownLoader extends Component {
    static propTypes = {
        data: PropTypes.arrayOf(PropTypes.object).isRequired,
        set: PropTypes.func.isRequired,
        keys: PropTypes.string.isRequired,
        title: PropTypes.string,
        emptify: PropTypes.bool,
        defaultValue: PropTypes.oneOfType([
            PropTypes.string,
            PropTypes.number,
            PropTypes.PropTypes.shape({})
        ])
    }
    static defaultProps = {
        title: '',
        emptify: false,
        defaultValue: ''
    }
    constructor() {
        super();
        this.state = {
            dropdownOpen: false,
            selected: null
        };
    }
    componentDidMount () {
        if (this.props.defaultValue) {
            this.select(this.props.defaultValue);
        }
    }
    toggle = () => { this.setState(prevState => ({ dropdownOpen: !prevState.dropdownOpen })); }
    select = (item) => {
        const { keys } = this.props;
        this.setState({ selected: item[keys] });
        this.props.set(item.id);
    }
    renderList = () => {
        const { keys } = this.props;
        return this.props.data.map((item, i) => {
            return <DropdownItem key={i} onClick={() => this.select(item)}>{item[keys]}</DropdownItem>;
        });
    }
    render() {
        const { selected } = this.state;
        return (
            <Dropdown isOpen={this.state.dropdownOpen} toggle={this.toggle}>
                <DropdownToggle caret>
                    {selected === null ? this.props.title || 'Not selected' : selected}
                </DropdownToggle>
                <DropdownMenu>
                    {this.props.emptify && selected
                        ? <DropdownItem onClick={() => { this.select(''); this.setState({ selected: null }); }}>Clear</DropdownItem> : ''}
                    {this.renderList()}
                </DropdownMenu>
            </Dropdown>
        );
    }
}
