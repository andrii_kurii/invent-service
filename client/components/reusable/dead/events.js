function enEvents (c) {
    switch (c) {
    case 'Renewal':
        return 'renewal';
    case 'Issuance':
        return 'issuance';
    case 'returned':
        return 'returned';
    case 'Write off':
        return 'writeoff';
    default:
        return '';
    }
}

export default enEvents;
