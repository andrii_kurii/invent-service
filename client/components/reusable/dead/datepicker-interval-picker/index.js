import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import DatePicker from 'react-datepicker';
import './styles.css';

const DatepickerIntervalPicker = ({ begin_date, end_date, setBeginDate, setEndDate, title, titleEnd }) => {
    return (
        <div className="datepicker-interval-picker">
            <span>{title}</span>
            <DatePicker
                className="form-control"
                selected={begin_date}
                dateFormat="lll:ss"
                locale="en"
                showTimeSelect
                timeCaption="Time"
                timeFormat="HH:mm"
                timeIntervals={1}
                onChange={date => setBeginDate(moment(date))}
            />
            <span style={{ textAlign: 'right' }}>{titleEnd}</span>
            <DatePicker
                className="form-control"
                selected={end_date}
                dateFormat="lll:ss"
                locale="en"
                showTimeSelect
                timeCaption="Time"
                timeFormat="HH:mm"
                timeIntervals={1}
                onChange={date => setEndDate(moment(date))}
            />
        </div>
    );
};

DatepickerIntervalPicker.propTypes = {
    begin_date: PropTypes.instanceOf(moment).isRequired,
    end_date: PropTypes.instanceOf(moment).isRequired,
    setBeginDate: PropTypes.func.isRequired,
    setEndDate: PropTypes.func.isRequired,
    title: PropTypes.string,
    titleEnd: PropTypes.string
};

DatepickerIntervalPicker.defaultProps = {
    title: 'Period start',
    titleEnd: 'Period end'
};

export default DatepickerIntervalPicker;
