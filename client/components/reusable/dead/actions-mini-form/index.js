import React from 'react';
import PropTypes from 'prop-types';
import { Form, Label, Input, Button } from 'reactstrap';
import { NumbersValidationWrapper } from '../..';

const ActionsMiniForm = ({ text = '', invent, value, action, submit, type }) => {
    return (
        <Form style={{ textAlign: 'right' }} onSubmit={submit} noValidate>
            <Label style={{ marginRight: '10px' }} for="writeoff">{ invent}</Label>
            {/* <Input
                autoComplete="off"
                id="writeoff"
                style={{ width: '100px', display: 'inline-block' }}
                type="text"
                value={value}
                onChange={action}
            /> */}
            <div style={{ display: 'inline-block' }}>
                <NumbersValidationWrapper type={type} func={rr => action(rr)} />
            </div>
            <Button style={{ margin: '0 20px', width: '135px', marginBottom: '5px' }}> {text} </Button>
        </Form>
    );
};

ActionsMiniForm.propTypes = {
    text: PropTypes.string.isRequired,
    invent: PropTypes.string.isRequired,
    value: PropTypes.string.isRequired,
    action: PropTypes.func.isRequired,
    submit: PropTypes.func.isRequired,
    type: PropTypes.string.isRequired

};

export default ActionsMiniForm;
