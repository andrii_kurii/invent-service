import React from 'react';
import PropTypes from 'prop-types';
import ValidateNumber from './validation';

const NumbersValidationWrapper = ({ type, func, value }) => {
    switch (type) {
    case ('Price'):
        return (
            <ValidateNumber
                len={3}
                tail={2}
                type="text"
                value={value}
                className="currency"
                onChange={(e, p, c) => func(p)}
                required
            />
        );
    case ('Quantity'):
        return (
            <ValidateNumber
                len={1}
                tail={0}
                value={value}
                type="text"
                className="currency"
                onChange={(e, p, c) => func(p)}
                required
            />
        );
    case ('Meters'):
        return (
            <ValidateNumber
                len={2}
                tail={1}
                type="text"
                value={value}
                className="currency"
                onChange={(e, p, c) => func(p)}
                required
            />
        );
    case ('Kilograms'):
        return (
            <ValidateNumber
                len={4}
                tail={3}
                type="text"
                value={value}
                className="currency"
                onChange={(e, p, c) => func(p)}
                required
            />
        );
    case ('Tons'):
        return (
            <ValidateNumber
                len={4}
                tail={3}
                type="text"
                value={value}
                className="currency"
                onChange={(e, p, c) => func(p)}
                required
            />
        );
    case ('Litrs'):
        return (
            <ValidateNumber
                len={4}
                tail={3}
                type="text"
                value={value}
                className="currency"
                onChange={(e, p, c) => func(p)}
                required
            />
        );
    case ('Cubic decimeters'):
        return (
            <ValidateNumber
                len={1}
                tail={0}
                type="text"
                value={value}
                className="currency"
                onChange={(e, p, c) => func(p)}
                required
            />
        );
    case ('Cubic meters'):
        return (
            <ValidateNumber
                len={4}
                tail={3}
                type="text"
                value={value}
                className="currency"
                onChange={(e, p, c) => func(p)}
                required
            />
        );
    default:
        return (
            <ValidateNumber
                len={1}
                tail={0}
                type="text"
                value={value}
                className="currency"
                onChange={(e, p, c) => func(p)}
                required
            />
        );
    }
};

NumbersValidationWrapper.propTypes = {
    type: PropTypes.string.isRequired,
    func: PropTypes.func.isRequired,
    value: PropTypes.oneOfType([
        PropTypes.string.isRequired,
        PropTypes.number.isRequired
    ]).isRequired
};
NumbersValidationWrapper.defaultProps = {
    value: ''
};

export default NumbersValidationWrapper;
