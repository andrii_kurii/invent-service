
function arrayForSelect(data, value, label) {
    let a = {};
    a = data.map((item) => {
        a = {
            value: item[value], label: item[label], ...item
        };
        return a;
    });
    return a;
}

export default arrayForSelect;
