import React from 'react';
import PropTypes, { object } from 'prop-types';
import { Form, Label, Button } from 'reactstrap';
import { Spinner, NumbersValidationWrapper, SelectLoader } from '../..';

const ActionsLoaderMiniForm = ({
    invent, text = '', workers = '', setItem, action, submit, type
}) => {
    return (
        <Form style={{ textAlign: 'right' }} onSubmit={submit} noValidate>
            <Label style={{ marginRight: '10px' }}>{ invent}</Label>
            <div style={{ display: 'inline-block' }}>
                <NumbersValidationWrapper type={type} func={rr => action(rr)} />
            </div>
            { workers
                ? (
                    <div style={{ display: 'inline-block', margin: '0 0 0 20px' }}>
                        <SelectLoader
                            set={id => setItem(id)}
                            data={workers}
                        />
                    </div>
                )
                : <Spinner />
            }
            <Button style={{ margin: '0 20px', width: '135px', marginBottom: '5px' }}> {text} </Button>
        </Form>
    );
};

ActionsLoaderMiniForm.propTypes = {
    text: PropTypes.string.isRequired,
    invent: PropTypes.string.isRequired,
    workers: PropTypes.arrayOf(object).isRequired,
    setItem: PropTypes.func.isRequired,
    action: PropTypes.func.isRequired,
    submit: PropTypes.func.isRequired,
    type: PropTypes.string.isRequired
};

export default ActionsLoaderMiniForm;
