import React from 'react';
import PropTypes from 'prop-types';
import { Pagination, PaginationItem, PaginationLink } from 'reactstrap';

function renderPagination({ page, end, getPage }) {
    let a = [];
    for (let i = 0; i <= end - 1; i += 1) {
        a[i] = i + 1;
    }
    return a.map((item, i) => {
        return (
            <div key={i}>
                {end > 1
                    ? (
                        <PaginationItem onClick={() => getPage(item)}>
                            <PaginationLink style={item === page ? { color: 'black' } : {}}>
                                {item}
                            </PaginationLink>
                        </PaginationItem>
                    )
                    : ''}
            </div>
        );
    });
}

const MyPagination = ({ end, page, getPage }) => {
    return (
        <Pagination aria-label="Page navigation example">
            {page > 1
                ? (
                    <PaginationItem>
                        <PaginationLink previous onClick={() => getPage(page - 1)} />
                    </PaginationItem>
                ) : ''}
            {renderPagination({ page, end, getPage })}
            {page < end
                ? (
                    <PaginationItem id="lol">
                        <PaginationLink next onClick={() => getPage(page + 1)} />
                    </PaginationItem>
                ) : ''}
        </Pagination>
    );
};

MyPagination.propTypes = {
    page: PropTypes.number.isRequired,
    end: PropTypes.number.isRequired,
    getPage: PropTypes.func.isRequired
};

export default MyPagination;
