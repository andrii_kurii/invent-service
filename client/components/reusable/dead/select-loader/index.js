import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Select from 'react-select';
import {
    Dropdown, DropdownItem, DropdownToggle, DropdownMenu
} from 'reactstrap';

export default class SelectLoader extends Component {
    static propTypes = {
        data: PropTypes.arrayOf(PropTypes.object).isRequired,
        set: PropTypes.func.isRequired
        // keys: PropTypes.string.isRequired,
        // title: PropTypes.string,
        // emptify: PropTypes.bool
    }
    // static defaultProps = {
    //     title: '',
    //     emptify: false
    // }
    constructor() { super(); this.state = { selectedWorker: '' }; }
    // toggle = () => { this.setState(prevState => ({ dropdownOpen: !prevState.dropdownOpen })); }
    // select = (item) => {
    //     const { keys } = this.props;
    //     this.setState({ selected: item[keys] });
    //     this.props.set(item.id);
    // }
    // renderList = () => {
    //     const { keys } = this.props;
    //     return this.props.data.map((item, i) => {
    //         return <DropdownItem key={i} onClick={() => this.select(item)}>{item[keys]}</DropdownItem>;
    //     });
    // }
    // render() {
    //     const { selected } = this.state;
    //     return (
    //         <Dropdown isOpen={this.state.dropdownOpen} toggle={this.toggle}>
    //             <DropdownToggle caret>
    //                 {selected === null ? this.props.title || 'Не обрано' : selected}
    //             </DropdownToggle>
    //             <DropdownMenu>
    //                 {this.props.emptify && selected
    //                     ? <DropdownItem onClick={() => { this.select(''); this.setState({ selected: null }); }}>Очистити</DropdownItem> : ''}
    //                 {this.renderList()}
    //             </DropdownMenu>
    //         </Dropdown>
    //     );
    // }
    // handle = (item) => {
    //     this.setState({ selectedWorker: item.id }, () => this.props.set(item.id));
    // }
    
    handle = (item) => {
        if (item !== null) {
            this.props.set(item.id);
        } else {
            this.props.set(null);
        }
    }
    render() {
        let list = {};
        list = this.props.data.map((item) => { list = { value: item.id, label: item.name, ...item }; return list; });    
        return (
            <Select
                // value={this.state.selectedWorker}
                onChange={this.handle}
                options={list}
                placeholder="Choose worker"
                className="multi multi-large"
                isClearable
            />
        );
    }
}
