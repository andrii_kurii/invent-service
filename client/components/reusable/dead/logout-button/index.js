import React from 'react';
import PropTypes from 'prop-types';
import { Button } from 'reactstrap';
import './logout-button.css';

export default class LogoutButton extends React.Component {
    static propTypes = {
        logout: PropTypes.func.isRequired
    }

    render() {
        return (
            <Button className="logout" onClick={() => this.props.logout()}>Exit</Button>
        );
    }
}
