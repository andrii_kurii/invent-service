import React from 'react';
import PropTypes from 'prop-types';
import { Button, Table } from 'reactstrap';
import moment from 'moment';
import { TGET, } from '../../../../api_temp';
import { Spinner, MyPagination, enEvents } from '../..';

export default class VehicleActionsView extends React.Component {
    static propTypes = {
        history: PropTypes.shape({ push: PropTypes.func.isRequired }).isRequired,
        itemFilter: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
        eventsFilter: PropTypes.oneOfType([
            PropTypes.string.isRequired,
            PropTypes.arrayOf(PropTypes.shape({}).isRequired).isRequired
        ]),
        fuelFilter: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
        workerFilter: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
        userFilter: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
        beginDate: PropTypes.instanceOf(moment),
        endDate: PropTypes.instanceOf(moment),
        refresh: PropTypes.bool,
        reportable: PropTypes.bool,
        url: PropTypes.string
    }
    static defaultProps = {
        itemFilter: '',
        eventsFilter: [],
        fuelFilter: '',
        workerFilter: '',
        userFilter: '',
        beginDate: moment().startOf('month'),
        endDate: moment().endOf('month'),
        refresh: false,
        reportable: false,
        url: ''
    }
    constructor(props) {
        super(props);
        this.state = {
            actions: '',
            page: 1,
            limit: 10,
            end: '',
            renewal: 0,
            issuance: 0,
            returned: 0,
            writeoff: 0
        };
    }
    componentDidMount() {
        this.init();
    }
    init = () => {
        this.setState({ actions: '' });
        TGET(
            `user/vehicles-actions${this.urlParser()}`,
            this.responseInventActions,
            this.errInventActions
        );
    }    
    sum = () => {      
        const { actions } = this.state;  
        let renewal = 0;
        let issuance = 0;
        let returned = 0;
        let writeoff = 0;
        actions.map((item, i) => {
            if (item.action_name === 'Renewal') { renewal += item.value}
            if (item.action_name === 'Issuance') { issuance += item.value}
            if (item.action_name === 'Returned') { returned += item.value}
            if (item.action_name === 'Write off') { writeoff += item.value}
            return null;
        });
        this.setState({ renewal, issuance, returned, writeoff });
    }
    responseInventActions = (datas) => {
        const { statusCode } = datas;
        if (statusCode === 200) {
            const { end, data } = datas;
            this.setState({ actions: data, end: parseInt(end, 10) }, () => this.sum());
        }
    }
    errInventActions = (data) => {
        console.log('err:', data);
    }
    renderActions = () => {
        const { actions, page } = this.state;
        if (actions) {
            return actions.map((item, i) => {
                const { id, mark, numbers, fuel, action_name, value, worker_name, created_at, creator_name } = item;
                return (
                    <tr key={i}>
                        <td>{ (page - 1) * 10 + i + 1}</td>
                        <td>{mark}</td>
                        <td>{numbers}</td>
                        <td>{fuel}</td>
                        <td>{action_name}</td>
                        <td>{value}</td>
                        <td>{worker_name || '—'}</td>
                        <td>{creator_name}</td>
                        <td>{moment(created_at).format('lll:ss')}</td>
                        <td className="pen" onClick={() => this.props.history.push(`/user/vehicles/events/${id}`)}> &#x270E; </td>
                    </tr>
                );
            });
        }
        return null;
    }
    componentDidUpdate = (prevProps) => {
        if (prevProps.itemFilter !== this.props.itemFilter) {
            this.init();
        }
        if (prevProps.eventsFilter.length !== this.props.eventsFilter.length) {
            this.init();
        }
        if (prevProps.fuelFilter !== this.props.fuelFilter) {
            this.init();
        }
        if (prevProps.workerFilter !== this.props.workerFilter) {
            this.init();
        }
        if (prevProps.userFilter !== this.props.userFilter) {
            this.init();
        }
        if (prevProps.beginDate !== this.props.beginDate) {
            this.init();
        }
        if (prevProps.endDate !== this.props.endDate) {
            this.init();
        }
        if (prevProps.refresh !== this.props.refresh) {
            this.init();
        }
        return true;
    }
    getPage = (page) => {
        const { end } = this.state;
        if (page > 0 && page <= end) {
            this.setState({ page }, () => this.init());
        }
    }
    urlParser = () => {
        if (this.props.url) {
            return this.props.url;
        }
        const { itemFilter, eventsFilter, fuelFilter, workerFilter, userFilter, beginDate, endDate } = this.props;
        const { page, limit } = this.state;
        let conds = '';
        if (eventsFilter) {
            this.props.eventsFilter.map((item) => {
                conds = `${conds}&${enEvents(item.name)}=true`;
                return null;
            });
        }
        return (
            `?${
                itemFilter
                    ? `&vehicle_id=${itemFilter}`
                    : ''}${
                eventsFilter
                    ? conds
                    : ''}${
                fuelFilter
                    ? `&fuel_id=${fuelFilter}`
                    : ''}${
                workerFilter
                    ? `&worker_id=${workerFilter}`
                    : ''}${
                userFilter
                    ? `&created_by=${userFilter}`
                    : ''}${
                page
                    ? `&page=${page}`
                    : ''}${
                limit
                    ? `&limit=${limit}`
                    : ''}${
                beginDate
                    ? `&begin_date=${beginDate.format('YYYY-MM-DDTHH:mm:ss')}`
                    : ''}${
                endDate
                    ? `&end_date=${endDate.format('YYYY-MM-DDTHH:mm:ss')}`
                    : ''}`
        );
    }
    report = () => {
        const p = this.state.page;
        const l = this.state.limit;
        this.setState({ page: 1, limit: 10000 }, () => {
            window.open(`/vehicles-report${this.urlParser()}`, '_blank');
            this.setState({ page: p, limit: l });
        });
    }
    render() {
        const { actions } = this.state;
        return (
            actions
                ? (
                    actions.length
                        ? (
                            <div>
                                { this.props.reportable ? (
                                    <Button
                                        style={{ margin: '0 20px 20px auto', display: 'block' }}
                                        // onClick={() => this.props.history.push('/inventorisation-report')}
                                        onClick={() => this.report()}
                                    > Generate a report
                                    </Button>
                                ) : ''}
                                {
                                    this.props.url ? (
                                        <div>
                                            <span style={{ width: '150px', display: 'inline-block' }}>Summary: </span>
                                            {this.state.renewal ? `Renewal: ${this.state.renewal}, ` : ''}
                                            {this.state.issuance ? `Issuance: ${this.state.issuance}, ` : ''}
                                            {this.state.returned ? `Returned: ${this.state.returned}, ` : ''}
                                            {this.state.writeoff ? `Write off: ${this.state.writeoff}, ` : ''}
                                        </div>
                                    ) : ''
                                }
                                <Table className="table-editor-helper">
                                    <thead>
                                        <tr>
                                            <th> </th>
                                            <th>Mark</th>
                                            <th>Number</th>
                                            <th>Fuel</th>
                                            <th>Action</th>
                                            <th>Dimension</th>
                                            <th>Worker</th>
                                            <th>Created by</th>
                                            <th>Created at</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {this.renderActions()}
                                    </tbody>
                                </Table>
                                {this.state.end !== '1'
                                    ? (
                                        <MyPagination
                                            end={this.state.end}
                                            page={this.state.page}
                                            getPage={this.getPage}
                                        />) : '' }
                            </div>)
                        : <h3 style={{ textAlign: 'center', marginTop: '30px' }}>No Actions</h3>
                )
                : <Spinner />
        );
    }
}
