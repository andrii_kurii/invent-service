import React from 'react';
import PropTypes from 'prop-types';
import { Button, Table } from 'reactstrap';
import moment from 'moment';
import { TGET, } from '../../../../api_temp';
import { Spinner, MyPagination } from '../..';

export default class ActionsView extends React.Component {
    static propTypes = {
        history: PropTypes.shape({ push: PropTypes.func.isRequired }).isRequired,
        match: PropTypes.shape({}).isRequired,
        selectedOption: PropTypes.arrayOf(PropTypes.object).isRequired,
        worker_filter: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired,
        user_filter: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
        begin_date: PropTypes.instanceOf(moment).isRequired,
        end_date: PropTypes.instanceOf(moment).isRequired,
        refresh: PropTypes.bool.isRequired,
        showName: PropTypes.bool,
        showInventName: PropTypes.bool,
        id: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
        reportable: PropTypes.bool,
        url: PropTypes.string
    }
    static defaultProps = {
        showName: false,
        showInventName: false,
        id: '',
        reportable: false,
        url: '',
        user_filter: ''
    }
    constructor(props) {
        super(props);
        this.state = {
            actions: '',
            page: 1,
            limit: 10,
            end: '',
            renewal: 0,
            issuance: 0,
            returned: 0,
            writeoff: 0
        };
    }
    componentDidMount() {
        this.init();
    }
    init = () => {
        this.setState({ actions: '' });
        TGET(
            `user/invent-actions${this.urlParser()}`,
            this.responseInventActions,
            this.errInventActions
        );
    }
    urlParser = () => {
        if (this.props.url) {
            return this.props.url;
        }
        const { worker_filter, user_filter, begin_date, end_date, id } = this.props;
        const { page, limit } = this.state;
        let conds = '';
        this.props.selectedOption.map((item) => {
            conds = `${conds}&${item.name}=true`;
            return null;
        });
        return (
            `?${id ? `&id=${id}` : ''}${conds}${
                page
                    ? `&page=${page}`
                    : ''}${
                worker_filter
                    ? `&worker_id=${worker_filter}`
                    : ''}${
                user_filter
                    ? `&created_by=${user_filter}`
                    : ''}${
                limit
                    ? `&limit=${limit}`
                    : ''}${
                begin_date
                    ? `&begin_date=${begin_date.format('YYYY-MM-DDTHH:mm:ss')}`
                    : ''}${
                end_date
                    ? `&end_date=${end_date.format('YYYY-MM-DDTHH:mm:ss')}`
                    : ''}`
        );
    }
    responseInventActions = (datas) => {
        const { statusCode } = datas;
        if (statusCode === 200) {
            const { end, actions } = datas;
            this.setState({ actions, end: parseInt(end, 10) }, () => this.sum());
        }
    }
    sum = () => {
        const { actions } = this.state;
        let renewal = 0;
        let issuance = 0;
        let returned = 0;
        let writeoff = 0;
        actions.map((item, i) => {
            if (item.action_name === 'Renewal') { renewal += item.value}
            if (item.action_name === 'Issuance') { issuance += item.value}
            if (item.action_name === 'Returned') { returned += item.value}
            if (item.action_name === 'Write off') { writeoff += item.value}
            return null;
        });
        this.setState({ renewal, issuance, returned, writeoff });
    }
    errInventActions = (data) => {
        console.log('err:', data);
    }
    renderInventAction = () => {
        const { actions, page } = this.state;
        const { showName, showInventName } = this.props;
        if (actions) {
            return actions.map((item, i) => {
                const { id, name, value, outer_id, invent_name, action_name, worker_name, created_at, creator_name } = item;
                return (
                    <tr key={i}>
                        <td>{ (page - 1) * 10 + i + 1}</td>
                        <td>{action_name}</td>
                        {showName ? <td>{name}</td> : null}
                        <td>{value}</td>
                        {showInventName ? <td>{invent_name}</td> : null}
                        <td>{worker_name || '—' }</td>
                        <td>{moment(created_at).format('lll:ss')}</td>
                        <td>{creator_name}</td>
                        <td className="pen" onClick={() => this.props.history.push(`/user/inventorisation/events/${id}`)}> &#x270E; </td>
                    </tr>
                );
            });
        }
        return null;
    }
    componentDidUpdate = (prevProps) => {
        if (prevProps.selectedOption.length !== this.props.selectedOption.length) {
            this.init();
        }
        if (prevProps.worker_filter !== this.props.worker_filter) {
            this.init();
        }
        if (prevProps.user_filter !== this.props.user_filter) {
            this.init();
        }
        if (prevProps.begin_date !== this.props.begin_date) {
            this.init();
        }
        if (prevProps.end_date !== this.props.end_date) {
            this.init();
        }
        if (prevProps.refresh !== this.props.refresh) {
            this.init();
        }
        return true;
    }
    getPage = (page) => {
        const { end } = this.state;
        if (page > 0 && page <= end) {
            this.setState({ page }, () => this.init());
        }
    }
    report = () => {
        const p = this.state.page;
        const l = this.state.limit;
        this.setState({ page: 1, limit: 10000 }, () => {
            window.open(`/inventorisation-report${this.urlParser()}`, '_blank');
            this.setState({ page: p, limit: l });
        });
    }
    render() {
        const { actions } = this.state;
        const { showName, showInventName } = this.props;
        return (
            actions
                ? actions.length
                    ? (
                        <div>
                            { this.props.reportable ? (
                                <Button
                                    style={{ margin: '0 20px 20px auto', display: 'block' }}
                                    // onClick={() => this.props.history.push('/inventorisation-report')}
                                    onClick={() => this.report()}
                                > Generate a report
                                </Button>
                            ) : ''}
                            {
                                this.props.url ? (
                                    <div>
                                        <span style={{ width: '150px', display: 'inline-block' }}>Summary: </span>
                                        {this.state.renewal ? `Renewal: ${this.state.renewal}, ` : ''}
                                        {this.state.issuance ? `Issuace: ${this.state.issuance}, ` : ''}
                                        {this.state.returned ? `Returned: ${this.state.returned}, ` : ''}
                                        {this.state.writeoff ? `Write off: ${this.state.writeoff}, ` : ''}
                                    </div>
                                ) : ''
                            }
                            <Table className="table-editor-helper">
                                <thead>
                                    <tr>
                                        <th> </th>
                                        <th>Action</th>
                                        {showName ? <th>Name</th> : null}
                                        <th>dimension</th>
                                        {showInventName ? <th>Measurement</th> : null}
                                        <th>Worker</th>
                                        <th>Created at</th>
                                        <th>Created by</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {this.renderInventAction()}
                                </tbody>
                            </Table>
                            {this.state.end > '1'
                                ? (
                                    <MyPagination
                                        end={this.state.end}
                                        page={this.state.page}
                                        getPage={this.getPage}
                                    />) : '' }
                        </div>
                    )
                    : <h4 style={{ textAlign: 'center', fontWeight: 900, marginTop: '20px' }}>No actions yet</h4>
                : <Spinner />
        );
    }
}
