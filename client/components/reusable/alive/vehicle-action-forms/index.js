import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Button, Table, Form, Input, Label } from 'reactstrap';
import moment from 'moment';
import DatePicker from 'react-datepicker';
import Select from 'react-select';
import { TGET, PUT } from '../../../../api_temp';
import { ActionForm } from '../..';

class VehicleActionInfo extends React.Component {
    static propTypes = {
        id: PropTypes.string.isRequired,
        refresh: PropTypes.func.isRequired,
        history: PropTypes.shape({ push: PropTypes.func.isRequired }).isRequired,
        match: PropTypes.shape({}).isRequired,
        vehicles: PropTypes.oneOfType([
            PropTypes.string.isRequired,
            PropTypes.arrayOf(PropTypes.shape({}).isRequired).isRequired
        ]).isRequired
    }
    renderInventorisation = () => {
        const { vehicles, id } = this.props;
        if (Array.isArray(vehicles)) {
            const el = vehicles.filter((item) => { return item.id == id; });
            if (el.length) {
                return el[0];
            }
        }
        return {};
    }
    renderInventorisationBody = () => {
        const el = this.renderInventorisation();
        if (el) {
            const { name, invent, created_at, creator_name } = el;
            return (
                <tr>
                    <td>{name}</td>
                    <td>{invent}</td>
                    <td>{moment(created_at).format('lll:ss')}</td>
                    <td>{creator_name}</td>
                    <td className="pen" onClick={() => this.props.history.push(`${this.props.id}/edit`)}> &#x270E; </td>
                </tr>
            );
        }
        return null;
    }
    render() {
        const { vehicles } = this.props;
        return (
            <div>
                { Array.isArray(vehicles)
                    ? (
                        <Table className="table-editor-helper">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Measurement method</th>
                                    <th>Created at</th>
                                    <th>Created by</th>
                                </tr>
                            </thead>
                            <tbody>
                                {this.renderInventorisationBody()}
                            </tbody>
                        </Table>) : ''
                }
                { Array.isArray(vehicles)
                    ? (
                        <ActionForm
                            refresh={this.props.refresh}
                            url="user/inventorisation"
                            id={this.props.id}
                            keys="inventorisation_id"
                            type={this.renderInventorisation().invent}
                            element={this.renderInventorisation()}
                        />) : ''
                }
            </div>
        );
    }
}

const mapStateToProps = state => ({
    vehicles: state.inventorisation,
    invents: state.invents,
    actionTypes: state.actionTypes,
    users: state.users,
    workers: state.workers
});

export default connect(mapStateToProps)(VehicleActionInfo);
