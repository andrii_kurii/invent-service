import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Button } from 'reactstrap';
import { TGET, PUT } from '../../../../api_temp';
import {
    Spinner, ActionsMiniForm, ActionsLoaderMiniForm
} from '../..';

class ActionForm extends React.Component {
    static propTypes = {
        id: PropTypes.string.isRequired,
        refresh: PropTypes.func.isRequired,
        url: PropTypes.string.isRequired,
        keys: PropTypes.string.isRequired,
        type: PropTypes.string.isRequired,
        workers: PropTypes.oneOfType([
            PropTypes.string.isRequired,
            PropTypes.arrayOf(PropTypes.shape({}).isRequired).isRequired
        ]).isRequired,
        element: PropTypes.shape({}).isRequired
    }
    constructor(props) {
        super(props);
        this.state = {
            former: '',
            renewal: '',
            issuance: '',
            returned: '',
            writeoff: '',
            worker_id: '',
            worker_issuance_id: ''
        };
        this.renewalHandler = this.renewalHandler.bind(this);
        this.renewalReturned = this.renewalReturned.bind(this);
        this.renewalIssuance = this.renewalIssuance.bind(this);
        this.renewalWriteoff = this.renewalWriteoff.bind(this);
    }
    renewalHandler = (renewal) => { this.setState({ renewal }); }
    renewalReturned = (returned) => { this.setState({ returned }); }
    renewalIssuance = (issuance) => { this.setState({ issuance }); }
    renewalWriteoff = (writeoff) => { this.setState({ writeoff }); }
    submitRenewal = (e) => {
        e.preventDefault();
        const key = this.props.keys;
        const { renewal } = this.state;
        console.log(renewal);
        if (renewal) {
            PUT(
                `${this.props.url}/${this.props.id}/renewal`,
                { [key]: this.props.id, renewal },
                this.responseUpdates, this.errUpdates
            );
        }
    }
    submitReturned = (e) => {
        e.preventDefault();
        const key = this.props.keys;
        const { returned, worker_id } = this.state;
        if (returned && worker_id) {
            PUT(
                `${this.props.url}/${this.props.id}/returned`,
                { [key]: this.props.id, returned, worker_id },
                this.responseUpdates,
                this.errUpdates
            );
        }
    }
    submitIssuance = (e) => {
        e.preventDefault();
        const key = this.props.keys;
        const { issuance, worker_issuance_id } = this.state;
        if (issuance && worker_issuance_id) {
            PUT(
                `${this.props.url}/${this.props.id}/issuance`,
                { [key]: this.props.id, issuance, worker_id: worker_issuance_id },
                this.responseUpdates,
                this.errUpdates
            );
        }
    }
    submitWriteoff = (e) => {
        e.preventDefault();
        const key = this.props.keys;
        const { writeoff } = this.state;
        if (writeoff) {
            PUT(
                `${this.props.url}/${this.props.id}/write-off`,
                { [key]: this.props.id, writeoff },
                this.responseUpdates,
                this.errUpdates
            );
        }
    }
    responseUpdates = (data) => {
        const { statusCode } = data;
        if (statusCode === 200) {
            this.setState({
                worker_id: '',
                worker_issuance_id: '',
                renewal: '',
                issuance: '',
                returned: '',
                writeoff: '',
                former: ''
            });
            this.props.refresh();
        }
    }
    errUpdates = (data) => {
        console.log('err:', data);
    }
    render() {
        const { former, renewal, returned, writeoff, issuance } = this.state;
        const { element, workers } = this.props;
        return (
            <div>
                { element
                    ? (
                        <div>
                            <Button
                                style={{ margin: '0 20px 10px auto', width: '135px' }}
                                className={former !== 'renewal' ? 'mini-form-low-opacity' : 'mini-form-hight-opacity'}
                                onClick={() => this.setState({ former: former && former === 'renewal' ? '' : 'renewal' })}
                            > Renewal
                            </Button>
                            <Button
                                style={{ margin: '0 20px 10px auto', width: '135px' }}
                                className={former !== 'returned' ? 'mini-form-low-opacity' : 'mini-form-hight-opacity'}
                                onClick={() => this.setState({ former: former && former === 'returned' ? '' : 'returned' })}
                            > Return
                            </Button>
                            <Button
                                style={{ margin: '0 20px 10px auto', width: '135px' }}
                                className={former !== 'issuance' ? 'mini-form-low-opacity' : 'mini-form-hight-opacity'}
                                onClick={() => this.setState({ former: former && former === 'issuance' ? '' : 'issuance' })}
                            > Issuance
                            </Button>
                            <Button
                                style={{ margin: '0 20px 10px auto', width: '135px' }}
                                className={former !== 'writeoff' ? 'mini-form-low-opacity' : 'mini-form-hight-opacity'}
                                onClick={() => this.setState({ former: former && former === 'writeoff' ? '' : 'writeoff' })}
                            >Write off
                            </Button>
                            {former === 'writeoff'
                                ? (
                                    <ActionsMiniForm
                                        invent={element.invent}
                                        value={writeoff}
                                        action={this.renewalWriteoff}
                                        submit={this.submitWriteoff}
                                        text="Write off"
                                        type={this.props.type}
                                    />)
                                : ''
                            }
                            {former === 'issuance'
                                ? (
                                    <ActionsLoaderMiniForm
                                        title="Choose worker"
                                        text="Issue"
                                        label_keys="name"
                                        value={issuance}
                                        invent={element.invent}
                                        workers={workers}
                                        action={this.renewalIssuance}
                                        setItem={id => this.setState({ worker_issuance_id: id })}
                                        submit={this.submitIssuance}
                                        type={this.props.type}
                                    />) : ''
                            }
                            {former === 'returned'
                                ? (
                                    <ActionsLoaderMiniForm
                                        title="Choose worker"
                                        text="Return"
                                        label_keys="name"
                                        value={returned}
                                        invent={element.invent}
                                        workers={workers}
                                        action={this.renewalReturned}
                                        setItem={id => this.setState({ worker_id: id })}
                                        submit={this.submitReturned}
                                        type={this.props.type}
                                    />) : ''
                            }
                            {former === 'renewal'
                                ? (
                                    <ActionsMiniForm
                                        invent={element.invent}
                                        value={renewal}
                                        action={this.renewalHandler}
                                        submit={this.submitRenewal}
                                        text="Renewal"
                                        type={this.props.type}
                                    />) : ''
                            }
                        </div>
                    )
                    : <Spinner />
                }
            </div>
        );
    }
}

const mapStateToProps = state => ({
    workers: state.workers
});

export default connect(mapStateToProps)(ActionForm);
