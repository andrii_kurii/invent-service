import parseForSelect from './dead/array-for-select';
import enEvents from './dead/events';
import ValidateNumber from './dead/validation';
import NumbersValidationWrapper from './dead/numbers-valid-wrapper';

import LogoutButton from './dead/logout-button';
import Spinner from './dead/spinner';
import ChangePassword from './dead/change-password';
import DropdownLoader from './dead/dropdown-loader';
import SelectLoader from './dead/select-loader';
import DatepickerIntervalPicker from './dead/datepicker-interval-picker';
import ActionsMiniForm from './dead/actions-mini-form';
import ActionsLoaderMiniForm from './dead/actions-loader-mini-form';
import MyPagination from './dead/pagination';

import ActionsView from './alive/actions-view';
import InventoryActionInfo from './alive/inventory-action-forms';
import VehiclesActionsView from './alive/vehicle-actions-view';
import ActionForm from './alive/action-forms';

export {
    parseForSelect,
    enEvents,
    ValidateNumber,
    NumbersValidationWrapper,

    Spinner,
    LogoutButton,
    ChangePassword,
    DropdownLoader,
    SelectLoader,
    DatepickerIntervalPicker,
    ActionsMiniForm,
    ActionsLoaderMiniForm,
    MyPagination,

    ActionsView,
    VehiclesActionsView,
    InventoryActionInfo,
    ActionForm
};
