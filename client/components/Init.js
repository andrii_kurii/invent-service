import React from 'react';
// import { connect } from 'react-redux';
import PropTypes from 'prop-types';
// import { inventorisationFetch } from '../redux/actions/with-request/fetch-inventorisation';
// import { inventsFetch } from '../redux/actions/with-request/fetch-invents';
import { fetch } from '../redux/actions/with-request/fetch-inventorisation';
// import { actionTypesFetch } from '../redux/actions/with-request/fetch-action-types';
// import { usersFetch } from '../redux/actions/with-request/fetch-users';
// import { workersFetch } from '../redux/actions/with-request/fetch-workers';
// import { counterpartiesFetch } from '../redux/actions/with-request/fetch-counterparties';
// import { fuelFetch } from '../redux/actions/with-request/fetch-fuel';
// import { vehiclesFetch } from '../redux/actions/with-request/fetch-vehicles';
// import wrapper from '../redux/actions/wrappeR';

export default class Init extends React.Component {
    static propTypes = {
        children: PropTypes.node.isRequired
    }
    componentDidMount() {
        fetch('inventorisation', 'get', 'user/inventorisation');
        fetch('invents', 'get', 'user/invents');
        fetch('action_Types', 'get', 'user/action-types');
        fetch('purchases', 'get', 'user/purchases');
        fetch('workers', 'get', 'user/workers');
        fetch('counterparties', 'get', 'user/counterparties');
        fetch('fuel', 'get', 'user/fuel');
        fetch('vehicles', 'get', 'user/vehicles');
    }
    render() {
        return (
            <div>{this.props.children}</div>
        );
    }
}

// const mapDispatchToProps = dispatch => ({
//     inventoryFetch: () => { dispatch({ type: 'FETCH_INVENTORY' }); }
// });

// export default connect(null, mapDispatchToProps)(Init);
