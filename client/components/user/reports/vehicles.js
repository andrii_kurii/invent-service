import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import queryString from 'query-string';

import {
    Button, Breadcrumb, BreadcrumbItem, Table, InputGroupAddon, InputGroupText, Input, FormGroup, Label,
    Dropdown, DropdownItem, Container, Form, Alert,
    Row, Col, ListGroup, ListGroupItem, DropdownToggle, DropdownMenu
} from 'reactstrap';
import { VehiclesActionsView, Spinner } from '../../reusable';
import { TGET, TPOST } from '../../../api_temp';

export default class Report extends React.Component {
    static propTypes = {
        history: PropTypes.shape({ push: PropTypes.func.isRequired }).isRequired,
        match: PropTypes.shape({ path: PropTypes.string.isRequired }).isRequired,
        location: PropTypes.shape({ search: PropTypes.string.isRequired }).isRequired
    }
    constructor() {
        super();
        this.state = { queries: '', worker: '', creator: '', name: '', vehicle: '', fuel: '' };
    }
    componentDidMount() {
        const queries = this.props.location.search;
        this.setState({ queries });
        const { id, worker_id, created_by, vehicle_id, fuel_id } = queryString.parse(queries);
        if (worker_id) {
            TGET(`user/workers?id=${worker_id}`, this.responseWorker, this.err);
        }
        if (id) {
            TGET(`user/inventorisation?&id=${id}`, this.responseName, this.err);
        }
        if (created_by) {
            TGET('user/users', data => this.responseCreator(data, created_by), this.err);
        }
        if (vehicle_id) {
            TGET(`user/vehicles?vehicle_id=${vehicle_id}`, this.responseVehicle, this.err);
        }
        if (fuel_id) {
            TGET('user/fuel', datas => this.responseFuel(datas, fuel_id), this.err);
        }
    }
    responseVehicle = (datas) => {
        const { statusCode, data } = datas;
        if (statusCode === 200) {
            this.setState({ vehicle: `${data[0].numbers}, ${data[0].mark}, Ліміт пального: ${data[0].fuel_limit || '—'}` });
        }
    }
    responseFuel = (datas, fuel_id) => {
        const { statusCode, data } = datas;
        if (statusCode === 200) {
            let i = '';
            data.map((item) => {
                if (item.id == fuel_id) {
                    this.setState({ fuel: item.name });
                }
                return null;
            });
        }
    }
    responseName = (datas) => {
        const { statusCode, data } = datas;
        if (statusCode === 200 && data.length) {
            this.setState({ name: data[0].name });
        }
    }
    responseCreator = (datas, creator) => {
        const { statusCode, data } = datas;
        if (statusCode === 200 && data.length) {
            data.map((item) => {
                if (item.id == creator) {
                    this.setState({ creator: item.name });
                }
                return null;
            });
        }
    }
    responseWorker = (datas) => {
        const { statusCode, data } = datas;
        if (statusCode === 200 && data.length) {
            this.setState({ worker: data[0].name });
        }
    }
    err = (data) => { console.log('err:', data); }
    events = (renewal, returned, issuance, writeoff) => {
        let a = renewal ? 'Renewal, ' : '';
        let b = returned ? 'Returned, ' : '';
        let c = issuance ? 'Issuance, ' : '';
        let d = writeoff ? 'Write off' : '';
        if (renewal || returned || issuance || writeoff) {
            return (`${a} ${b} ${c} ${d}`);
        }
        return '';
    }
    render() {
        const queries = queryString.parse(this.state.queries);
        const { renewal, returned, issuance, writeoff } = queries;
        const { worker, name, creator, vehicle, fuel } = this.state;
        const { selectedEvent, beginDate, endDate, vehicleFilter, fuelFilter, userFilter, workerFilter
        } = this.state;
        return (
            <div>
                <h2 style={{ textAlign: 'center', margin: '20px 20px 0 20px' }}>REPORT</h2>
                <h4 style={{ textAlign: 'center', margin: '0 20px 40px 20px' }}>vehicles</h4>
                <div>
                    <span style={{ width: '150px', display: 'inline-block' }}>Period:</span>{moment(queries.begin_date).format('LLL')} - {moment(queries.end_date).format('LLL')}
                </div>
                <div>{worker ? <span><span style={{ width: '150px', display: 'inline-block' }}>Worker: </span>{worker}</span> : ''}</div>
                <div>{name ? <span><span style={{ width: '150px', display: 'inline-block' }}>Name: </span>{name}</span> : ''}</div>
                <div>{vehicle ? <span><span style={{ width: '150px', display: 'inline-block' }}>Vehicle: </span>{vehicle}</span> : ''}</div>
                <div>{fuel ? <span><span style={{ width: '150px', display: 'inline-block' }}>Fuel type: </span>{fuel}</span> : ''}</div>
                <div>{creator ? <span><span style={{ width: '150px', display: 'inline-block' }}>Creared: </span>{creator}</span> : ''}</div>
                {renewal || returned || issuance || writeoff ? <span style={{ width: '150px', display: 'inline-block' }}>Actions: </span> : ''}{this.events(renewal, returned, issuance, writeoff)}
                {this.state.queries ? (
                    <div>
                        <VehiclesActionsView
                            history={this.props.history}
                            match={this.props.match}
                            itemFilter={vehicleFilter}
                            eventsFilter={selectedEvent}
                            fuelFilter={fuelFilter}
                            workerFilter={workerFilter}
                            userFilter={userFilter}
                            beginDate={beginDate}
                            endDate={endDate}
                            url={this.state.queries}
                            showName
                            showInventName
                        />
                        <h5 style={{ margin: '20px' }}>Report date: {moment().format('LLL')}</h5>
                    </div>
                ) : <Spinner />}
            </div>
        );
    }
}
