import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import queryString from 'query-string';

import {
    Button, Breadcrumb, BreadcrumbItem, Table, InputGroupAddon, InputGroupText, Input, FormGroup, Label,
    Dropdown, DropdownItem, Container, Form, Alert,
    Row, Col, ListGroup, ListGroupItem, DropdownToggle, DropdownMenu
} from 'reactstrap';
import { ActionsView, Spinner } from '../../reusable';
import { TGET, TPOST } from '../../../api_temp';

export default class Report extends React.Component {
    static propTypes = {
        history: PropTypes.shape({ push: PropTypes.func.isRequired }).isRequired,
        match: PropTypes.shape({ path: PropTypes.string.isRequired }).isRequired,
        location: PropTypes.shape({ search: PropTypes.string.isRequired }).isRequired
    }
    constructor() {
        super();
        this.state = { queries: '', worker: '', creator: '', name: '' };
    }
    componentDidMount() {
        const queries = this.props.location.search;
        this.setState({ queries });
        const { id, worker_id, created_by } = queryString.parse(queries);
        if (worker_id) {
            TGET(`user/workers?id=${worker_id}`, this.responseWorker, this.err);
        }
        if (id) {
            TGET(`user/inventorisation?&id=${id}`, this.responseName, this.err);
        }
        if (created_by) {
            TGET('user/users', data => this.responseCreator(data, created_by), this.err);
        }
    }
    responseName = (datas) => {
        const { statusCode, data } = datas;
        if (statusCode === 200 && data.length) {
            this.setState({ name: data[0].name });
        }
    }
    responseCreator = (datas, creator) => {
        const { statusCode, data } = datas;
        if (statusCode === 200 && data.length) {
            data.map((item) => {
                if (item.id == creator) {
                    this.setState({ creator: item.name });
                }
                return null;
            });
        }
    }
    responseWorker = (datas) => {
        const { statusCode, data } = datas;
        if (statusCode === 200 && data.length) {
            this.setState({ worker: data[0].name });
        }
    }
    err = (data) => { console.log('err:', data); }
    events = (Issuance, Writeoff, Renewal, Retunred) => {
        let a = Issuance ? 'Issuance, ' : '';
        let b = Writeoff ? 'Writeoff, ' : '';
        let c = Renewal ? 'Renewal, ' : '';
        let d = Retunred ? 'Retunred' : '';
        if (Issuance || Writeoff || Renewal || Retunred) {
            return (`${a} ${b} ${c} ${d}`);
        }
        return '';
    }
    render() {
        const queries = queryString.parse(this.state.queries);
        const { Issuance, Writeoff, Renewal, Retunred } = queries;
        const { worker, name, creator } = this.state;
        return (
            <div>
                <h2 style={{ textAlign: 'center', margin: '20px 20px 0 20px' }}>REPORT</h2>
                <h4 style={{ textAlign: 'center', margin: '0 20px 40px 20px' }}>inventory</h4>
                <div>
                    <span style={{ width: '150px', display: 'inline-block' }}>Period:</span>{moment(queries.begin_date).format('LLL')} - {moment(queries.end_date).format('LLL')}
                </div>
                <div>{worker ? <span><span style={{ width: '150px', display: 'inline-block' }}>Worker: </span>{worker}</span> : ''}</div>
                <div>{name ? <span><span style={{ width: '150px', display: 'inline-block' }}>Name: </span>{name}</span> : ''}</div>
                <div>{creator ? <span><span style={{ width: '150px', display: 'inline-block' }}>Created: </span>{creator}</span> : ''}</div>
                {Issuance || Writeoff || Renewal || Retunred ? <span style={{ width: '150px', display: 'inline-block' }}>Actions: </span> : ''}{this.events(Issuance, Writeoff, Renewal, Retunred)}
                {this.state.queries ? (
                    <div>
                        <ActionsView
                            id=""
                            history=""
                            match=""
                            selectedOption={[]}
                            worker_filter=""
                            user_filter=""
                            begin_date=""
                            end_date=""
                            refresh=""
                            url={this.state.queries}
                            showName
                            showInventName
                        />
                        <h5 style={{ margin: '20px' }}>Report date: {moment().format('LLL')}</h5>
                    </div>
                ) : <Spinner />}
            </div>
        );
    }
}
