import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import Datepicker from 'react-datepicker';
// import CurrencyInput from 'react-money-input';
import CurrencyInput from 'react-currency-masked-input'
import {
    Button, Breadcrumb, BreadcrumbItem, Table, InputGroupAddon, InputGroupText, Input, FormGroup, Label,
    Dropdown, DropdownItem, Container, Form, Alert,
    Row, Col, ListGroup, ListGroupItem, DropdownToggle, DropdownMenu
} from 'reactstrap';

import { Spinner, DropdownLoader, DatepickerIntervalPicker, ActionsView, InventoryActionInfo, NumbersValidationWrapper } from '../../../reusable';
import { TPOST } from '../../../../api_temp';
import './styles.css';


export default class Purchase extends React.Component {
    static propTypes = {
        history: PropTypes.shape({ push: PropTypes.func.isRequired }).isRequired,
        match: PropTypes.shape({ path: PropTypes.string.isRequired }).isRequired
    }
    constructor() {
        super();
        this.state = { warning: '', price: 0, bill_date: moment(), description: '', invoice: '' };
        this.priceHandle = this.priceHandle.bind(this);
        this.descriptionHandle = this.descriptionHandle.bind(this);
    }
    priceHandle = (price) => { this.setState({ price: price.target.value }); }
    invoiceHandle = (invoice) => { this.setState({ invoice: invoice.target.value }); }
    descriptionHandle = (description) => { this.setState({ description: description.target.value }); }
    handleSubmit = (e) => {
        e.preventDefault();
        const { match } = this.props;
        const { id } = match.params;
        const { price, bill_date, invoice, description } = this.state;
        TPOST('user/purchases', { counterparty_id: id, price, bill_date: bill_date.format('YYYY-MM-DDTHH:mm:ss'), invoice, description }, this.responsePurhase, this.err);
    }
    responsePurhase = (datas) => {
        const { statusCode } = datas;
        if (statusCode === 200) {
            this.props.history.push('/user/purchases');
        }
    }
    err = (data) => {
        console.log('err:', data);
    }
    render() {
        const { price, bill_date, invoice, description, warning } = this.state;
        return (
            <div className="purchase">
                <Button
                    style={{ margin: '20px auto 20px 20px', display: 'block' }}
                    onClick={() => this.props.history.push('/user/purchases')}
                > Назад
                </Button>
                <h2 style={{ textAlign: 'center', marginTop: '30px' }}>Приєднання закупівлі</h2>
                <Form onSubmit={this.handleSubmit} noValidate>
                    <Row>
                        <Col>
                            <ListGroup className="form-titles col-9">
                                <ListGroupItem>Ціна</ListGroupItem>
                                <ListGroupItem>Дата чеку/закупівлі</ListGroupItem>
                                <ListGroupItem>Номер видаткової накладної</ListGroupItem>
                                <ListGroupItem>Додаткова інформація</ListGroupItem>
                            </ListGroup>
                        </Col>
                        <div className="separator"></div>
                        <Col>
                            <ListGroup className=" form-fields col-9">
                                <FormGroup>
                                    <NumbersValidationWrapper type="Ціна" func={value => this.setState({ price: value })} />
                                </FormGroup>
                                <FormGroup className="fix-datepicker-pos">
                                    <Datepicker
                                        className="form-control"
                                        selected={bill_date}
                                        dateFormat="lll"
                                        onChange={date => this.setState({ bill_date: moment(date) })}
                                        showTimeSelect
                                        locale="en"
                                        timeFormat="HH:mm"
                                        timeIntervals={1}
                                        timeCaption="Time"
                                    />
                                </FormGroup>
                                <FormGroup>
                                    <Input
                                        type="text"
                                        value={invoice}
                                        onChange={this.invoiceHandle}
                                    />
                                </FormGroup>
                                <FormGroup>
                                    <Input
                                        type="textarea"
                                        value={description}
                                        onChange={this.descriptionHandle}
                                    />
                                </FormGroup>
                            </ListGroup>
                        </Col>
                    </Row>
                    {warning
                        ? (
                            <Alert color="warning" className="fields-warning-create">
                                {warning}
                            </Alert>
                        )
                        : ''}
                    { price && bill_date
                        ? <Button style={{ margin: '20px auto', display: 'block' }} className="add-assistant-submit" type="submit">Створити</Button>
                        : ''}
                </Form>
            </div>
        );
    }
}
