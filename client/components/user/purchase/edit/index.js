import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import moment from 'moment';
import Select from 'react-select';
import Datepicker from 'react-datepicker';
import {
    Button, Table, Input, FormGroup, Form, Alert,
    Row, Col, ListGroup, ListGroupItem
} from 'reactstrap';

import { Spinner, NumbersValidationWrapper } from '../../../reusable';
import { TPOST, TGET } from '../../../../api_temp';


class EditPurchase extends React.Component {
    static propTypes = {
        history: PropTypes.shape({ push: PropTypes.func.isRequired }).isRequired,
        match: PropTypes.shape({ path: PropTypes.string.isRequired }).isRequired,
        counterparties: PropTypes.oneOfType([
            PropTypes.string.isRequired,
            PropTypes.arrayOf(PropTypes.shape({}).isRequired).isRequired
        ]).isRequired
    }
    constructor() {
        super();
        this.state = { warning: '', price: 0, bill_date: moment(), description: '', invoice: '', created_at: '', selectedCounterparty: [] };
        this.priceHandle = this.priceHandle.bind(this);
        this.descriptionHandle = this.descriptionHandle.bind(this);
    }
    componentDidMount() {
        const { match } = this.props;
        const { id } = match.params;
        TGET(`user/purchases?event_id=${id}`, this.responseAction, this.err);
        this.init();
    }
    init = () => {
        const { match } = this.props;
        const { id } = match.params;
        TGET(`user/purchase-history?id=${id}`, this.responseHistory, this.err);
    }
    responseAction = (datas) => {
        const { statusCode, purchases } = datas;
        if (statusCode === 200) {
            this.setState({
                created_at: moment(purchases[0].created_at),
                price: purchases[0].price,
                bill_date: moment(purchases[0].bill_date),
                description: purchases[0].description || '',
                invoice: purchases[0].invoice,
                selectedCounterparty: { value: purchases[0].counterparty_id, label: purchases[0].name }
            });
        }
    }
    priceHandle = (price) => { this.setState({ price: price.target.value }); }
    invoiceHandle = (invoice) => { this.setState({ invoice: invoice.target.value }); }
    descriptionHandle = (description) => { this.setState({ description: description.target.value }); }
    handleSubmit = (e) => {
        e.preventDefault();
        this.setState({ history: '' });
        const { match } = this.props;
        const { id } = match.params;
        const { price, bill_date, invoice, description, selectedCounterparty } = this.state;
        TPOST('user/update-purchase', {
            id, price, bill_date: bill_date.format('YYYY-MM-DDTHH:mm:ss'), invoice, description, counterparty_id: selectedCounterparty.value
        }, this.responsePurchase, this.err);
    }
    responsePurchase = (datas) => {
        const { statusCode } = datas;
        console.log(datas);
        if (statusCode === 200) {
            this.init();
        }
    }
    err = (data) => {
        console.log('err:', data);
    }
    responseHistory = (datas) => {
        const { statusCode, history } = datas;
        if (statusCode === 200) {
            this.setState({ history });
        }
    }
    renderHistory = () => {
        const { history } = this.state;
        if (history) {
            return history.map((item, i) => {
                const { name, price, description, bill_date, invoice, created_at, creator_name } = item;
                return (
                    <tr key={i}>
                        <td>{i + 1}</td>
                        <td>{name}</td>
                        <td>{price}</td>
                        <td>{invoice || '—'}</td>
                        <td>{description || '—'}</td>
                        <td>{moment(bill_date).format('lll:ss')}</td>
                        <td>{creator_name}</td>
                        <td>{moment(created_at).format('lll:ss')}</td>
                    </tr>
                );
            });
        }
        return null;
    }
    render() {
        const { created_at, price, bill_date, invoice, description, warning, selectedCounterparty, history } = this.state;
        const { counterparties } = this.props;
        return (
            <div className="purchase">
                <Button
                    style={{ margin: '20px auto 20px 20px', display: 'block' }}
                    onClick={() => this.props.history.push('/user/purchases')}
                > Back
                </Button>
                <h2 style={{ textAlign: 'center', marginTop: '30px' }}>Procurement joining</h2>
                <Form onSubmit={this.handleSubmit} noValidate>
                    <Row>
                        <Col>
                            <ListGroup className="form-titles col-9">
                                <ListGroupItem>Date of attachment</ListGroupItem>
                                <ListGroupItem>Counterparty</ListGroupItem>
                                <ListGroupItem>Price</ListGroupItem>
                                <ListGroupItem>Date of purchase</ListGroupItem>
                                <ListGroupItem>Expense bill number</ListGroupItem>
                                <ListGroupItem>Additional Information</ListGroupItem>
                            </ListGroup>
                        </Col>
                        <div className="separator"></div>
                        <Col>
                            <ListGroup className=" form-fields col-9">
                                <FormGroup className="fix-datepicker-pos">
                                    <Datepicker
                                        disabled
                                        locale="en"
                                        className="form-control"
                                        selected={created_at}
                                        dateFormat="lll"
                                        showTimeSelect
                                        timeFormat="HH:mm"
                                        timeIntervals={1}
                                        timeCaption="Time"
                                    />
                                </FormGroup>
                                <FormGroup>
                                    { counterparties ? (
                                        <Select
                                            isClearable
                                            value={selectedCounterparty}
                                            onChange={item => this.setState({ selectedCounterparty: item })}
                                            options={counterparties}
                                            placeholder="Choose Counterparty"
                                            className="multi multi-large"
                                        />) : <div style={{ display: 'inline-block', height: '38px' }}><Spinner classes="smallify" /></div> }
                                </FormGroup>
                                <FormGroup>
                                    <NumbersValidationWrapper value={price} type="Ціна" func={value => this.setState({ price: value })} />
                                </FormGroup>
                                <FormGroup className="fix-datepicker-pos">
                                    <Datepicker
                                        className="form-control"
                                        selected={bill_date}
                                        dateFormat="lll"
                                        onChange={date => this.setState({ bill_date: moment(date) })}
                                        showTimeSelect
                                        locale="en"
                                        timeFormat="HH:mm"
                                        timeIntervals={1}
                                        timeCaption="Time"
                                    />
                                </FormGroup>
                                <FormGroup>
                                    <Input
                                        type="text"
                                        value={invoice}
                                        onChange={this.invoiceHandle}
                                    />
                                </FormGroup>
                                <FormGroup>
                                    <Input
                                        type="textarea"
                                        value={description}
                                        onChange={this.descriptionHandle}
                                    />
                                </FormGroup>
                            </ListGroup>
                        </Col>
                    </Row>
                    {warning
                        ? (
                            <Alert color="warning" className="fields-warning-create">
                                {warning}
                            </Alert>
                        )
                        : ''}
                    { price && bill_date
                        ? <Button style={{ margin: '20px auto', display: 'block' }} className="add-assistant-submit" type="submit">Change</Button>
                        : ''}
                </Form>
                {
                    history
                        ? history.length
                            ? (
                                <div>
                                    <h2 style={{ textAlign: 'center', marginTop: '30px' }}>Change History</h2>
                                    <Table className="table-editor-helper">
                                        <thead>
                                            <tr>
                                                <th> </th>
                                                <th>Counterparty</th>
                                                <th>Price</th>
                                                <th>Sales Invoice</th>
                                                <th>Descr</th>
                                                <th>Date of purchase</th>
                                                <th>Created by</th>
                                                <th>Created at</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {this.renderHistory()}
                                        </tbody>
                                    </Table>
                                </div>
                            )
                            : ''
                        : <Spinner />
                }
            </div>
        );
    }
}

const mapStateToProps = state => ({
    counterparties: state.counterparties
});

export default connect(mapStateToProps)(EditPurchase);
