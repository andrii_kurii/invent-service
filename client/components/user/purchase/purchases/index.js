import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Button, Table, Input } from 'reactstrap';
import moment from 'moment';
import Select from 'react-select';
import { TGET } from '../../../../api_temp';
import { Spinner, DropdownLoader, DatepickerIntervalPicker, MyPagination } from '../../../reusable';

moment.locale('uk');

class Purchases extends React.Component {
    static propTypes = {
        history: PropTypes.shape({ push: PropTypes.func.isRequired }).isRequired,
        match: PropTypes.shape({ path: PropTypes.string.isRequired }).isRequired,
        users: PropTypes.oneOfType([
            PropTypes.string.isRequired,
            PropTypes.arrayOf(PropTypes.shape({}).isRequired).isRequired
        ]).isRequired,
        counterparties: PropTypes.oneOfType([
            PropTypes.string.isRequired,
            PropTypes.arrayOf(PropTypes.shape({}).isRequired).isRequired
        ]).isRequired
    }
    constructor(props) {
        super(props);
        this.state = {
            purchases: '',
            invoices: '',
            selected_counterparty: [],
            selected_cp: [],
            // selected_invoice: [],
            counterparty_filter: '',
            user_filter: '',
            invoiceFilter: '',
            begin_date: moment().startOf('month'),
            end_date: moment().endOf('month'),
            total: 0,
            page: 1,
            limit: 10,
            end: ''
        };
    }
    componentDidMount() {
        // TGET('user/invoices', data => this.response('invoices', data), this.err);
        this.init();
    }
    parser = (url) => {
        const { page, limit, begin_date, end_date, counterparty_filter, user_filter, invoiceFilter } = this.state;
        return (
            `${url}?${
                page
                    ? `&page=${page}`
                    : ''}${
                limit
                    ? `&limit=${limit}`
                    : ''}${
                counterparty_filter
                    ? `&counterparty_id=${counterparty_filter}`
                    : ''}${
                invoiceFilter
                    ? `&invoice=${invoiceFilter}`
                    : ''}${
                user_filter
                    ? `&created_by=${user_filter}`
                    : ''}${
                begin_date
                    ? `&begin_date=${begin_date.format('YYYY-MM-DDTHH:mm:ss')}`
                    : ''}${
                end_date
                    ? `&end_date=${end_date.format('YYYY-MM-DDTHH:mm:ss')}`
                    : ''}`
        );
    }
    init = () => {
        this.setState({ purchases: '', total: '' });
        TGET(this.parser('user/purchases'), this.responsePurchases, this.err);
    }
    response = (key, datas) => {
        const { statusCode, data } = datas;
        if (statusCode === 200) {
            let a = {};
            a = data.map((item) => { a = { value: item.invoice, label: item.invoice, ...item }; return a; });
            this.setState({ [key]: a });
        }
    }
    responseTotal = (datas) => {
        const { statusCode, total } = datas;
        if (statusCode === 200) {
            this.setState({ total });
        }
    }
    responsePurchases = (datas) => {
        const { statusCode, purchases, end, total } = datas;
        if (statusCode === 200) {
            this.setState({ purchases, total, end });
        }
    }
    err = (data) => {
        console.log('err:', data);
    }
    renderPurchases = () => {
        const { purchases, page } = this.state;
        if (purchases) {
            return purchases.map((item, i) => {
                const { id, name, price, description, bill_date, invoice, created_at, creator_name } = item;
                return (
                    <tr key={i}>
                        <td>{(page - 1) * 10 + i + 1}</td>
                        <td>{name}</td>
                        <td>{price}</td>
                        <td>{invoice || '—'}</td>
                        <td>{description || '—'}</td>
                        <td>{moment(bill_date).format('lll:ss')}</td>
                        <td>{creator_name}</td>
                        <td>{moment(created_at).format('lll:ss')}</td>
                        <td className="pen" onClick={() => this.props.history.push(`${this.props.match.path}/events/${id}`)}> &#x270E; </td>
                    </tr>
                );
            });
        }
        return null;
    }
    handleSelectCounterparty = (selected_cp) => {
        if (selected_cp !== null) {
            this.setState({ selected_cp, counterparty_filter: selected_cp.id }, () => this.init());
        } else {
            this.setState({ selected_cp, counterparty_filter: '' }, () => this.init());
        }
    }
    handleInvoice = (e) => {
        clearTimeout(this.timeout);
        const invoiceFilter = e.target.value;
        this.timeout = setTimeout(() => this.setState({ invoiceFilter }, () => this.init()), 750);
    }
    // handleSelectInvoice = (fkey, selected_invoice, skey, inner_key) => {
    //     if (selected_invoice !== null) {
    //         this.setState({ [fkey]: selected_invoice, [skey]: selected_invoice[inner_key] }, () => this.init());
    //     } else {
    //         this.setState({ [fkey]: selected_invoice, [skey]: '' }, () => this.init());
    //     }
    // }
    getPage = (page) => {
        const { end } = this.state;
        if (page > 0 && page <= end) {
            this.setState({ page }, () => this.init());
        }
    }
    render() {
        const { match, history, users, counterparties } = this.props;
        const {
            purchases, invoices, selected_counterparty, selected_cp, selected_invoice, begin_date, end_date, total
        } = this.state;
        return (
            <div>
                <Button
                    style={{ margin: '20px 20px 0 auto', display: 'block' }}
                    onClick={() => history.push(`${match.path}/create-counterparty`)}
                > Create Counterparty
                </Button>
                <div style={{ display: 'inline-block' }}>
                    <span style={{ padding: '0 20px', display: 'inline-block', minWidth: '150px' }}>Attach Purchase to</span>
                    { counterparties ? (
                        <Select
                            value={selected_counterparty}
                            onChange={item => this.props.history.push(`${this.props.match.path}/${item.id}`)}
                            options={counterparties}
                            placeholder="Choose counterparty"
                            className="multi multi-large"
                        />) : <div style={{ display: 'inline-block', height: '38px' }}><Spinner classes="smallify" /></div> }
                </div>
                <h2 style={{
                    textAlign: 'center', marginTop: '30px', marginBottom: '20px', paddingTop: '30px', borderTop: '1px solid lightgrey'
                }}
                > Purchases
                </h2>
                <div className="filter-multi-select">
                    <div style={{ display: 'inline-block' }}>
                        <span style={{ padding: '0 20px', minWidth: '230px', display: 'inline-block' }}>Counterparty</span>
                        { counterparties ? (
                            <Select
                                isClearable="true"
                                value={selected_cp}
                                onChange={item => this.handleSelectCounterparty(item)}
                                options={counterparties}
                                placeholder="Choose"
                                className="multi multi-large"
                            />) : <div style={{ display: 'inline-block', height: '38px' }}><Spinner classes="smallify" /></div> }
                    </div>
                </div>
                <div className="filter-multi-select">
                    <div style={{ display: 'flex', alignItems: 'center' }}>
                        <span style={{ padding: '0 20px', minWidth: '230px', display: 'inline-block' }}>Sales Invoice</span>
                        <Input
                            style={{ width: '300px' }}
                            placeholder="Type to filter"
                            onChange={this.handleInvoice}
                        />
                    </div>
                    {/* <Select
                        isClearable="true"
                        value={selected_invoice}
                        onChange={item => this.handleSelectInvoice('selected_invoice', item, 'invoiceFilter', 'invoice')}
                        options={invoices}
                        placeholder="Choose"
                        className="multi multi-large"
                    /> */}
                </div>
                <div style={{ display: 'inline-block' }}>
                    <DatepickerIntervalPicker
                        // title="Початкова дата закупівель"
                        begin_date={begin_date}
                        end_date={end_date}
                        setBeginDate={date => this.setState({ begin_date: date }, () => this.init())}
                        setEndDate={date => this.setState({ end_date: date }, () => this.init())}
                    />
                </div>
                <div className="filter-multi-select">
                    <div style={{ display: 'inline-block' }}>
                        <span style={{ padding: '0 20px', minWidth: '230px', display: 'inline-block' }}>Created by</span>
                        {users
                            ? (
                                <DropdownLoader
                                    style={{ display: 'inline-block' }}
                                    data={users}
                                    set={user_id => this.setState({ user_filter: user_id }, () => this.init())}
                                    title="Choose author"
                                    keys="name"
                                    emptify
                                />)
                            : <div style={{ display: 'inline-block', height: '38px' }}><Spinner classes="smallify" /></div>
                        }
                    </div>
                </div>
                { purchases
                    ? (
                        purchases.length
                            ? (
                                <div>
                                    <Table className="table-editor-helper">
                                        <thead>
                                            <tr>
                                                <th> </th>
                                                <th>Counterparty</th>
                                                <th>Price</th>
                                                <th>Sales Invoice</th>
                                                <th>Descr</th>
                                                <th>Date of purchase</th>
                                                <th>Created by</th>
                                                <th>Created at</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {this.renderPurchases()}
                                            <tr>
                                                <td></td>
                                                <td></td>
                                                <td style={{ fontWeight: 900 }}>{total}</td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                        </tbody>
                                    </Table>
                                    {this.state.end !== '1'
                                        ? (
                                            <MyPagination
                                                end={this.state.end}
                                                page={this.state.page}
                                                getPage={this.getPage}
                                            />) : '' }
                                </div>)
                            : <h3 style={{ textAlign: 'center' }}>No puchases</h3>
                    )
                    : <Spinner />
                }
            </div>
        );
    }
}

const mapStateToProps = state => ({
    inventorisation: state.inventorisation,
    invents: state.invents,
    actionTypes: state.actionTypes,
    users: state.users,
    workers: state.workers,
    counterparties: state.counterparties
});

export default connect(mapStateToProps)(Purchases);
