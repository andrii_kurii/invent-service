import React from 'react';
import PropTypes from 'prop-types';
import { Button, Input, FormGroup, Form, Alert, Row, Col, ListGroup, ListGroupItem
} from 'reactstrap';
import { DropdownLoader, Spinner, DatepickerIntervalPicker } from '../../../reusable';
import { TGET, TPOST } from '../../../../api_temp';
import { counterpartiesFetch } from '../../../../redux/actions/with-request/fetch-counterparties';

export default class CreateCounterparty extends React.Component {
    static propTypes = {
        history: PropTypes.shape({ push: PropTypes.func.isRequired }).isRequired,
        match: PropTypes.shape({ path: PropTypes.string.isRequired }).isRequired
    }
    constructor() {
        super();
        this.state = { warning: '', name: '', description: '' };
        this.nameToggle = this.nameToggle.bind(this);
        this.descriptionToggle = this.descriptionToggle.bind(this);
    }
    nameToggle = (name) => { this.setState({ name: name.target.value }); }
    descriptionToggle = (description) => { this.setState({ description: description.target.value }); }
    handleSubmit = (e) => {
        e.preventDefault();
        const { name, description } = this.state;
        TPOST('user/create-counterparty', { name, description }, this.responseCreateCounterparty, this.errCreateCounterparty);
    }
    responseCreateCounterparty = (datas) => {
        const { statusCode } = datas;
        if (statusCode === 200) {
            this.props.history.push('/user/purchases');
            counterpartiesFetch();
        }
    }
    errCreateCounterparty = (data) => { console.log('err:', data); }
    render() {
        const { history } = this.props;
        const { warning, name, description } = this.state;
        return (
            <div>
                <Button
                    style={{ margin: '20px auto 20px 20px', display: 'block' }}
                    onClick={() => history.push('/user/purchases')}
                > Назад
                </Button>
                <h2 style={{ textAlign: 'center', marginTop: '30px' }}>Create new Counterparty</h2>
                <Form onSubmit={this.handleSubmit}>
                    <Row>
                        <Col>
                            <ListGroup className="form-titles col-8">
                                <ListGroupItem>Counterparty (Name)</ListGroupItem>
                                <ListGroupItem>Additional info</ListGroupItem>
                            </ListGroup>
                        </Col>
                        <div className="separator"></div>
                        <Col>
                            <ListGroup className=" form-fields col-8">
                                <FormGroup>
                                    <Input
                                        type="text"
                                        value={name}
                                        onChange={this.nameToggle}
                                    />
                                </FormGroup>
                                <FormGroup>
                                    <Input
                                        type="textarea"
                                        value={description}
                                        onChange={this.descriptionToggle}
                                    />
                                </FormGroup>
                            </ListGroup>
                        </Col>
                    </Row>
                    {warning
                        ? (
                            <Alert color="warning" className="fields-warning-create">
                                {warning}
                            </Alert>
                        )
                        : ''}
                    { name
                        ? <Button style={{ margin: '20px auto', display: 'block' }} className="add-assistant-submit" type="submit">Створити</Button>
                        : ''}
                </Form>
            </div>
        );
    }
}
