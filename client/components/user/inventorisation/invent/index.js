import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Button } from 'reactstrap';
import moment from 'moment';
import { TGET } from '../../../../api_temp';
import { ActionsView, InventoryActionInfo } from '../../../reusable';

import './invent-item.css';

class Invent extends React.Component {
    static propTypes = {
        history: PropTypes.shape({ push: PropTypes.func.isRequired }).isRequired,
        match: PropTypes.shape({}).isRequired
    }
    constructor(props) {
        super(props);
        this.state = {
            selectedOption: [],
            worker_filter: '',
            user_filter: '',
            begin_date: moment().startOf('month'),
            end_date: moment().endOf('month'),
            refresh: false
        };
    }
    handleMultiSelect = (selectedOption) => {
        this.setState({ selectedOption });
    }
    render() {
        const { match, history } = this.props;
        const { id } = match.params;
        const { refresh } = this.state;
        const { selectedOption, worker_filter, user_filter, begin_date, end_date } = this.state;
        return (
            <div>
                <Button style={{ margin: '20px 0 0 auto', display: 'block' }} onClick={() => history.push('/user/inventorisation')}>
                    Done
                </Button>
                <h2 style={{ textAlign: 'center', marginTop: '0', marginBottom: '10px' }}>Current inventory</h2>
                <InventoryActionInfo
                    history={this.props.history}
                    match={this.props.history}
                    id={id}
                    refresh={() => this.setState({ refresh: !refresh })}
                />
                <h2 style={{ textAlign: 'center', marginBottom: '20px', paddingTop: '30px' }}>
                  Summary on current Inventory
                </h2>
                <ActionsView
                    id={id}
                    history={this.props.history}
                    match={this.props.history}
                    selectedOption={selectedOption}
                    worker_filter={worker_filter}
                    user_filter={user_filter}
                    begin_date={begin_date}
                    end_date={end_date}
                    refresh={refresh}
                />
            </div>
        );
    }
}

const mapStateToProps = state => ({
    inventorisation: state.inventorisation,
    invents: state.invents,
    actionTypes: state.actionTypes,
    users: state.users,
    workers: state.workers
});

export default connect(mapStateToProps)(Invent);
