import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {
    Button, Breadcrumb, BreadcrumbItem, Table, InputGroupAddon, InputGroupText, Input, FormGroup, Label,
    Dropdown, DropdownItem, Container, Form, Alert,
    Row, Col, ListGroup, ListGroupItem, DropdownToggle, DropdownMenu
} from 'reactstrap';
import DatePicker from 'react-datepicker';
import moment from 'moment';
import Select from 'react-select';
import { TGET, TPOST } from '../../../../api_temp';
import { Spinner, DropdownLoader, DatepickerIntervalPicker, NumbersValidationWrapper } from '../../../reusable';

class Event extends React.Component {
    static propTypes = {
        history: PropTypes.shape({ push: PropTypes.func.isRequired }).isRequired,
        match: PropTypes.shape({ path: PropTypes.string.isRequired }).isRequired,
        workers: PropTypes.oneOfType([
            PropTypes.string.isRequired,
            PropTypes.arrayOf(PropTypes.shape({}).isRequired).isRequired
        ]).isRequired,
        actionTypes: PropTypes.oneOfType([
            PropTypes.string.isRequired,
            PropTypes.arrayOf(PropTypes.shape({}).isRequired).isRequired
        ]).isRequired,
        inventorisation: PropTypes.oneOfType([
            PropTypes.string.isRequired,
            PropTypes.arrayOf(PropTypes.shape({}).isRequired).isRequired
        ]).isRequired
    }
    constructor(props) {
        super(props);
        this.state = {
            created_at: moment(),
            actionHistory: '',
            // worker_id: '',
            // inventorisation_id: '',
            invent_name: '',
            // action_types: '',
            selectedOption: [],
            selectedInv: [],
            selectedWorker: []
        };
    }
    componentDidMount() {
        this.init();
    }
    init = () => {
        const { match } = this.props;
        const { id } = match.params;
        TGET(`user/invent-actions?event_id=${id}`, this.responseEvent, this.err);
        TGET(`user/invent-action-history?id=${id}`, this.responseHistory, this.err);
    }
    responseHistory = (datas) => {
        const { statusCode, data } = datas;
        if (statusCode === 200) {
            this.setState({ actionHistory: data });
        }
    }
    responseEvent = (datas) => {
        const { statusCode, actions } = datas;
        if (statusCode === 200) {
            if (actions.length) {
                this.setState({
                    created_at: moment(actions[0].created_at),
                    worker: actions[0],
                    invent_name: actions[0].invent_name,
                    action_type: actions[0].action_type,
                    inventorisation_id: actions[0].inventorisation_id,
                    value: actions[0].value,
                    selectedOption: { value: actions[0].action_type, label: actions[0].action_name },
                    selectedInv: { value: actions[0].inventorisation_id, label: actions[0].name },
                    selectedWorker: actions[0].worker_id ? { value: actions[0].worker_id, label: actions[0].worker_name } : '',
                });
            }
        }
    }
    err = (data) => {
        console.log('err:', data);
    }
    handleSubmit = (e) => {
        e.preventDefault();
        const { match } = this.props;
        const { id } = match.params;
        const { selectedOption, selectedInv, selectedWorker, value } = this.state;
        TPOST('user/update-invent-action', {
            invent_action_id: id,
            action_type: selectedOption.value,
            inventorisation_id: selectedInv.value,
            worker_id: selectedWorker? selectedWorker.value : null,
            value
        }, this.responseUpdateAction, this.err);
    }
    responseUpdateAction = (datas) => {
        const { statusCode, data } = datas;
        if (statusCode === 200) {
            this.setState({ actionHistory: '' }, () => this.init());
        }
    }
    handleMultiSelect = (selectedOption) => {
        this.setState({ selectedOption });
    }
    handleSelectedInv = (selectedInv) => {
        this.setState({ selectedInv, invent_name: selectedInv.invent, value: '' }, () => document.getElementsByClassName('currency')[0].value = '');
    }
    handleSelectedWorker = (selectedWorker) => {
        this.setState({ selectedWorker });
    }
    renderHistory = () => {
        const { actionHistory } = this.state;
        if (actionHistory) {
            return actionHistory.map((item, i) => {
                const { id, name, value, invent_name, action_name, worker_name, created_at, creator_name } = item;
                return (
                    <tr key={i}>
                        <td>{i + 1}</td>
                        <td>{action_name}</td>
                        <td>{name}</td>
                        <td>{value}</td>
                        <td>{invent_name}</td>
                        <td>{worker_name || '—' }</td>
                        <td>{moment(created_at).format('lll:ss')}</td>
                        <td>{creator_name}</td>
                        {/* <td className="pen" onClick={() => this.props.history.push(`/user/inventorisation/events/${id}`)}> &#x270E; </td> */}
                    </tr>
                );
            });
        }
        return null;
    }
    render() {
        const { match, history, workers, actionTypes, inventorisation } = this.props;
        const { invent_name, value, created_at, actionHistory } = this.state;
        const {
            selectedOption, selectedInv, selectedWorker
        } = this.state;
        return (
            <div>
                <Button style={{ margin: '20px auto 0 0', display: 'block' }} onClick={() => history.push('/user/inventorisation')}>
                    Back
                </Button>
                <h2 style={{ textAlign: 'center', marginTop: '30px' }}>Edit Action</h2>
                <Form onSubmit={this.handleSubmit} noValidate>
                    <Row>
                        <Col>
                            <ListGroup className="form-titles col-8">
                                <ListGroupItem>Action date</ListGroupItem>
                                <ListGroupItem>Action</ListGroupItem>
                                <ListGroupItem>Name</ListGroupItem>
                                <ListGroupItem>Worker</ListGroupItem>
                                <ListGroupItem>Dimension ({invent_name})</ListGroupItem>
                            </ListGroup>
                        </Col>
                        <div className="separator"></div>
                        <Col>
                            <ListGroup className=" form-fields col-8">
                                <FormGroup className="fields-clear-marbot">                                    
                                    {created_at ? (
                                        <DatePicker
                                            disabled
                                            className="form-control"
                                            selected={created_at}
                                            dateFormat="lll:ss"
                                            locale="en"
                                            showTimeSelect
                                            timeCaption="Time"
                                            timeFormat="HH:mm"
                                            timeIntervals={1}
                                        />
                                    ) : <div style={{ display: 'inline-block', height: '38px' }}><Spinner classes="smallify" /></div> }
                                </FormGroup>
                            </ListGroup>
                            <ListGroup style={{ marginTop: '.75em' }} className=" form-fields col-8">
                                <FormGroup className="fields-clear-marbot">
                                    { actionTypes ? (
                                        <Select
                                            value={selectedOption}
                                            onChange={this.handleMultiSelect}
                                            options={actionTypes}
                                            placeholder="Choose"
                                            className="multi"
                                        />) : <div style={{ display: 'inline-block', height: '38px' }}><Spinner classes="smallify" /></div> }
                                </FormGroup>
                            </ListGroup>
                            <ListGroup style={{ marginTop: '.75em' }} className=" form-fields col-8">
                                <FormGroup className="fields-clear-marbot">
                                    { inventorisation ? (
                                        <Select
                                            value={selectedInv}
                                            onChange={this.handleSelectedInv}
                                            options={inventorisation}
                                            placeholder="Choose"
                                            className="multi"
                                        />) : <div style={{ display: 'inline-block', height: '38px' }}><Spinner classes="smallify" /></div> }
                                </FormGroup>
                            </ListGroup>
                            <ListGroup style={{ marginTop: '.75em' }} className=" form-fields col-8">
                                <FormGroup className="fields-clear-marbot">
                                    { workers ? (
                                        <Select
                                            isClearable
                                            value={selectedWorker}
                                            onChange={this.handleSelectedWorker}
                                            options={workers}
                                            placeholder="Choose"
                                            className="multi"
                                        />) : <div style={{ display: 'inline-block', height: '38px' }}><Spinner classes="smallify" /></div> }
                                </FormGroup>
                            </ListGroup>
                            <ListGroup style={{ marginTop: '.75em' }} className=" form-fields col-8">
                                <FormGroup className="fields-clear-marbot">
                                    { selectedInv ? (
                                        <NumbersValidationWrapper value={value} type={invent_name} func={rr => this.setState({ value: rr })} />
                                    ): <div style={{ display: 'inline-block', height: '38px' }}><Spinner classes="smallify" /></div> }
                                </FormGroup>
                            </ListGroup>
                        </Col>
                    </Row>
                    { selectedOption && selectedInv && value
                        ? <Button style={{ margin: '20px auto', display: 'block' }} className="add-assistant-submit" type="submit">Submit</Button>
                        : ''}
                </Form>
                {
                    actionHistory
                        ? actionHistory.length
                            ? (
                                <div>
                                    <h2 style={{ textAlign: 'center', marginTop: '30px' }}>Changes history</h2>
                                    <Table className="table-editor-helper">
                                        <thead>
                                            <tr>
                                                <th> </th>
                                                <th>Action</th>
                                                <th>Name</th>
                                                <th>Dimension</th>
                                                <th>Measurement</th>
                                                <th>Workers</th>
                                                <th>Created at</th>
                                                <th>Created by</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {this.renderHistory()}
                                        </tbody>
                                    </Table>
                                </div>
                            )
                            : ''
                        : <Spinner />
                }
            </div>
        );
    }
}

const mapStateToProps = state => ({
    inventorisation: state.inventorisation,
    invents: state.invents,
    actionTypes: state.actionTypes,
    users: state.users,
    workers: state.workers
});

export default connect(mapStateToProps)(Event);
