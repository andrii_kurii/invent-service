import React from 'react';
import PropTypes from 'prop-types';
import { Button, Table } from 'reactstrap';
import moment from 'moment';
import Select from 'react-select';
import { connect } from 'react-redux';

import './invents.css';

import { Spinner, DropdownLoader, DatepickerIntervalPicker, ActionsView } from '../../../reusable';

class Invents extends React.Component {
    static propTypes = {
        history: PropTypes.shape({ push: PropTypes.func.isRequired }).isRequired,
        match: PropTypes.shape({ path: PropTypes.string.isRequired }).isRequired,
        inventorisation: PropTypes.oneOfType([
            PropTypes.string.isRequired,
            PropTypes.arrayOf(PropTypes.shape({}).isRequired).isRequired
        ]).isRequired,
        actionTypes: PropTypes.oneOfType([
            PropTypes.string.isRequired,
            PropTypes.arrayOf(PropTypes.shape({}).isRequired).isRequired
        ]).isRequired,
        users: PropTypes.oneOfType([
            PropTypes.string.isRequired,
            PropTypes.arrayOf(PropTypes.shape({}).isRequired).isRequired
        ]).isRequired,
        workers: PropTypes.oneOfType([
            PropTypes.string.isRequired,
            PropTypes.arrayOf(PropTypes.shape({}).isRequired).isRequired
        ]).isRequired
    }
    constructor(props) {
        super(props);
        this.state = {
            selectedOption: [],
            selectedItem: [],
            selectedWorker: [],
            worker_filter: '',
            user_filter: '',
            inventory_filter: '',
            begin_date: moment().startOf('month'),
            end_date: moment().endOf('month'),
            refresh: false
        };
    }
    componentDidMount() {
        setInterval(() => console.log(this.props), 1000);
    }
    handleMultiSelect = (selectedOption) => {
        this.setState({ selectedOption });
    }
    handleInventory = (selectedItem) => {
        const { refresh } = this.state;
        if (selectedItem !== null) {
            this.setState({ selectedItem, inventory_filter: selectedItem.id }, () => this.setState({ refresh: !refresh }));
        } else {
            this.setState({ selectedItem, inventory_filter: '' }, () => this.setState({ refresh: !refresh }));
        }
    }
    handleSelectWorkers = (selectedWorker) => {
        const { refresh } = this.state;
        if (selectedWorker !== null) {
            this.setState({ selectedWorker, worker_filter: selectedWorker.id }, () => this.setState({ refresh: !refresh }));
        } else {
            this.setState({ selectedWorker, worker_filter: '' }, () => this.setState({ refresh: !refresh }));
        }
    }
    render() {
        const { match, history, inventorisation, actionTypes, users, workers } = this.props;
        // const { refresh } = this.state;
        const {
            selectedOption, selectedItem, selectedWorker, worker_filter, user_filter, inventory_filter, begin_date, end_date
        } = this.state;
        // console.log(inventorisation.data, inventorisation.loading);
        return (
            <div>
                <Button
                    style={{ margin: '20px 20px 0 auto', display: 'block' }}
                    onClick={() => history.push(`${match.path}/create-invent`)}
                > Create Inventory/Supplies
                </Button>
                <div style={{ display: 'inline-block' }}>
                    <span style={{ padding: '0 20px', display: 'inline-block', minWidth: '150px', marginTop: '25px' }}>Add actions for Inventory/Consumables</span>
                    { inventorisation.loading ? <div style={{ display: 'inline-block', height: '38px' }}><Spinner classes="smallify" /></div>
                        : (
                            <Select
                                onChange={irem => this.props.history.push(`${this.props.match.path}/${irem.id}`)}
                                options={inventorisation.data}
                                placeholder="Choose Inventory"
                                className="multi multi-large"
                            />)}
                </div>
                <h2 style={{
                    textAlign: 'center', marginBottom: 0, paddingTop: '30px', borderTop: '1px solid lightgrey', marginTop: '20px' }}
                >Inventorisation
                </h2>
                <div className="filter-multi-select">
                    <div style={{ display: 'inline-block' }}>
                        <span style={{ padding: '0 20px', minWidth: '150px', display: 'inline-block' }}>Name</span>
                        { inventorisation.loading ? <div style={{ display: 'inline-block', height: '38px' }}><Spinner classes="smallify" /></div> 
                            : (
                                <Select
                                    isClearable="true"
                                    value={selectedItem}
                                    onChange={inventory => this.handleInventory(inventory)}
                                    options={inventorisation.data}
                                    placeholder="Choose"
                                    className="multi multi-large"
                                />)}
                    </div>
                </div>
                <div className="filter-multi-select">
                    <span style={{ padding: '0 20px', display: 'inline-block', minWidth: '150px' }}>Actions</span>
                    { actionTypes.loading ? <div style={{ display: 'inline-block', height: '38px' }}><Spinner classes="smallify" /></div>
                        : (
                            <Select
                                isMulti="true"
                                value={selectedOption}
                                onChange={this.handleMultiSelect}
                                options={actionTypes.data}
                                placeholder="Choose"
                                className="multi"
                            />)}
                </div>
                <div className="filter-multi-select">
                    <div style={{ display: 'inline-block' }}>
                        <span style={{ padding: '0 20px', display: 'inline-block', minWidth: '150px' }}>Worker</span>
                        { workers.loading ? <div style={{ display: 'inline-block', height: '38px' }}><Spinner classes="smallify" /></div>
                            : (
                                <Select
                                    isClearable="true"
                                    value={selectedWorker}
                                    onChange={this.handleSelectWorkers}
                                    options={workers.data}
                                    placeholder="Choose"
                                    className="multi multi-larger"
                                />)}
                    </div>
                </div>
                <div style={{ display: 'inline-block' }}>
                    <DatepickerIntervalPicker
                        begin_date={begin_date}
                        end_date={end_date}
                        setBeginDate={date => this.setState({ begin_date: date })}
                        setEndDate={date => this.setState({ end_date: date })}
                    />
                </div>
                <div className="filter-multi-select">
                    <div style={{ display: 'inline-block' }}>
                        <span style={{ padding: '0 20px', minWidth: '150px', display: 'inline-block' }}>Created by</span>
                        {/* {users.loading ? <div style={{ display: 'inline-block', height: '38px' }}><Spinner classes="smallify" /></div>
                            : (
                                <DropdownLoader
                                    style={{ display: 'inline-block' }}
                                    data={users.data}
                                    set={user_id => this.setState({ user_filter: user_id })}
                                    title="Choose author"
                                    keys="name"
                                    emptify
                                />)} */}
                    </div>
                </div>
                {/* <ActionsView
                    id={inventory_filter}
                    history={this.props.history}
                    match={this.props.match}
                    selectedOption={selectedOption}
                    worker_filter={worker_filter}
                    user_filter={user_filter}
                    begin_date={begin_date}
                    end_date={end_date}
                    refresh={refresh}
                    showName
                    showInventName
                    reportable
                /> */}
            </div>
        );
    }
}

const mapStateToProps = state => ({
    inventorisation: state.inventorisation,
    invents: state.invents,
    actionTypes: state.actionTypes,
    users: state.users,
    workers: state.workers,
    vehicles: state.vehicles,
    fuel: state.fuel
});

export default connect(mapStateToProps)(Invents);
