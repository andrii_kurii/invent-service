import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {
    Button, Breadcrumb, BreadcrumbItem, Table, InputGroupAddon, InputGroupText, Input, FormGroup, Label,
    Dropdown, DropdownItem, Container, Form, Alert,
    Row, Col, ListGroup, ListGroupItem, DropdownToggle, DropdownMenu
} from 'reactstrap';
import { DropdownLoader, Spinner } from '../../../reusable';
import { TGET, TPOST } from '../../../../api_temp';

class EditInvent extends React.Component {
    static propTypes = {
        history: PropTypes.shape({ push: PropTypes.func.isRequired }).isRequired,
        match: PropTypes.shape({ path: PropTypes.string.isRequired }).isRequired,
        invents: PropTypes.oneOfType([
            PropTypes.string.isRequired,
            PropTypes.arrayOf(PropTypes.shape({}).isRequired).isRequired
        ]).isRequired,
        inventorisation: PropTypes.oneOfType([
            PropTypes.string.isRequired,
            PropTypes.arrayOf(PropTypes.shape({}).isRequired).isRequired
        ]).isRequired
    }
    constructor() {
        super();
        this.state = { warning: '', name: '', invent_type: '', dropdownOpen: false, invents: '', invtory: '' };
        this.nameToggle = this.nameToggle.bind(this);
        this.inventToggle = this.inventToggle.bind(this);
    }
    componentDidMount() {
        this.responseInventorisation();
    }
    componentDidUpdate(prevProps, prevState) {
        if (this.props.inventorisation !== prevProps.inventorisation) {
            this.responseInventorisation();
        }
    }
    responseInventorisation = () => {
        const { match } = this.props;
        const { id } = match.params;
        if (Array.isArray(this.props.inventorisation)) {
            const inventorisation = this.props.inventorisation.filter(item => item.id == id);
            this.setState({ invtory: inventorisation[0], invent_type: { id: inventorisation[0].invent_id, name: inventorisation[0].invent } });
        }
    }
    errInventorisation = (data) => {
        console.log('err:', data);
    }
    nameToggle = (name) => { this.setState({ name: name.target.value }); }
    inventToggle = (invent_type) => { this.setState({ invent_type }); }
    dropdownToggle = () => this.setState(prevState => ({ dropdownOpen: !prevState.dropdownOpen }));
    handleSubmit = (e) => {
        e.preventDefault();
        const { invent_type } = this.state;
        const { match } = this.props;
        const { id } = match.params;
        TPOST('user/update-inventor', { id, invent_id: invent_type }, this.responseUpdateInventor, this.err);
    }
    responseUpdateInventor = (datas) => {
        const { statusCode } = datas;
        const { match } = this.props;
        const { id } = match.params;
        if (statusCode === 200) {
            this.props.history.push(`/user/inventorisation/${id}`);
        }
    }
    errCreateInventor = (data) => { console.log('err:', data); }
    err = (data) => { console.log('err:', data); }
    render() {
        const { history, invents } = this.props;
        const { warning, name, invent_type, invtory } = this.state;
        return (
            <div>
                <Button
                    style={{ margin: '20px auto 20px 20px', display: 'block' }}
                    onClick={() => history.push('/user/inventorisation')}
                > Back
                </Button>
                <h2 style={{ textAlign: 'center', marginTop: '30px' }}>Edit Inventory/Consumables</h2>
                {invtory ? (
                    <Form onSubmit={this.handleSubmit}>
                        <Row>
                            <Col>
                                <ListGroup className="form-titles col-8">
                                    <ListGroupItem>Name</ListGroupItem>
                                    <ListGroupItem>Inventorisation type</ListGroupItem>
                                </ListGroup>
                            </Col>
                            <div className="separator"></div>
                            <Col>
                                <ListGroup className=" form-fields col-8">
                                    <FormGroup className="fields-clear-marbot">
                                        <Input
                                            disabled
                                            type="text"
                                            value={invtory.name}
                                        />
                                    </FormGroup>
                                    <FormGroup className="form-smaller-dropdown" style={{ marginTop: '0.5rem' }}>
                                        { invents && invent_type
                                            ? <DropdownLoader defaultValue={invent_type} data={invents} keys="name" set={this.inventToggle} />
                                            : <Spinner classes="smaller" /> }
                                    </FormGroup>
                                </ListGroup>
                            </Col>
                        </Row>
                        <Button style={{ margin: '20px auto', display: 'block' }} className="add-assistant-submit" type="submit">Save</Button>
                    </Form>
                ) : <Spinner />}                
            </div>
        );
    }
}

const mapStateToProps = state => ({
    inventorisation: state.inventorisation,
    invents: state.invents,
    actionTypes: state.actionTypes,
    users: state.users,
    workers: state.workers
});

const mapDispatchToProps = (dispatch) => {
    return {
        getInventorisationById: data => dispatch(data)
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(EditInvent);
