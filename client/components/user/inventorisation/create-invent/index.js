import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {
    Button, Breadcrumb, BreadcrumbItem, Table, InputGroupAddon, InputGroupText, Input, FormGroup, Label,
    Dropdown, DropdownItem, Container, Form, Alert,
    Row, Col, ListGroup, ListGroupItem, DropdownToggle, DropdownMenu
} from 'reactstrap';
import { DropdownLoader, Spinner } from '../../../reusable';
import { TGET, TPOST } from '../../../../api_temp';
import { inventorisationFetch } from '../../../../redux/actions/with-request/fetch-inventorisation';
// import { inventoryFetch } from '../../../../redux/actions/fetch-inventorisation';

class CreateInvent extends React.Component {
    static propTypes = {
        history: PropTypes.shape({ push: PropTypes.func.isRequired }).isRequired,
        match: PropTypes.shape({ path: PropTypes.string.isRequired }).isRequired,
        invents: PropTypes.shape({}).isRequired
    }
    constructor() {
        super();
        this.state = { warning: '', name: '', invent_type: '', dropdownOpen: false };
        this.nameToggle = this.nameToggle.bind(this);
        this.inventToggle = this.inventToggle.bind(this);
    }
    nameToggle = (name) => { this.setState({ name: name.target.value }); }
    inventToggle = (invent_type) => { this.setState({ invent_type }); }
    dropdownToggle = () => this.setState(prevState => ({ dropdownOpen: !prevState.dropdownOpen }));
    handleSubmit = (e) => {
        e.preventDefault();
        const { name, invent_type } = this.state;
        TPOST('user/create-inventor', { name, invent_type }, this.responseCreateInventor, this.errCreateInventor);
    }
    responseCreateInventor = (datas) => {
        const { statusCode, data } = datas;
        if (statusCode === 200) {
            this.props.history.push('/user/inventorisation');
            inventorisationFetch();
        }
    }
    errCreateInventor = (data) => { console.log('err:', data); }
    render() {
        const { history, invents } = this.props;
        const { warning, name, invent_type } = this.state;
        return (
            <div>
                <Button
                    style={{ margin: '20px auto 20px 20px', display: 'block' }}
                    onClick={() => history.push('/user/inventorisation')}
                > Back
                </Button>
                <h2 style={{ textAlign: 'center', marginTop: '30px' }}>Create new Inventory/Consumables</h2>
                <Form onSubmit={this.handleSubmit}>
                    <Row>
                        <Col>
                            <ListGroup className="form-titles col-8">
                                <ListGroupItem>Name</ListGroupItem>
                                <ListGroupItem>Inventorisation type</ListGroupItem>
                            </ListGroup>
                        </Col>
                        <div className="separator"></div>
                        <Col>
                            <ListGroup className=" form-fields col-8">
                                <FormGroup className="fields-clear-marbot">
                                    <Input
                                        type="text"
                                        value={name}
                                        onChange={this.nameToggle}
                                    />
                                </FormGroup>
                                <FormGroup className="form-smaller-dropdown">
                                    { invents
                                        ? <DropdownLoader data={invents} keys="name" set={this.inventToggle} />
                                        : <Spinner classes="smaller" /> }
                                </FormGroup>
                            </ListGroup>
                        </Col>
                    </Row>
                    {warning
                        ? (
                            <Alert color="warning" className="fields-warning-create">
                                {warning}
                            </Alert>
                        )
                        : ''}
                    { name && invent_type
                        ? <Button style={{ margin: '20px auto', display: 'block' }} className="add-assistant-submit" type="submit">Create</Button>
                        : ''}
                </Form>
            </div>
        );
    }
}
const mapStateToProps = state => ({
    invents: state.invents
});

export default connect(mapStateToProps)(CreateInvent);

// const mapStateToProps = () => ({});

// const mapDispatchToProps = (dispatch) => {
//     return {
//         getInvent: d => dispatch(d)
//     };
// };

// export default connect(mapStateToProps, mapDispatchToProps)(CreateInvent);
