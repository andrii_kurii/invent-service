import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {
    Button, Breadcrumb, BreadcrumbItem, Table, InputGroupAddon, InputGroupText, Input, FormGroup, Label,
    Dropdown, DropdownItem, Container, Form, Alert,
    Row, Col, ListGroup, ListGroupItem, DropdownToggle, DropdownMenu
} from 'reactstrap';
import DatePicker from 'react-datepicker';
import moment from 'moment';
import Select from 'react-select';
import { TGET, TPOST } from '../../../../api_temp';
import { Spinner, DropdownLoader, DatepickerIntervalPicker, NumbersValidationWrapper } from '../../../reusable';

class Event extends React.Component {
    static propTypes = {
        history: PropTypes.shape({ push: PropTypes.func.isRequired }).isRequired,
        match: PropTypes.shape({ path: PropTypes.string.isRequired }).isRequired
    }
    constructor(props) {
        super(props);
        this.state = {
            action_history: '',
            created_at: moment(),
            value: '',
            selectedOption: [],
            selectedVehicle: []
        };
    }
    componentDidMount() {
        this.init();
    }
    init = () => {
        const { match } = this.props;
        const { id } = match.params;
        TGET(`user/vehicles-actions?event_id=${id}`, this.responseEvent, this.err);
        TGET(`user/vehicle-action-history?id=${id}`, this.responseHistory, this.err);
    }
    responseVehicles = (datas) => {
        const { statusCode, data } = datas;
        if (statusCode === 200) {
            let a = {};
            a = data.map((item) => { a = { value: item.id, label: item.numbers, ...item }; return a; });
            this.setState({ vehicles: a });
        }
    }
    responseHistory = (datas) => {
        const { statusCode, data } = datas;
        if (statusCode === 200) {
            this.setState({ action_history: data });
        }
    }
    responseEvent = (datas) => {
        const { statusCode, data } = datas;
        if (statusCode === 200) {
            if (data.length) {
                this.setState({
                    created_at: moment(data[0].created_at),
                    action_type: data[0].action_type,
                    worker: data[0],
                    value: data[0].value,
                    invent_name: data[0].invent_name,
                    selectedOption: { value: data[0].action_type, label: data[0].action_name },
                    selectedVehicle: { value: data[0].vehicle_id, label: data[0].numbers },
                    selectedWorker: data[0].worker_id ? { value: data[0].worker_id, label: data[0].worker_name } : '',
                });
            }
        }
    }
    responseWorkers = (datas) => {
        const { statusCode, data } = datas;
        if (statusCode === 200) {
            let a = {};
            a = data.map((item) => { a = { value: item.id, label: item.name, ...item }; return a; });
            this.setState({ workers: a });
        }
    }
    err = (data) => { console.log('err:', data); }
    responseActionTypes = (datas) => {
        const { statusCode, data } = datas;
        if (statusCode === 200) {
            let a = {};
            a = data.map((item) => { a = { value: item.id, label: item.name, ...item }; return a; });
            this.setState({ action_types: a });
        }
    }
    handleSubmit = (e) => {
        e.preventDefault();
        const { match } = this.props;
        const { id } = match.params;
        const { selectedVehicle, selectedOption, selectedWorker, value } = this.state;
        TPOST('user/update-vehicle-action', {
            vehicle_action_id: id,
            vehicle_id: selectedVehicle.value,
            action_type: selectedOption.value,
            value,
            worket_id: selectedWorker ? selectedWorker.value : ''
        }, this.responseUpdateAction, this.err);
    }
    responseUpdateAction = (datas) => {
        const { statusCode, data } = datas;
        if (statusCode === 200) {
            this.setState({ action_history: '' }, () => this.init());
        }
    }
    handleMultiSelect = (selectedOption) => {
        this.setState({ selectedOption });
    }
    handleSelectedVehicle = (selectedVehicle) => {
        this.setState({ selectedVehicle });
    }
    handleSelectedWorker = (selectedWorker) => {
        this.setState({ selectedWorker });
    }
    renderHistory = () => {
        const { action_history } = this.state;
        if (action_history) {
            return action_history.map((item, i) => {
                const { id, value, invent_name, action_name, numbers, worker_name, created_at, creator_name } = item;
                return (
                    <tr key={i}>
                        <td>{i + 1}</td>
                        <td>{action_name}</td>
                        <td>{numbers}</td>
                        <td>{value}</td>
                        <td>{worker_name || '—' }</td>
                        <td>{moment(created_at).format('lll:ss')}</td>
                        <td>{creator_name}</td>
                        {/* <td className="pen" onClick={() => this.props.history.push(`/user/inventorisation/events/${id}`)}> &#x270E; </td> */}
                    </tr>
                );
            });
        }
        return null;
    }
    render() {
        const { history, workers, actionTypes, vehicles } = this.props;
        const { value, created_at, action_history } = this.state;
        const {
            selectedOption, selectedWorker, selectedVehicle
        } = this.state;
        return (
            <div>
                <Button style={{ margin: '20px auto 0 0', display: 'block' }} onClick={() => history.push('/user/vehicles')}>
                    Назад
                </Button>
                <h2 style={{ textAlign: 'center', marginTop: '30px' }}>Edit action</h2>
                <Form onSubmit={this.handleSubmit} noValidate>
                    <Row>
                        <Col>
                            <ListGroup className="form-titles col-8">
                                <ListGroupItem>Actione date</ListGroupItem>
                                <ListGroupItem>Action</ListGroupItem>
                                <ListGroupItem>Vehicle number</ListGroupItem>
                                <ListGroupItem>Worker</ListGroupItem>
                                <ListGroupItem>Volume in liters</ListGroupItem>
                            </ListGroup>
                        </Col>
                        <div className="separator"></div>
                        <Col>
                            <ListGroup className=" form-fields col-8">
                                <FormGroup className="fields-clear-marbot">                                    
                                    {created_at ? (
                                        <DatePicker
                                            disabled
                                            className="form-control"
                                            selected={created_at}
                                            dateFormat="lll:ss"
                                            locale="en"
                                            showTimeSelect
                                            timeCaption="Time"
                                            timeFormat="HH:mm"
                                            timeIntervals={1}
                                        />
                                    ) : <div style={{ display: 'inline-block', height: '38px' }}><Spinner classes="smallify" /></div> }
                                </FormGroup>
                            </ListGroup>
                            <ListGroup style={{ marginTop: '.75em' }} className=" form-fields col-8">
                                <FormGroup className="fields-clear-marbot">
                                    { actionTypes ? (
                                        <Select
                                            value={selectedOption}
                                            onChange={this.handleMultiSelect}
                                            options={actionTypes}
                                            placeholder="Choose"
                                            className="multi"
                                        />) : <div style={{ display: 'inline-block', height: '38px' }}><Spinner classes="smallify" /></div> }
                                </FormGroup>
                            </ListGroup>
                            <ListGroup style={{ marginTop: '.75em' }} className=" form-fields col-8">
                                <FormGroup className="fields-clear-marbot">
                                    { vehicles ? (
                                        <Select
                                            value={selectedVehicle}
                                            onChange={this.handleSelectedVehicle}
                                            options={vehicles}
                                            placeholder="Choose"
                                            className="multi"
                                        />) : <div style={{ display: 'inline-block', height: '38px' }}><Spinner classes="smallify" /></div> }
                                </FormGroup>
                            </ListGroup>
                            <ListGroup style={{ marginTop: '.75em' }} className=" form-fields col-8">
                                <FormGroup className="fields-clear-marbot">
                                    { workers ? (
                                        <Select
                                            isClearable
                                            value={selectedWorker}
                                            onChange={this.handleSelectedWorker}
                                            options={workers}
                                            placeholder="Оберіть"
                                            className="multi"
                                        />) : <div style={{ display: 'inline-block', height: '38px' }}><Spinner classes="smallify" /></div> }
                                </FormGroup>
                            </ListGroup>
                            <ListGroup style={{ marginTop: '.75em' }} className=" form-fields col-8">
                                <FormGroup className="fields-clear-marbot">                          
                                    { value ? (
                                        <NumbersValidationWrapper value={value} type="Litrs" func={rr => this.setState({ value: rr})} />
                                    ): <div style={{ display: 'inline-block', height: '38px' }}><Spinner classes="smallify" /></div> }
                                </FormGroup>
                            </ListGroup>
                        </Col>
                    </Row>
                    {/* {warning
                        ? (
                            <Alert color="warning" className="fields-warning-create">
                                {warning}
                            </Alert>
                        )
                        : ''} */}
                    { selectedOption && selectedVehicle && value && value != 0
                        ? <Button style={{ margin: '20px auto', display: 'block' }} className="add-assistant-submit" type="submit">Change</Button>
                        : ''}
                </Form>
                {
                    action_history
                        ? action_history.length
                            ? (
                                <div>
                                    <h2 style={{ textAlign: 'center', marginTop: '30px' }}>Changes history</h2>
                                    <Table className="table-editor-helper">
                                        <thead>
                                            <tr>
                                                <th> </th>
                                                <th>Action</th>
                                                <th>Number</th>
                                                <th>Dimension</th>
                                                <th>Worker</th>
                                                <th>Created at</th>
                                                <th>Created by</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {this.renderHistory()}
                                        </tbody>
                                    </Table>
                                </div>
                            )
                            : ''
                        : <Spinner />
                }
            </div>
        );
    }
}

const mapStateToProps = state => ({
    invents: state.invents,
    actionTypes: state.actionTypes,
    users: state.users,
    workers: state.workers,
    fuel: state.fuel,
    vehicles: state.vehicles
});

export default connect(mapStateToProps)(Event);
