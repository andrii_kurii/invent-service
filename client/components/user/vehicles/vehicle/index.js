import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import { connect } from 'react-redux';
import { Button, Table } from 'reactstrap';

import { Spinner, ActionForm, VehiclesActionsView } from '../../../reusable';
import './styles.css';


class Vehicle extends React.Component {
    static propTypes = {
        history: PropTypes.shape({ push: PropTypes.func.isRequired }).isRequired,
        match: PropTypes.shape({ path: PropTypes.string.isRequired }).isRequired,
        vehicles: PropTypes.oneOfType([
            PropTypes.string.isRequired,
            PropTypes.arrayOf(PropTypes.shape({}).isRequired).isRequired
        ]).isRequired
    }
    constructor() {
        super();
        this.state = {
            refresh: false
        };
    }
    renderCurrentVehicle = () => {
        const { match, vehicles } = this.props;
        const { id } = match.params;
        if (Array.isArray(vehicles)) {
            const el = vehicles.filter((item) => { return item.id == id; });
            if (el.length) {
                return el[0];
            }
        }
        return {};
    }
    renderCurrentVehicleBody = () => {
        const { match } = this.props;
        const { id } = match.params;
        const el = this.renderCurrentVehicle();
        if (el) {
            const { mark, numbers, fuel_limit, fuel, created_at, creator_name } = el;
            return (
                <tr>
                    <td>{mark}</td>
                    <td>{numbers}</td>
                    <td>{fuel}</td>
                    <td>{fuel_limit || '—'}</td>
                    <td>{moment(created_at).format('lll:ss')}</td>
                    <td>{creator_name}</td>
                    <td className="pen" onClick={() => this.props.history.push(`${id}/edit`)}> &#x270E; </td>
                </tr>
            );
        }
        return null;
    }
    render() {
        const { match, vehicles } = this.props;
        const { id } = match.params;
        const { refresh } = this.state;
        console.log(this.renderCurrentVehicle());
        return (
            <div className="purchase">
                <Button
                    style={{ margin: '20px auto 20px 20px', display: 'block' }}
                    onClick={() => this.props.history.push('/user/vehicles')}
                > Back
                </Button>
                <h2 style={{ textAlign: 'center', marginTop: '0', marginBottom: '10px' }}>Current vehicle</h2>
                { vehicles
                    ? (
                        <Table className="table-editor-helper">
                            <thead>
                                <tr>
                                    <th>Mark</th>
                                    <th>Number</th>
                                    <th>Fuel type</th>
                                    <th>Fuel limit</th>
                                    <th>Created at</th>
                                    <th>Created by</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                {this.renderCurrentVehicleBody()}
                            </tbody>
                        </Table>) : ''}
                { vehicles
                    ? (
                        <ActionForm
                            refresh={() => this.setState({ refresh: !refresh })}
                            url="user/vehicles"
                            id={id}
                            keys="vehicle_id"
                            type="Litrs"
                            element={vehicles}
                        />
                    )
                    : <Spinner />
                }
                <h2 style={{ textAlign: 'center', marginTop: '30px' }}>Working with fuel</h2>
                <VehiclesActionsView
                    itemFilter={id}
                    history={this.props.history}
                    match={this.props.history}
                    refresh={this.state.refresh}
                    showName
                    showInventName
                />
            </div>
        );
    }
}

const mapStateToProps = state => ({
    vehicles: state.vehicles,
    invents: state.invents,
    actionTypes: state.actionTypes,
    users: state.users,
    workers: state.workers
});

export default connect(mapStateToProps)(Vehicle);
