import React from 'react';
import PropTypes from 'prop-types';
import {
    Button, Breadcrumb, BreadcrumbItem, Table, InputGroupAddon, InputGroupText, Input, FormGroup, Label,
    Dropdown, DropdownItem, Container, Form, Alert,
    Row, Col, ListGroup, ListGroupItem, DropdownToggle, DropdownMenu
} from 'reactstrap';
import { DropdownLoader, Spinner, NumbersValidationWrapper } from '../../../reusable';
import { TGET, TPOST } from '../../../../api_temp';

export default class EditVehicle extends React.Component {
    static propTypes = {
        history: PropTypes.shape({ push: PropTypes.func.isRequired }).isRequired,
        match: PropTypes.shape({ path: PropTypes.string.isRequired }).isRequired
    }
    constructor() {
        super();
        this.state = { warning: '', dropdownOpen: false, invents: '', fuel: '', vehicle: '', markValue: '', numbersValue: '', fuelType: '', fuelLimit: '' };
        this.markToggle = this.markToggle.bind(this);
        this.fuelToggle = this.fuelToggle.bind(this);
        this.limitToggle = this.limitToggle.bind(this);
    }
    componentDidMount() {
        const { match } = this.props;
        const { id } = match.params;
        TGET(`user/vehicles?vehicle_id=${id}`, datas => this.feel('vehicle', datas), this.err);
        TGET('user/fuel', datas => this.response('fuel', datas), this.err);
    }
    feel = (key, datas) => {
        const { statusCode, data } = datas;
        if (statusCode === 200 && key) {
            this.setState({
                markValue: data[0].mark,
                numbersValue: data[0].numbers,
                fuelType: {id: data[0].fuel_id, name: data[0].fuel},
                fuelLimit:  data[0].fuel_limit || ''
            })
        }
        this.response(key, datas);

    }
    response = (key, datas) => {
        const { statusCode, data } = datas;
        if (statusCode === 200 && key) {
            this.setState({ [key]: data });
        }
    }
    responseInventorisation = (datas) => {
        const { statusCode, data } = datas;
        if (statusCode === 200) {
            this.setState({ invent_type: { id: data[0].invent_id, name: data[0].invent } });
        }
    }
    errInventorisation = (data) => {
        console.log('err:', data);
    }
    responseInventTypes = (datas) => {
        const { statusCode, data } = datas;
        if (statusCode === 200) {
            this.setState({ invents: data });
        }
    }
    errInventTypes = (data) => { console.log('err:', data); }
    markToggle = (markValue) => { this.setState({ markValue: markValue.target.value }); }
    limitToggle = (fuelLimit) => { this.setState({ fuelLimit }); }
    fuelToggle = (fuelType) => { this.setState({ fuelType }); }
    dropdownToggle = () => this.setState(prevState => ({ dropdownOpen: !prevState.dropdownOpen }));
    handleSubmit = (e) => {
        e.preventDefault();
        const { markValue, fuelLimit, fuelType } = this.state;
        const { match } = this.props;
        const { id } = match.params;
        TPOST('user/update-vehicle', { id, mark: markValue, fuel_id: fuelType, fuel_limit: fuelLimit == 0 ? '' : fuelLimit }, this.responseUpdateInventor, this.err);
    }
    responseUpdateInventor = (datas) => {
        const { statusCode } = datas;
        const { match } = this.props;
        const { id } = match.params;
        if (statusCode === 200) {
            this.props.history.push(`/user/vehicles/${id}`);
        }
    }
    errCreateInventor = (data) => { console.log('err:', data); }
    err = (data) => { console.log('err:', data); }
    render() {
        const { history } = this.props;
        const { warning, name, invent_type, invents, fuel, vehicle, markValue, numbersValue, fuelLimit, fuelType } = this.state;
        return (
            <div>
                <Button
                    style={{ margin: '20px auto 20px 20px', display: 'block' }}
                    onClick={() => history.push('/user/vehicles')}
                > Back
                </Button>
                <h2 style={{ textAlign: 'center', marginTop: '30px' }}>Edit vehicle</h2>
                {vehicle ? (
                    <Form onSubmit={this.handleSubmit} noValidate>
                        <Row>
                            <Col>
                                <ListGroup className="form-titles col-8">
                                    <ListGroupItem>Mark</ListGroupItem>
                                    <ListGroupItem>Number</ListGroupItem>
                                    <ListGroupItem>Fuel type</ListGroupItem>
                                    <ListGroupItem>Fuel limit</ListGroupItem>
                                </ListGroup>
                            </Col>
                            <div className="separator"></div>
                            <Col>
                                <ListGroup className=" form-fields col-8">
                                    <FormGroup className="fields-clear-marbot" style={{ marginBottom: '1.5rem '}}>
                                        <Input
                                            type="text"
                                            value={markValue}
                                            onChange={this.markToggle}
                                        />
                                    </FormGroup>
                                    <FormGroup className="fields-clear-marbot" style={{ marginBottom: '1.5rem '}}>
                                        <Input
                                            disabled
                                            type="text"
                                            value={numbersValue}
                                        />
                                    </FormGroup>
                                    <FormGroup className="form-smaller-dropdown" style={{ marginBottom: '1.5rem '}}>
                                        { fuel && fuelType
                                            ? <DropdownLoader defaultValue={fuelType} data={fuel} keys="name" set={this.fuelToggle} />
                                            : <Spinner classes="smaller" /> }
                                    </FormGroup>
                                    <FormGroup className="fields-clear-marbot">
                                        <NumbersValidationWrapper value={fuelLimit} type="Litrs" func={rr => this.limitToggle(rr)} />
                                    </FormGroup>
                                </ListGroup>
                            </Col>
                        </Row>
                        {markValue ? <Button style={{ margin: '20px auto', display: 'block' }} className="add-assistant-submit" type="submit">Save</Button> : ''}
                    </Form>
                ) : <Spinner />}
            </div>
        );
    }
}
