import Vehicles from './vehicles';
import Vehicle from './vehicle';
import CreateVehicle from './create-vehicle';
import EditVehicle from './edit';
import Event from './event';

export {
    Vehicles,
    Vehicle,
    CreateVehicle,
    EditVehicle,
    Event
};
