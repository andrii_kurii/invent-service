import React from 'react';
import PropTypes, { number } from 'prop-types';
import { connect } from 'react-redux';
import { Button, Input, FormGroup, Form, Alert, Row, Col, ListGroup, ListGroupItem
} from 'reactstrap';
import { DropdownLoader, Spinner, DatepickerIntervalPicker } from '../../../reusable';
import { TGET, TPOST } from '../../../../api_temp';
import './styles.css';
import { vehiclesFetch } from '../../../../redux/actions/with-request/fetch-vehicles';

class CreateVehicle extends React.Component {
    static propTypes = {
        history: PropTypes.shape({ push: PropTypes.func.isRequired }).isRequired,
        match: PropTypes.shape({ path: PropTypes.string.isRequired }).isRequired
    }
    constructor() {
        super();
        this.state = {
            warning: '',
            mark: '',
            numbers: '',
            fuel_id: ''
        };
        this.markHandle = this.markHandle.bind(this);
        this.numbersHandle = this.numbersHandle.bind(this);
    }
    markHandle = (name) => { this.setState({ mark: name.target.value }); }
    numbersHandle = (name) => { this.setState({ numbers: name.target.value }); }
    handleSubmit = (e) => {
        e.preventDefault();
        const { mark, numbers, fuel_id } = this.state;
        TPOST('user/create-vehicle', { mark, numbers, fuel_id }, this.responseCreateVehicle, this.err);
    }
    responseCreateVehicle = (datas) => {
        const { statusCode } = datas;
        if (statusCode === 200) {
            this.props.history.push('/user/vehicles');
            vehiclesFetch();
        }
    }
    err = (data) => { console.log('err:', data); }
    render() {
        const { history, fuel } = this.props;
        const { warning, mark, numbers, fuel_id } = this.state;
        return (
            <div className="create-vehicle-fix-labels">
                <Button
                    style={{ margin: '20px auto 20px 20px', display: 'block' }}
                    onClick={() => history.push('/user/vehicles')}
                > Back
                </Button>
                <h2 style={{ textAlign: 'center', marginTop: '30px' }}>Create new vehicle</h2>
                <Form onSubmit={this.handleSubmit}>
                    <Row>
                        <Col>
                            <ListGroup className="form-titles col-8">
                                <ListGroupItem>Mark</ListGroupItem>
                                <ListGroupItem>Number</ListGroupItem>
                                <ListGroupItem>Fuel type</ListGroupItem>
                            </ListGroup>
                        </Col>
                        <div className="separator"></div>
                        <Col>
                            <ListGroup className=" form-fields col-8">
                                <FormGroup>
                                    <Input
                                        type="text"
                                        value={mark}
                                        onChange={this.markHandle}
                                    />
                                </FormGroup>
                                <FormGroup>
                                    <Input
                                        type="text"
                                        value={numbers}
                                        onChange={this.numbersHandle}
                                    />
                                </FormGroup>
                                <FormGroup className="form-smaller-dropdown">
                                    { fuel
                                        ? <DropdownLoader data={fuel} keys="name" set={id => this.setState({ fuel_id: id })} />
                                        : <Spinner classes="smaller" /> }
                                </FormGroup>
                            </ListGroup>
                        </Col>
                    </Row>
                    {warning
                        ? (
                            <Alert color="warning" className="fields-warning-create">
                                {warning}
                            </Alert>
                        )
                        : ''}
                    { mark && numbers && fuel_id
                        ? <Button style={{ margin: '20px auto', display: 'block' }} className="add-assistant-submit" type="submit">Create</Button>
                        : ''}
                </Form>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    fuel: state.fuel
});

export default connect(mapStateToProps)(CreateVehicle);
