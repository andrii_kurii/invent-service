import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Button, Table } from 'reactstrap';
import moment from 'moment';
import Select from 'react-select';
import { TGET } from '../../../../api_temp';
import { Spinner, DropdownLoader, DatepickerIntervalPicker, VehiclesActionsView, parseForSelect } from '../../../reusable';

class Vehicles extends React.Component {
    static propTypes = {
        history: PropTypes.shape({ push: PropTypes.func.isRequired }).isRequired,
        match: PropTypes.shape({ path: PropTypes.string.isRequired }).isRequired,
        users: PropTypes.oneOfType([
            PropTypes.string.isRequired,
            PropTypes.arrayOf(PropTypes.shape({}).isRequired).isRequired
        ]).isRequired,
        workers: PropTypes.oneOfType([
            PropTypes.string.isRequired,
            PropTypes.arrayOf(PropTypes.shape({}).isRequired).isRequired
        ]).isRequired,
        actionTypes: PropTypes.oneOfType([
            PropTypes.string.isRequired,
            PropTypes.arrayOf(PropTypes.shape({}).isRequired).isRequired
        ]).isRequired,
        fuel: PropTypes.oneOfType([
            PropTypes.string.isRequired,
            PropTypes.arrayOf(PropTypes.shape({}).isRequired).isRequired
        ]).isRequired,
        vehicles: PropTypes.oneOfType([
            PropTypes.string.isRequired,
            PropTypes.arrayOf(PropTypes.shape({}).isRequired).isRequired
        ]).isRequired
    }
    constructor(props) {
        super(props);
        this.state = {
            vehicles: '',
            vehicleFilter: '',
            eventsFilter: '',
            fuelFilter: '',
            userFilter: '',
            workerFilter: '',
            beginDate: moment().startOf('month'),
            endDate: moment().endOf('month'),

            selectedVehicle: '',
            selectedEvent: '',
            selectedNumber: '',
            selectedFuel: '',
            selectedWorker: ''
        };
    }
    renderVehicles = () => {
        const { vehicles, page } = this.state;
        if (vehicles) {
            return vehicles.map((item, i) => {
                const { mark, numbers, fuel, created_at, creator_name } = item;
                return (
                    <tr key={i}>
                        <td>{(page - 1) * 10 + i + 1}</td>
                        <td>{mark}</td>
                        <td>{numbers || '—'}</td>
                        <td>{fuel}</td>
                        <td>{creator_name}</td>
                        <td>{moment(created_at).format('lll:ss')}</td>
                    </tr>
                );
            });
        }
        return null;
    }
    getPage = (page) => { const { end } = this.state; if (page > 0 && page <= end) { this.setState({ page }); } }
    handleMySelector = (pkey, fkey, skey) => {
        if (fkey !== null) {
            this.setState({ [pkey]: fkey, [skey]: fkey.id });
        } else {
            this.setState({ [pkey]: fkey, [skey]: '' });
        }
    }
    render() {
        const { match, history, users, workers, actionTypes, fuel, vehicles } = this.props;
        const {
            selectedVehicle, selectedEvent, selectedNumber, selectedFuel, selectedWorker, beginDate, endDate,
            vehicleFilter, fuelFilter, userFilter, workerFilter
        } = this.state;
        return (
            <div>
                <Button
                    style={{ margin: '20px 20px 0 auto', display: 'block' }}
                    onClick={() => history.push(`${match.path}/create-vehicle`)}
                > Created vehicle
                </Button>
                <div style={{ display: 'inline-block' }}>
                    <span style={{ padding: '0 20px', display: 'inline-block', minWidth: '150px' }}>Actions for vehicles</span>
                    { vehicles ? (
                        <Select
                            value={selectedVehicle}
                            onChange={item => this.props.history.push(`${this.props.match.path}/${item.id}`)}
                            options={parseForSelect(vehicles, 'id', 'numbers')}
                            placeholder="Choose number"
                            className="multi multi-large"
                        />) : <div style={{ display: 'inline-block', height: '38px' }}><Spinner classes="smallify" /></div> }
                </div>
                <h2 style={{
                    textAlign: 'center', marginTop: '30px', marginBottom: '20px', paddingTop: '30px', borderTop: '1px solid lightgrey'
                }}
                > Vehicles
                </h2>
                <div className="filter-multi-select">
                    <div style={{ display: 'inline-block' }}>
                        <span style={{ padding: '0 20px', minWidth: '230px', display: 'inline-block' }}>Vehicle number</span>
                        { vehicles ? (
                            <Select
                                isClearable="true"
                                value={selectedNumber}
                                onChange={item => this.handleMySelector('selectedNumber', item, 'vehicleFilter')}
                                options={parseForSelect(vehicles, 'id', 'numbers')}
                                placeholder="mark"
                                className="multi multi-large"
                            />) : <div style={{ display: 'inline-block', height: '38px' }}><Spinner classes="smallify" /></div> }
                    </div>
                </div>
                <div className="filter-multi-select">
                    <div style={{ display: 'inline-block' }}>
                        <span style={{ padding: '0 20px', minWidth: '230px', display: 'inline-block' }}>Actions</span>
                        { actionTypes ? (
                            <Select
                                isMulti
                                value={selectedEvent}
                                // onChange={item => this.handleMySelector('selectedEvent', item, 'eventsFilter')}
                                onChange={arr => this.setState({ selectedEvent: arr })}
                                options={parseForSelect(actionTypes, 'id', 'name')}
                                placeholder="Number"
                                className="multi multi-large"
                            />) : <div style={{ display: 'inline-block', height: '38px' }}><Spinner classes="smallify" /></div> }
                    </div>
                </div>
                <div className="filter-multi-select">
                    <div style={{ display: 'inline-block' }}>
                        <span style={{ padding: '0 20px', minWidth: '230px', display: 'inline-block' }}>Fuel type</span>
                        { fuel ? (
                            <Select
                                isClearable="true"
                                value={selectedFuel}
                                onChange={item => this.handleMySelector('selectedFuel', item, 'fuelFilter')}
                                options={parseForSelect(fuel, 'id', 'name')}
                                placeholder="Fuel type"
                                className="multi multi-large"
                            />) : <div style={{ display: 'inline-block', height: '38px' }}><Spinner classes="smallify" /></div> }
                    </div>
                </div>
                <div className="filter-multi-select">
                    <div style={{ display: 'inline-block' }}>
                        <span style={{ padding: '0 20px', minWidth: '230px', display: 'inline-block' }}>Worker</span>
                        { workers ? (
                            <Select
                                isClearable="true"
                                value={selectedWorker}
                                onChange={item => this.handleMySelector('selectedWorker', item, 'workerFilter')}
                                options={parseForSelect(workers, 'id', 'name')}
                                placeholder="Worker"
                                className="multi multi-large"
                            />) : <div style={{ display: 'inline-block', height: '38px' }}><Spinner classes="smallify" /></div> }
                    </div>
                </div>
                <div style={{ display: 'inline-block' }}>
                    <DatepickerIntervalPicker
                        // title="Початкова дата закупівель"
                        begin_date={beginDate}
                        end_date={endDate}
                        setBeginDate={date => this.setState({ beginDate: date })}
                        setEndDate={date => this.setState({ endDate: date })}
                    />
                </div>
                <div className="filter-multi-select">
                    <div style={{ display: 'inline-block' }}>
                        <span style={{ padding: '0 20px', minWidth: '230px', display: 'inline-block' }}>Created by</span>
                        {users
                            ? (
                                <DropdownLoader
                                    style={{ display: 'inline-block' }}
                                    data={users}
                                    set={user_id => this.setState({ userFilter: user_id })}
                                    title="Choose vehicle"
                                    keys="name"
                                    emptify
                                />)
                            : <div style={{ display: 'inline-block', height: '38px' }}><Spinner classes="smallify" /></div>
                        }
                    </div>
                </div>
                <VehiclesActionsView
                    history={this.props.history}
                    match={this.props.match}
                    itemFilter={vehicleFilter}
                    eventsFilter={selectedEvent}
                    fuelFilter={fuelFilter}
                    workerFilter={workerFilter}
                    userFilter={userFilter}
                    beginDate={beginDate}
                    endDate={endDate}
                    showName
                    showInventName
                    reportable
                />
            </div>
        );
    }
}

const mapStateToProps = state => ({
    inventorisation: state.inventorisation,
    invents: state.invents,
    actionTypes: state.actionTypes,
    users: state.users,
    workers: state.workers,
    fuel: state.fuel,
    vehicles: state.vehicles
});

export default connect(mapStateToProps)(Vehicles);
