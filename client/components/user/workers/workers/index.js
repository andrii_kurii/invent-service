import React from 'react';
import PropTypes from 'prop-types';
import { Button, Table, Form, Input } from 'reactstrap';
import moment from 'moment';
import ToggleButton from 'react-toggle-button'

import { TGET, TPOST } from '../../../../api_temp';
import { Spinner, MyPagination } from '../../../reusable';

export default class Workers extends React.Component {
    static propTypes = {
        history: PropTypes.shape({ push: PropTypes.func.isRequired }).isRequired,
        match: PropTypes.shape({ path: PropTypes.string.isRequired }).isRequired
    }
    constructor(props) {
        super(props);
        this.state = { workers: '', filter: '', page: 1, limit: 10, end: '' };
    }
    componentDidMount() {
        this.init();
    }
    init = () => {
        const { filter, page, limit } = this.state;
        // this.setState({  workers: '', filter: '' })
        TGET(`user/workers?${filter ? `&user_filter=${filter}` : ''}${page ? `&page=${page}${limit ? `&limit=${limit}` : ''}` : ''}`, this.responseWorkers, this.err);
    }
    responseWorkers = (datas) => {
        const { statusCode, data, end } = datas;
        if (statusCode === 200) {
            this.setState({ workers: data, end: parseInt(end, 10) });
        }
    }
    err = (data) => {
        console.log('err:', data);
    }
    submitActivity = (id, active) => {
        TPOST('user/update-worker-activity', { id, active }, this.responseActivity, this.err);
    }
    responseActivity = (datas) => {
        const { statusCode } = datas;
        if (statusCode === 200) {
            this.init();
        }
    }
    reset = (time) => {
        clearTimeout(this.ref);
        this.ref = setTimeout(() => { this.setState({ workers: '' }); this.init(); this.ref = undefined }, time || 750);
    }
    renderWorkers = () => {
        const { workers, page } = this.state;
        if (workers) {
            return workers.map((item, i) => {
                const { id, name, post, description, created_at, creator_name, active } = item;
                return (
                    <tr key={i}>
                        <td>{ (page - 1) * 10 + i + 1}</td>
                        <td>{name}</td>
                        <td>{post}</td>
                        <td>{description}</td>
                        <td>{moment(created_at).format('lll:ss')}</td>
                        <td>{creator_name}</td>
                        <td>
                            <Form>
                                <ToggleButton
                                    type="submit"
                                    value={active || false}
                                    inactiveLabel="No"
                                    activeLabel="Yes"
                                    onToggle={() => this.submitActivity(id, !active)}
                                />
                            </Form>
                        </td>
                        <td
                            className="pen"
                            onClick={() => {
                                if (active) {
                                    this.props.history.push(`${this.props.match.path}/${id}`);
                                }
                            }}
                        >
                          &#x270E;
                        </td>
                    </tr>
                );
            });
        }
        return null;
    }
    render() {
        const { match, history } = this.props;
        const { workers } = this.state;
        return (
            <div>
                <Button
                    style={{ margin: '20px 20px 20px auto', display: 'block' }}
                    onClick={() => history.push(`${match.path}/create-worker`)}
                > Create Worker
                </Button>
                <h2 style={{ textAlign: 'center', marginTop: '30px', marginBottom: '20px' }}>Worker</h2>
                <Input
                    style={{ width: '300px', marginBottom: '10px' }}
                    placeholder="Search by name"
                    onChange={value => this.setState({ filter: value.target.value }, () => this.reset())}
                />
                { workers
                    ? (
                        workers.length
                            ? (
                                <div>
                                    <Table className="table-editor-helper">
                                        <thead>
                                            <tr>
                                                <th> </th>
                                                <th>Full name</th>
                                                <th>Position</th>
                                                <th>Additional information</th>
                                                <th>Created at</th>
                                                <th>Created by</th>
                                                <th>Operational</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {this.renderWorkers()}
                                        </tbody>
                                    </Table>
                                    <MyPagination
                                        end={this.state.end}
                                        page={this.state.page}
                                        getPage={(p) => this.setState({ page: p }, () => this.init())}
                                    />
                                </div>)
                            : <h3 style={{ textAlign: 'center' }}>No workers yes</h3>
                    )
                    : <Spinner />
                }
            </div>
        );
    }
}
