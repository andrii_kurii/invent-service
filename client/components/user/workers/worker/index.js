import React from 'react';
import PropTypes from 'prop-types';
import { Button, Form, Row, Col, ListGroup, ListGroupItem, FormGroup, Input, Alert } from 'reactstrap';
import moment from 'moment';
import { TGET, TPOST } from '../../../../api_temp';
import { Spinner } from '../../../reusable';

export default class Worker extends React.Component {
    static propTypes = {
        history: PropTypes.shape({ push: PropTypes.func.isRequired }).isRequired,
        match: PropTypes.shape({ path: PropTypes.string.isRequired }).isRequired
    }
    constructor() {
        super();
        this.state = { warning: '', name: '', post: '', desc: '', worker: '' };
        this.nameToggle = this.nameToggle.bind(this);
        this.postToggle = this.postToggle.bind(this);
        this.descToggle = this.descToggle.bind(this);
    }
    componentDidMount() {
        this.init();
    }
    init = () => {        
        const { match } = this.props;
        const { id } = match.params;
        TGET(`user/workers?id=${id}&active=true`, this.response, this.err);
    }
    nameToggle = (name) => { this.setState({ name: name.target.value, warning: '' }); }
    postToggle = (post) => { this.setState({ post: post.target.value, warning: '' }); }
    descToggle = (desc) => { this.setState({ desc: desc.target.value, warning: '' }); }
    response = (datas) => {
        const { data, statusCode } = datas;
        if (statusCode === 200) {
            if (data.length) {
                this.setState({
                    worker: data[0],
                    name: data[0].name,
                    post: data[0].post,
                    desc: data[0].description
                });
            }
        }
    }
    err = (datas) => { console.log(datas)}
    handleSubmit = (e) => {
        e.preventDefault();
        const { name, post, desc } = this.state;
        const { match } = this.props;
        const { id } = match.params;
        TPOST('user/update-worker', { id, name, post, desc }, this.responseUpdateWorker, this.err);
    }
    responseUpdateWorker = (datas) => {
        const { statusCode } = datas;
        if (statusCode === 200) {
            this.props.history.push('/user/workers');
        } else if (statusCode === 401) {
            this.setState({ warning: 'Worker with such name is already exist' });
        }
    }
    render() {
        const { history } = this.props;
        const { warning, name, post, desc, worker } = this.state;
        return (
            <div>
                <Button
                    style={{ margin: '20px auto 20px 20px', display: 'block' }}
                    onClick={() => history.push('/user/workers')}
                > Назад
                </Button>
                <h2 style={{ textAlign: 'center', marginTop: '30px' }}>Edit worker</h2>
                { worker
                    ? (
                        <Form onSubmit={this.handleSubmit}>
                            <Row>
                                <Col>
                                    <ListGroup className="form-titles col-8">
                                        <ListGroupItem>Name</ListGroupItem>
                                        <ListGroupItem>Position</ListGroupItem>
                                        <ListGroupItem>Additional infotmation</ListGroupItem>
                                    </ListGroup>
                                </Col>
                                <div className="separator"></div>
                                <Col>
                                    <ListGroup className=" form-fields col-8">
                                        <FormGroup>
                                            <Input
                                                type="text"
                                                value={name}
                                                onChange={this.nameToggle}
                                            />
                                        </FormGroup>
                                        <FormGroup>
                                            <Input
                                                type="text"
                                                value={post}
                                                onChange={this.postToggle}
                                            />
                                        </FormGroup>
                                        <FormGroup>
                                            <Input
                                                type="textarea"
                                                value={desc}
                                                onChange={this.descToggle}
                                            />
                                        </FormGroup>
                                    </ListGroup>
                                </Col>
                            </Row>
                            {warning
                                ? (
                                    <Alert color="warning" className="fields-warning-create">
                                        {warning}
                                    </Alert>
                                )
                                : ''}
                            { name && post
                                ? <Button style={{ margin: '20px auto', display: 'block' }} className="add-assistant-submit" type="submit">Submit</Button>
                                : ''}
                        </Form>
                    )
                    : <Spinner />
                }
            </div>
        );
    }
}
