import React from 'react';
import PropTypes from 'prop-types';
import {
    Button, Breadcrumb, BreadcrumbItem, Table, InputGroupAddon, InputGroupText, Input, FormGroup, Label,
    Dropdown, DropdownItem, Container, Form, Alert,
    Row, Col, ListGroup, ListGroupItem, DropdownToggle, DropdownMenu
} from 'reactstrap';
import { DropdownLoader, Spinner } from '../../../reusable';
import { TGET, TPOST } from '../../../../api_temp';
import { workersFetch } from '../../../../redux/actions/with-request/fetch-workers';

export default class CreateWorker extends React.Component {
    static propTypes = {
        history: PropTypes.shape({ push: PropTypes.func.isRequired }).isRequired,
        match: PropTypes.shape({ path: PropTypes.string.isRequired }).isRequired
    }
    constructor() {
        super();
        this.state = { warning: '', name: '', post: '', desc: '' };
        this.nameToggle = this.nameToggle.bind(this);
        this.postToggle = this.postToggle.bind(this);
        this.descToggle = this.descToggle.bind(this);
    }
    nameToggle = (name) => { this.setState({ name: name.target.value }); }
    postToggle = (post) => { this.setState({ post: post.target.value }); }
    descToggle = (desc) => { this.setState({ desc: desc.target.value }); }

    handleSubmit = (e) => {
        e.preventDefault();
        const { name, post, desc } = this.state;
        TPOST('user/create-worker', { name, post, desc }, this.responseCreateWorker, this.errCreateWorker);
    }
    responseCreateWorker = (datas) => {
        const { statusCode, data } = datas;
        if (statusCode === 200) {
            this.props.history.push('/user/workers');
            workersFetch();
        }
    }
    errCreateWorker = (data) => { console.log('err:', data); }

    render() {
        const { history } = this.props;
        const { warning, name, post, desc } = this.state;
        return (
            <div>
                <Button
                    style={{ margin: '20px auto 20px 20px', display: 'block' }}
                    onClick={() => history.push('/user/workers')}
                > Back
                </Button>
                <h2 style={{ textAlign: 'center', marginTop: '30px' }}>Create new worker</h2>
                <Form onSubmit={this.handleSubmit}>
                    <Row>
                        <Col>
                            <ListGroup className="form-titles col-8">
                                <ListGroupItem>Full name</ListGroupItem>
                                <ListGroupItem>Position</ListGroupItem>
                                <ListGroupItem>Additional information</ListGroupItem>
                            </ListGroup>
                        </Col>
                        <div className="separator"></div>
                        <Col>
                            <ListGroup className=" form-fields col-8">
                                <FormGroup>
                                    <Input
                                        type="text"
                                        value={name}
                                        onChange={this.nameToggle}
                                    />
                                </FormGroup>
                                <FormGroup>
                                    <Input
                                        type="text"
                                        value={post}
                                        onChange={this.postToggle}
                                    />
                                </FormGroup>
                                <FormGroup>
                                    <Input
                                        type="textarea"
                                        value={desc}
                                        onChange={this.descToggle}
                                    />
                                </FormGroup>
                            </ListGroup>
                        </Col>
                    </Row>
                    {warning
                        ? (
                            <Alert color="warning" className="fields-warning-create">
                                {warning}
                            </Alert>
                        )
                        : ''}
                    { name && post
                        ? <Button style={{ margin: '20px auto', display: 'block' }} className="add-assistant-submit" type="submit">Create</Button>
                        : ''}
                </Form>
            </div>
        );
    }
}
