

const initialState = {
    data: [],
    loading: false,
    error: null
};

export default function (state = initialState, action) {
    switch (action.type) {
    case 'INVENTORISATION_REQUEST':
        return {
            ...state,
            loading: true
        };
    case 'INVENTORISATION_SUCCESS':
        return {
            ...state,
            loading: false,
            data: action.data.inventorisation.map((item) => { return { value: item.id, label: item.name, ...item }; })
        };
    case 'INVENTORISATION_FAILURE':
        return {
            ...state,
            loading: false,
            error: action.error
        };
    default:
        return state;
    }
}
