

const initialState = {
    data: [],
    loading: false,
    error: null
};

export default function (state = initialState, action) {
    switch (action.type) {
    case 'FUEL_REQUEST':
        return {
            ...state,
            loading: true
        };
    case 'FUEL_SUCCESS':
        return {
            ...state,
            loading: false,
            data: action.data.fuel
        };
    case 'FUEL_FAILURE':
        return {
            ...state,
            loading: false,
            error: action.error
        };
    default:
        return state;
    }
}
