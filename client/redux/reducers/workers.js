

const initialState = {
    data: [],
    loading: false,
    error: null
};

export default function (state = initialState, action) {
    switch (action.type) {
    case 'WORKERS_REQUEST':
        return {
            ...state,
            loading: true
        };
    case 'WORKERS_SUCCESS':
        console.log(3333, action.data);
        return {
            ...state,
            loading: false,
            data: action.data.data.map((item) => { return { value: item.id, label: item.name, ...item }; })
        };
    case 'WORKERS_FAILURE':
        return {
            ...state,
            loading: false,
            error: action.error
        };
    default:
        return state;
    }
}
