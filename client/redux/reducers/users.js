

const initialState = {
    data: [],
    loading: false,
    error: null
};

export default function (state = initialState, action) {
    switch (action.type) {
    case 'USERS_REQUEST':
        return {
            ...state,
            loading: true
        };
    case 'USERS_SUCCESS':
        return {
            ...state,
            loading: false,
            data: action.data.users
        };
    case 'USERS_FAILURE':
        return {
            ...state,
            loading: false,
            error: action.error
        };
    default:
        return state;
    }
}
