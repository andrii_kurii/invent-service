

const initialState = {
    data: [],
    loading: false,
    error: null
};

export default function (state = initialState, action) {
    switch (action.type) {
    case 'VEHICLES_REQUEST':
        return {
            ...state,
            loading: true
        };
    case 'VEHICLES_SUCCESS':
        return {
            ...state,
            loading: false,
            data: action.data.data.map((item) => { return { value: item.id, label: item.name, ...item }; })
        };
    case 'VEHICLES_FAILURE':
        return {
            ...state,
            loading: false,
            error: action.error
        };
    default:
        return state;
    }
}
