import { combineReducers } from 'redux';
import inventorisation from './inventorisation';
import invents from './invents';
import actionTypes from './action-types';
import users from './users';
import workers from './workers';
import counterparties from './counterparties';
import fuel from './fuel';
import vehicles from './vehicles';

export default combineReducers({
    invents,
    inventorisation,
    actionTypes,
    users,
    workers,
    counterparties,
    fuel,
    vehicles
});
