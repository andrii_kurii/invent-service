

const initialState = {
    data: [],
    loading: false,
    error: null
};

export default function (state = initialState, action) {
    switch (action.type) {
    case 'COUNTERPARTIES_REQUEST':
        return {
            ...state,
            loading: true
        };
    case 'COUNTERPARTIES_SUCCESS':
        return {
            ...state,
            loading: false,
            data: action.data.counterparties.map((item) => { return { value: item.id, label: item.name, ...item }; })
        };
    case 'COUNTERPARTIES_FAILURE':
        return {
            ...state,
            loading: false,
            error: action.error
        };
    default:
        return state;
    }
}
