

const initialState = {
    data: [],
    loading: false,
    error: null
};

export default function (state = initialState, action) {
    switch (action.type) {
    case 'ACTION_TYPES_REQUEST':
        return {
            ...state,
            loading: true
        };
    case 'ACTION_TYPES_SUCCESS':
        return {
            ...state,
            loading: false,
            data: action.data.actionTypes.map((item) => { return { value: item.id, label: item.name, ...item }; })
        };
    case 'ACTION_TYPES_FAILURE':
        return {
            ...state,
            loading: false,
            error: action.error
        };
    default:
        return state;
    }
}
