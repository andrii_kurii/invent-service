

const initialState = {
    data: [],
    loading: false,
    error: null
};

export default function (state = initialState, action) {
    switch (action.type) {
    case 'INVENTS_REQUEST':
        return {
            ...state,
            loading: true
        };
    case 'INVENTS_SUCCESS':
        return {
            ...state,
            loading: false,
            data: action.data.invents
        };
    case 'INVENTS_FAILURE':
        return {
            ...state,
            loading: false,
            error: action.error
        };
    default:
        return state;
    }
}
