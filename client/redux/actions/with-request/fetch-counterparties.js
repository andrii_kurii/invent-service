
import axios from 'axios';
import { parseUrl } from '../../../api_temp';
import store from '../../store';

export const FETCH_COUNTERPARTIES = 'FETCH_COUNTERPARTIES';

function actionCounterpartiesReceiver(data) {
    return {
        type: FETCH_COUNTERPARTIES,
        counterparties: data
    };
}
export const counterpartiesFetch = () => {
    axios.get(parseUrl('user/counterparties'),
        { headers: { 'Content-Type': 'application/json', Authorization: `Bearer ${localStorage.getItem('token')}` } })
        .then((datas) => {
            const { status, data: { counterparties } } = datas;
            if (status === 200) {
                store.dispatch(actionCounterpartiesReceiver(counterparties));
            }
        });
};
