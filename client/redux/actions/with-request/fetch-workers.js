
import axios from 'axios';
import { parseUrl } from '../../../api_temp';
import store from '../../store';

export const FETCH_WORKERS = 'FETCH_WORKERS';

function actionWorkersReceiver(data) {
    return {
        type: FETCH_WORKERS,
        workers: data
    };
}
export const workersFetch = () => {
    axios.get(parseUrl('user/workers?active=true'),
        { headers: { 'Content-Type': 'application/json', Authorization: `Bearer ${localStorage.getItem('token')}` } })
        .then((datas) => {
            const { status, data } = datas;
            if (status === 200) {
                store.dispatch(actionWorkersReceiver(data.data));
            }
        });
};
