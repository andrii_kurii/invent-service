
import axios from 'axios';
import { parseUrl } from '../../../api_temp';
import store from '../../store';

export const FETCH_VEHICLES = 'FETCH_VEHICLES';

function actionVehiclesReceiver(data) {
    return {
        type: FETCH_VEHICLES,
        vehicles: data
    };
}
export const vehiclesFetch = () => {
    axios.get(parseUrl('user/vehicles'),
        { headers: { 'Content-Type': 'application/json', Authorization: `Bearer ${localStorage.getItem('token')}` } })
        .then((datas) => {
            const { status, data } = datas;
            if (status === 200) {
                store.dispatch(actionVehiclesReceiver(data.data));
            }
        });
};
