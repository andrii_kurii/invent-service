
import axios from 'axios';
import { parseUrl } from '../../../api_temp';
import store from '../../store';

export const FETCH_USERS = 'FETCH_USERS';

function actionUsersReceiver(data) {
    return {
        type: FETCH_USERS,
        users: data
    };
}
export const usersFetch = () => {
    axios.get(parseUrl('user/users'),
        { headers: { 'Content-Type': 'application/json', Authorization: `Bearer ${localStorage.getItem('token')}` } })
        .then((datas) => {
            const { status, data } = datas;
            if (status === 200) {
                store.dispatch(actionUsersReceiver(data.data));
            }
        });
};
