
import axios from 'axios';
import { parseUrl } from '../../../api_temp';
import store from '../../store';

export const FETCH_FUEL = 'FETCH_FUEL';

function actionFuelReceiver(data) {
    return {
        type: FETCH_FUEL,
        fuel: data
    };
}
export const fuelFetch = () => {
    axios.get(parseUrl('user/fuel'),
        { headers: { 'Content-Type': 'application/json', Authorization: `Bearer ${localStorage.getItem('token')}` } })
        .then((datas) => {
            const { status, data } = datas;
            if (status === 200) {
                store.dispatch(actionFuelReceiver(data.data));
            }
        });
};
