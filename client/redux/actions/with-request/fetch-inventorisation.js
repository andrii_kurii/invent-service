
import axios from 'axios';
import { parseUrl } from '../../../api_temp';
import store from '../../store';

/* eslint-disable import/prefer-default-export */
export const fetch = (actionName, method, url) => {
    const actionNameUpper = actionName.toUpperCase();
    const actionRequest = `${actionNameUpper}_REQUEST`;
    const actionSuccess = `${actionNameUpper}_SUCCESS`;
    const actionFailure = `${actionNameUpper}_FAILURE`;

    store.dispatch({ type: actionRequest });
    axios({
        method,
        url: parseUrl(url),
        headers: { 'Content-Type': 'application/json', Authorization: `Bearer ${localStorage.getItem('token')}` }
    })
        .then((datas) => {
            const { status, data } = datas;
            // if (status === 200) {
            store.dispatch({ type: actionSuccess, data });
            // }
        })
        .catch((datas) => {
            console.log(datas);
            const error = {
                message: datas.response.message || 'error happened',
                body: datas.response
            };
            store.dispatch({ type: actionFailure, error });
        });
};
