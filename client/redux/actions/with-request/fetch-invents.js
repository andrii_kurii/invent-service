
import axios from 'axios';
import { parseUrl } from '../../../api_temp';
import store from '../../store';

export const FETCH_INVENTS = 'FETCH_INVENTS';

function inventsReceiver(invents) {
    return {
        type: FETCH_INVENTS,
        invents
    };
}
export const inventsFetch = () => {
    axios.get(parseUrl('user/invents'),
        { headers: { 'Content-Type': 'application/json', Authorization: `Bearer ${localStorage.getItem('token')}` } })
        .then((datas) => {
            const { status, data: { invents } } = datas;
            if (status === 200) {
                store.dispatch(inventsReceiver(invents));
            }
        });
};
