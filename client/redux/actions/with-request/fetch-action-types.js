
import axios from 'axios';
import { parseUrl } from '../../../api_temp';
import store from '../../store';

export const FETCH_ACTION_TYPES = 'FETCH_ACTION_TYPES';

function actionTypesReceiver(actionTypes) {
    return {
        type: FETCH_ACTION_TYPES,
        actionTypes
    };
}
export const actionTypesFetch = async () => {
    const result = await axios.get(
        parseUrl('user/action-types'),
        {
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        }
    );

    const { status, data: { actionTypes } } = result;
    if (status === 200) {
        store.dispatch(actionTypesReceiver(actionTypes));
    }
};
