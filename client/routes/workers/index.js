
import React from 'react';
import PropTypes from 'prop-types';
import { Route, Switch } from 'react-router-dom';
import {
    Button, Breadcrumb, BreadcrumbItem, Table, InputGroupAddon, InputGroupText, Input, FormGroup, Label
} from 'reactstrap';
import { Worker, CreateWorker, Workers } from '../../components/user/workers';

export default class WorkersRoutes extends React.Component {
    static propTypes = {
        history: PropTypes.shape({ push: PropTypes.func.isRequired }).isRequired,
        match: PropTypes.shape({ path: PropTypes.string.isRequired }).isRequired,
        setTab: PropTypes.func.isRequired
    }
    componentDidMount() {
        this.props.setTab();
    }
    render() {
        const { match } = this.props;
        return (
            <Switch>
                <Route
                    exact
                    path={`${match.path}`}
                    render={routeProps => <Workers {...routeProps} />}
                />
                <Route
                    path={`${match.path}/create-worker`}
                    render={routeProps => <CreateWorker {...routeProps} />}
                />
                <Route
                    path={`${match.path}/:id`}
                    render={routeProps => <Worker {...routeProps} />}
                />
            </Switch>
        );
    }
}
