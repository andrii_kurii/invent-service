
import React from 'react';
import PropTypes from 'prop-types';
import { Route, Switch } from 'react-router-dom';
import {
    Button, Breadcrumb, BreadcrumbItem, Table, InputGroupAddon, InputGroupText, Input, FormGroup, Label
} from 'reactstrap';
import { Vehicle, Vehicles, CreateVehicle, EditVehicle, Event } from '../../components/user/vehicles';

export default class VehiclesRoutes extends React.Component {
    static propTypes = {
        history: PropTypes.shape({ push: PropTypes.func.isRequired }).isRequired,
        match: PropTypes.shape({ path: PropTypes.string.isRequired }).isRequired,
        setTab: PropTypes.func.isRequired
    }
    componentDidMount() { this.props.setTab(); }
    render() {
        const { match } = this.props;
        return (
            <Switch>
                <Route
                    exact
                    path={`${match.path}`}
                    render={routeProps => <Vehicles {...routeProps} />}
                />
                <Route
                    path={`${match.path}/events/:id`}
                    render={routeProps => <Event {...routeProps} />}
                />
                <Route
                    path={`${match.path}/create-vehicle`}
                    render={routeProps => <CreateVehicle {...routeProps} />}
                />
                <Route
                    path={`${match.path}/:id/edit`}
                    render={routeProps => <EditVehicle {...routeProps} />}
                />
                <Route
                    path={`${match.path}/:id`}
                    render={routeProps => <Vehicle {...routeProps} />}
                />
            </Switch>
        );
    }
}
