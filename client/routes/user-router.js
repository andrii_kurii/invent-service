import React from 'react';
import PropTypes from 'prop-types';
import { Route, Switch } from 'react-router-dom';
import { Provider } from 'react-redux';
import {
    Nav, NavItem, NavLink
} from 'reactstrap';

import { LogoutButton } from '../components/reusable';
import InventorisationRoutes from './inventorisation';
import PurchaseRoutes from './purchase';
import VehiclesRoutes from './vehicles';
import WorkersRoutes from './workers';
import Init from '../components/Init';
import store from '../redux/store';


export default class UserRouter extends React.Component {
    static propTypes = {
        history: PropTypes.shape({ push: PropTypes.func.isRequired }).isRequired,
        match: PropTypes.shape({ path: PropTypes.string.isRequired }).isRequired
    }
    constructor() {
        super();
        this.toggle = this.toggle.bind(this);
        this.state = { activeTab: '' };
    }
    toggle = (tab) => {
        const { history, match } = this.props;
        history.push(`${match.path}/${tab}`);
    }
    logout = () => {
        localStorage.removeItem('token');
        const { history } = this.props;
        history.push('/login');
    }
    render() {
        const { activeTab } = this.state;
        const { match } = this.props;
        return (
            <Provider store={store}>
                <LogoutButton logout={this.logout} />
                <div>
                    <Nav tabs>
                        <NavItem>
                            <NavLink
                                className={activeTab === 'inventorisation' ? 'active' : ''}
                                onClick={() => { this.toggle('inventorisation'); }}
                            >
                                INVENTORISATION AND SUPPLIES
                            </NavLink>
                        </NavItem>
                        <NavItem>
                            <NavLink
                                className={activeTab === 'purchases' ? 'active' : ''}
                                onClick={() => { this.toggle('purchases'); }}
                            >
                                PURCHASES
                            </NavLink>
                        </NavItem>
                        <NavItem>
                            <NavLink
                                className={activeTab === 'vehicles' ? 'active' : ''}
                                onClick={() => { this.toggle('vehicles'); }}
                            >
                                VEHICLES
                            </NavLink>
                        </NavItem>
                        <NavItem>
                            <NavLink
                                className={activeTab === 'workers' ? 'active' : ''}
                                onClick={() => { this.toggle('workers'); }}
                            >
                                WORKERS
                            </NavLink>
                        </NavItem>
                    </Nav>
                    <Init>
                        <Switch>
                            <Route
                                exact
                                path={`${match.path}/`}
                                render={routeProps => <InventorisationRoutes {...routeProps} setTab={() => this.setState({ activeTab: 'inventorisation' })} />}
                            />
                            <Route
                                path={`${match.path}/inventorisation`}
                                render={routeProps => <InventorisationRoutes {...routeProps} setTab={() => this.setState({ activeTab: 'inventorisation' })} />}
                            />
                            <Route
                                path={`${match.path}/purchases`}
                                render={routeProps => <PurchaseRoutes {...routeProps} setTab={() => this.setState({ activeTab: 'purchases' })} />}
                            />
                            <Route
                                path={`${match.path}/vehicles`}
                                render={routeProps => <VehiclesRoutes {...routeProps} setTab={() => this.setState({ activeTab: 'vehicles' })} />}
                            />
                            <Route
                                path={`${match.path}/workers`}
                                render={routeProps => <WorkersRoutes {...routeProps} setTab={() => this.setState({ activeTab: 'workers' })} />}
                            />
                        </Switch>
                    </Init>
                </div>
            </Provider>
        );
    }
}
