
import React from 'react';
import PropTypes from 'prop-types';
import { Route, Switch } from 'react-router-dom';
import {
    Button, Breadcrumb, BreadcrumbItem, Table, InputGroupAddon, InputGroupText, Input, FormGroup, Label
} from 'reactstrap';
import { Purchases, CreateCounterparty, Purchase, EditPurchase } from '../../components/user/purchase';

export default class PurchaseRoutes extends React.Component {
    static propTypes = {
        history: PropTypes.shape({ push: PropTypes.func.isRequired }).isRequired,
        match: PropTypes.shape({ path: PropTypes.string.isRequired }).isRequired,
        setTab: PropTypes.func.isRequired
    }
    componentDidMount() {
        this.props.setTab();
    }
    render() {
        const { match } = this.props;
        return (
            <Switch>
                <Route
                    exact
                    path={`${match.path}`}
                    render={routeProps => <Purchases {...routeProps} />}
                />
                <Route
                    path={`${match.path}/create-counterparty`}
                    render={routeProps => <CreateCounterparty {...routeProps} />}
                />
                <Route
                    path={`${match.path}/events/:id`}
                    render={routeProps => <EditPurchase {...routeProps} />}
                />
                <Route
                    path={`${match.path}/:id`}
                    render={routeProps => <Purchase {...routeProps} />}
                />
            </Switch>
        );
    }
}
