
import React from 'react';
import PropTypes from 'prop-types';
import { Route, Switch } from 'react-router-dom';
import {
    Button, Breadcrumb, BreadcrumbItem, Table, InputGroupAddon, InputGroupText, Input, FormGroup, Label
} from 'reactstrap';
// import InventList from '../../components/user/inventorisation/invent-list';
// import CreateInvent from '../../components/user/inventorisation/create-invent';
import { CreateInvent, Invents, Invent, Event, InventEdit } from '../../components/user/inventorisation';


export default class InventorisationRoutes extends React.Component {
    static propTypes = {
        history: PropTypes.shape({ push: PropTypes.func.isRequired }).isRequired,
        match: PropTypes.shape({ path: PropTypes.string.isRequired }).isRequired
    }
    componentDidMount() { this.props.setTab(); }
    render() {
        const { match } = this.props;
        return (
            <Switch>
                <Route
                    exact
                    path={`${match.path}`}
                    render={routeProps => <Invents {...routeProps} />}
                />
                <Route
                    path={`${match.path}/events/:id`}
                    render={routeProps => <Event {...routeProps} />}
                />
                <Route
                    path={`${match.path}/create-invent`}
                    render={routeProps => <CreateInvent {...routeProps} />}
                />
                <Route
                    path={`${match.path}/:id/edit`}
                    render={routeProps => <InventEdit {...routeProps} />}
                />
                <Route
                    path={`${match.path}/:id`}
                    render={routeProps => <Invent {...routeProps} />}
                />
            </Switch>
        );
    }
}
