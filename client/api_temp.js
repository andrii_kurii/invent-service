
// const ip = 'dry-taiga-14344.herokuapp.com';
require('es6-promise').polyfill();
require('isomorphic-fetch');

const ip = 'localhost';
const type = 'http';
const port = ':9090';
// const ip = 'mighty-springs-99959.herokuapp.com';
// const type = 'http';
// const port = '';


function parseUrl(url) {
    console.log(`${type}://${ip}${port}/api/${url}`);
    return `${type}://${ip}${port}/api/${url}`;
}

function GET(url, response, err) {
    let statusCode;
    fetch(
        parseUrl(url),
        {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        }
    )
        .then((res) => {
            statusCode = res.status;
            return res.json();
        })
        .then((data) => {
            // setTimeout(() => response({ ...data, statusCode }), 2000);
            response({ ...data, statusCode });
        })
        .catch((error) => {
            err(error);
        });
}

function TGET(url, response, err) {
    let statusCode;
    let token = localStorage.getItem('token');
    fetch(
        parseUrl(url),
        {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${token}`
            }
        }
    )
        .then((res) => {
            statusCode = res.status;
            return res.json();
        })
        .then((data) => {
            response({ ...data, statusCode })
            // setTimeout(() => response({ ...data, statusCode }), 500);
        })
        .catch((error) => {
            err(error);
        });
}

function POST(url, body, response, err) {
    let statusCode;
    fetch(
        parseUrl(url),
        {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(body)
        }
    )
        .then((res) => {
            statusCode = res.status;
            return res.json();
        })
        .then((data) => {
            // setTimeout(() => response({ ...data, statusCode }), 1000);
            response({ ...data, statusCode });
        })
        .catch((error) => {
            err(error);
        });
}

function TPOST(url, body, response, err) {
    let statusCode;
    fetch(
        parseUrl(url),
        {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify(body)
        }
    )
        .then((res) => {
            statusCode = res.status;
            return res.json();
        })
        .then((data) => {
            // setTimeout(() => response({ ...data, statusCode }), 1000);
            response({ ...data, statusCode });
        })
        .catch((error) => {
            err(error);
        });
}

function PUT(url, body, response, err) {
    let statusCode;
    fetch(
        parseUrl(url),
        {
            method: 'PUT',
            headers: {
                // 'Access-Control-Allow-Origin': '*',
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify(body)
        }
    )
        .then((res) => {
            statusCode = res.status;
            return res.json();
        })
        .then((data) => {
            // setTimeout(() => response({ ...data, statusCode }), 1000);
            response({ ...data, statusCode });
        })
        .catch((error) => {
            err(error);
        });
}

export {
    GET,
    TGET,
    POST,
    TPOST,
    PUT,
    parseUrl
};
