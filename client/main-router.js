import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

import Login from './components/login';
import User from './routes/user-router';
import InventorisationReport from './components/user/reports/inventorisation';
import VehiclesReport from './components/user/reports/vehicles';

class Routes extends React.Component {
    render() {
        return (
            <Router>
                <div className="router">
                    <Switch>
                        <Route exact path="/" component={Login} />
                        <Route path="/login" component={Login} />
                        <Route path="/user" component={User} />
                        <Route path="/inventorisation-report" component={InventorisationReport} />
                        <Route path="/vehicles-report" component={VehiclesReport} />
                    </Switch>
                </div>
            </Router>
        );
    }
}

export default Routes;
