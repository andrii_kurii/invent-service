
-- \c komuna;
-- SET client_encoding = 'UTF8';

INSERT INTO users 
    (name, role, login, password) 
VALUES 
    ('admin', 'all', 'superadmin', '$2a$10$UuetIoOECmyuTZ93MOwxTeAUemtORqrEug4NLonh9wjNs/YUEVwtS');

INSERT INTO users 
    (name, role, login, password) 
VALUES 
    ('moderator', 'all', 'moderator', '$2a$10$UuetIoOECmyuTZ93MOwxTeAUemtORqrEug4NLonh9wjNs/YUEVwtS');

-- INSERT INTO users
--     (name, role, login, password)
-- VALUES
--     ('Director', 'read', 'director', '$2a$10$UuetIoOECmyuTZ93MOwxTeAUemtORqrEug4NLonh9wjNs/YUEVwtS');

INSERT INTO fuel (name) VALUES ('diesel');
INSERT INTO fuel (name) VALUES ('gasoline');
INSERT INTO fuel (name) VALUES ('other');

INSERT INTO invent (name) VALUES ('Quantity');
INSERT INTO invent (name) VALUES ('Meters');
INSERT INTO invent (name) VALUES ('Kilograms');
INSERT INTO invent (name) VALUES ('Tons');
INSERT INTO invent (name) VALUES ('Litrs');
INSERT INTO invent (name) VALUES ('Cubic meters');

INSERT INTO action_types (name) VALUES ('Renewal');
INSERT INTO action_types (name) VALUES ('Issuance');
INSERT INTO action_types (name) VALUES ('Returned');
INSERT INTO action_types (name) VALUES ('Write off');

