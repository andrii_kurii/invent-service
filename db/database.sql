
-- DROP DATABASE IF EXISTS komuna;
-- CREATE DATABASE komuna WITH TEMPLATE = template0 ENCODING = 'UTF8';
-- SET client_encoding = 'UTF8';

-- \c komuna;

CREATE TYPE roles AS ENUM (
	'all',
	'read');

CREATE TABLE fuel (
	ID SERIAL PRIMARY KEY,
	name text NOT NULL,
	unique (name));

CREATE TABLE action_types (
	ID SERIAL PRIMARY KEY,
	name text NOT NULL,
	unique (name));

CREATE TABLE invent (
	ID SERIAL PRIMARY KEY,
	name text NOT NULL,
	unique (name));

CREATE TABLE users (
	ID SERIAL PRIMARY KEY,
	name text NOT NULL,
	role roles NOT NULL,
	login text NOT NULL,
	password text NOT NULL,
	created_at timestamp DEFAULT now() NOT NULL,
	unique(login),
	unique(name));

CREATE TABLE workers (
	ID SERIAL PRIMARY KEY,
	name text NOT NULL,
	post text NOT NULL,
	description text,
	active boolean DEFAULT true NOT NULL,
	created_at timestamp DEFAULT now() NOT NULL,
	created_by integer REFERENCES users(ID) NOT NULL,
	unique(name));

CREATE TABLE inventorisation (
	ID SERIAL PRIMARY KEY,
	name text NOT NULL,
	invent_id integer REFERENCES invent(ID) NOT NULL,
	-- value double precision NOT NULL DEFAULT 0,
	created_at timestamp DEFAULT now() NOT NULL,
	created_by integer REFERENCES users(ID) NOT NULL,
	unique(name));

-- # ==============================================
CREATE TABLE invent_actions (
	ID SERIAL PRIMARY KEY,
	inventorisation_id integer REFERENCES inventorisation(ID) NOT NULL,
	-- intentory_value double precision NOT NULL,
	action_type integer REFERENCES action_types(ID) NOT NULL NOT NULL,
	value double precision NOT NULL,
	worker_id integer REFERENCES workers(ID),
	created_at timestamp DEFAULT now() NOT NULL,
	created_by integer REFERENCES users(ID) NOT NULL);

CREATE TABLE invent_actions_history (
	ID SERIAL PRIMARY KEY,
	invent_action_id integer REFERENCES invent_actions(ID) NOT NULL,
	inventorisation_id integer REFERENCES inventorisation(ID) NOT NULL,
	-- intentory_value double precision NOT NULL,
	action_type integer REFERENCES action_types(ID) NOT NULL NOT NULL,
	value double precision NOT NULL,
	worker_id integer REFERENCES workers(ID),
	created_by integer REFERENCES users(ID) NOT NULL,
	created_at timestamp DEFAULT now() NOT NULL);

CREATE OR REPLACE FUNCTION trigger_invent_actions_history()
    RETURNS TRIGGER
    AS $$
    BEGIN
    
    INSERT INTO invent_actions_history (
	invent_action_id, inventorisation_id, action_type, value, worker_id, created_by) 
	VALUES (
		NEW.ID, NEW.inventorisation_id, NEW.action_type, NEW.value, NEW.worker_id, NEW.created_by
	);
	RETURN NEW;
    END;
    $$
    LANGUAGE plpgsql;
-- DROP TRIGGER IF EXISTS trigger_invent_actions_history ON invent_actions;
CREATE TRIGGER trigger_invent_actions_history
	AFTER UPDATE OR INSERT ON invent_actions
	FOR EACH ROW
	EXECUTE PROCEDURE trigger_invent_actions_history();
-- # ==============================================

CREATE TABLE counterparties (
	ID SERIAL PRIMARY KEY,
	name text NOT NULL,
	description text,
	created_at timestamp DEFAULT now() NOT NULL,
	created_by integer REFERENCES users(ID) NOT NULL,
	unique(name));

-- # ==============================================
CREATE TABLE purchases (
	ID SERIAL PRIMARY KEY,
	counterparty_id integer REFERENCES counterparties(ID) NOT NULL,
	price double precision NOT NULL,
	invoice text,
	description text,
	bill_date timestamp NOT NULL,
	created_at timestamp DEFAULT now() NOT NULL,
	created_by integer REFERENCES users(ID) NOT NULL,
	unique(invoice));

CREATE TABLE purchases_history (
	ID SERIAL PRIMARY KEY,
	purchase_id integer REFERENCES purchases(ID) NOT NULL,
	counterparty_id integer REFERENCES counterparties(ID) NOT NULL,
	price double precision NOT NULL,
	invoice text,
	description text,
	bill_date timestamp NOT NULL,
	created_at timestamp DEFAULT now() NOT NULL,
	created_by integer REFERENCES users(ID) NOT NULL);

CREATE OR REPLACE FUNCTION trigger_purchase_history()
    RETURNS TRIGGER
    AS $$
    BEGIN    
    INSERT INTO purchases_history (
		purchase_id, counterparty_id, price, invoice, description, bill_date, created_by) 
	VALUES (
		NEW.ID, NEW.counterparty_id, NEW.price, NEW.invoice, NEW.description, NEW.bill_date, NEW.created_by
	);
	RETURN NEW;
    END;
    $$
    LANGUAGE plpgsql;

CREATE TRIGGER trigger_purchase_history
	AFTER UPDATE OR INSERT ON purchases
	FOR EACH ROW
	EXECUTE PROCEDURE trigger_purchase_history();

-- # ==============================================
CREATE TABLE vehicles (
	ID SERIAL PRIMARY KEY,
	mark text NOT NULL,
	numbers text NOT NULL,
	fuel_id integer REFERENCES fuel(ID) NOT NULL,
	fuel_limit real,
	created_at timestamp DEFAULT now() NOT NULL,
	created_by integer REFERENCES users(ID) NOT NULL);

CREATE TABLE vehicle_actions (
	ID SERIAL PRIMARY KEY,
	vehicle_id integer REFERENCES vehicles(ID) NOT NULL,
	-- outer_id SERIAL NOT NULL,
	action_type integer REFERENCES action_types(ID) NOT NULL NOT NULL,
	value double precision NOT NULL,
	worker_id integer REFERENCES workers(ID),
	created_at timestamp DEFAULT now() NOT NULL,
	created_by integer REFERENCES users(ID) NOT NULL);

CREATE TABLE vehicle_actions_history (
	ID SERIAL PRIMARY KEY,
	vehicle_action_id integer REFERENCES vehicle_actions(ID) NOT NULL,
	vehicle_id integer REFERENCES vehicles(ID) NOT NULL,
	action_type integer REFERENCES action_types(ID) NOT NULL NOT NULL,
	value double precision NOT NULL,
	worker_id integer REFERENCES workers(ID),
	created_at timestamp DEFAULT now() NOT NULL,
	created_by integer REFERENCES users(ID) NOT NULL);

CREATE OR REPLACE FUNCTION trigger_vehicle_actions_history()
    RETURNS TRIGGER
    AS $$
    BEGIN    
    INSERT INTO vehicle_actions_history (
	vehicle_action_id, vehicle_id, action_type, value, worker_id, created_by) 
	VALUES (
		NEW.ID, NEW.vehicle_id, NEW.action_type, NEW.value, NEW.worker_id, NEW.created_by
	);
	RETURN NEW;
    END;
    $$
    LANGUAGE plpgsql;
    
CREATE TRIGGER trigger_vehicle_actions_history
	AFTER UPDATE OR INSERT ON vehicle_actions
	FOR EACH ROW
	EXECUTE PROCEDURE trigger_vehicle_actions_history();
-- # ==============================================
