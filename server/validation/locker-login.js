const Joi = require('joi');

module.exports = {
    body: {
        locker: Joi.string().required(),
        password: Joi.string().required()
    }
};
