
const Joi = require('joi');

const { gender } = require('./types/enums');

module.exports = {
    params: {
        id: Joi.number().integer().min(1)
    },
    body: {
        first_name: Joi.string().required(),
        last_name: Joi.string().required(),
        email: Joi.string().email().required(),
        b_date: Joi.string().regex(/([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))/).required(),
        active: Joi.boolean().required(),
        sex: Joi.string().valid(gender).required()
    }
};
