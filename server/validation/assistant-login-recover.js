
const Joi = require('joi');

module.exports = {
    params: {
        id: Joi.number().integer().min(1)
    },
    body: {
        login: Joi.string().required(),
        password: Joi.string().required()
    }
};
