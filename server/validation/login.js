const Joi = require('joi');

module.exports = {
    body: {
        login: Joi.string().required(),
        password: Joi.string().required()
        // password: Joi.string().regex(/[a-zA-Z0-9]{5,10}/).required()
    }
};
