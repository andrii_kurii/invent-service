const Joi = require('joi');

module.exports = {
    params: {
        id: Joi.number().required()
    },
    body: {
        login: Joi.string().required(),
        password: Joi.string().required()
    }
};
