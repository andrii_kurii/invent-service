
const Joi = require('joi');

const { active, gender } = require('../types/enums');

module.exports = {
    params: {
        id: Joi.number().min(1).required()
    },
    body: {
        first_name: Joi.string().required(),
        last_name: Joi.string().required(),
        sex: Joi.string().valid(gender).required(),
        b_date: Joi.string().regex(/([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))/).required(),
        email: Joi.string().email({ minDomainAtoms: 2 }),
        active: Joi.string().valid(active)
    }
};
