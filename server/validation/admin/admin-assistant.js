const Joi = require('joi');

module.exports = {
    query: {
        active: Joi.string().valid(['', 'false', 'true'])
    }
};
