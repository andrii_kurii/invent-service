
const Joi = require('joi');

module.exports = {
    params: {
        id: Joi.number().min(1).required()
    }
};
