
const Joi = require('joi');

const { active } = require('../types/enums');

module.exports = {
    params: {
        active: Joi.string().valid(active)
    }
};
