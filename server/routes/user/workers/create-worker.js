const db = require('../../../connect');

module.exports = function createWorker(req, res, next) {
    const { name, post, desc } = req.body;
    const { authData } = req.body;
    const { user } = authData;
    const { id } = user;

    db.any(`INSERT INTO workers (name, post, description, created_by)
        VALUES ( ($1), ($2), ($3), ($4) )`,
    [name, post, desc || null, id])
        .then(() => {
            res.status(200)
                .json({
                    message: 'worker created'
                });
        })
        .catch((err) => {
            return next(err);
        });
};
