const db = require('../../../connect');

module.exports = function updateWorker(req, res, next) {
    const { id, active } = req.body;

    db.any(`UPDATE workers SET active = ($2)
    WHERE ID = ($1)`,
    [id, active])
        .then(() => {
            res.status(200)
                .json({
                    message: 'activity updated'
                });
        })
        .catch((err) => {
            return next(err);
        });
};
