
const db = require('../../../connect');

async function helper(limit, req, next, res) {
    const { id, user_filter, active } = req.query;
    try {
        const data = await db.any(`select (count(*) + ($4)) / ($4) as end 
                    from workers    
                    inner join users on users.id = workers.created_by
                WHERE workers.ID IS NOT NULL
                    ${id ? 'AND workers.ID = ($1)' : ''}
                    ${user_filter ? "AND workers.name LIKE '%'  || ($2) || '%'" : ''}
                    ${Boolean(active) ? 'AND workers.active = ($3)' : ''}`,
        [id, user_filter, active, limit])     
        const { end } = data[0];
        return res(end);
    } catch (e) {
        console.error(e);
        return next(e);
    }
}

module.exports = async function getWorkers(req, res, next) {
    const { id, user_filter, active, page, limit } = req.query;
    const local_page = page || 1;
    const local_limit = limit || 999;
    try {
        const data = await db.any(
            `select workers.*, users.name as creator_name
                from workers
                inner join users on users.id = workers.created_by
            WHERE workers.ID IS NOT NULL
                ${id ? 'AND workers.ID = ($1)' : ''}
                ${user_filter ? "AND workers.name LIKE '%'  || ($2) || '%'" : ''}
                ${Boolean(active) ? 'AND workers.active = ($3)' : ''}
            ORDER BY workers.active desc
            LIMIT ($4) OFFSET (($5) - 1) * ($4)`,
            [id, user_filter, active, local_limit, local_page]
        );
        return helper(local_limit, req, next, (end) => {
            return res.status(200).json({
                data,
                message: 'received workers',
                end
            });
        });
    } catch (e) {
        console.error(e);
        return next(e);
    }
};
