const db = require('../../../connect');

module.exports = function updateWorker(req, res, next) {
    const { id, name, post, desc } = req.body;
    db.any(`UPDATE workers SET name = ($2), post = ($3), description = ($4)
    WHERE ID = ($1)`,
    [id, name, post, desc])
        .then(() => {
            res.status(200)
                .json({
                    message: 'worker updated'
                });
        })
        .catch((err) => {
            if (err.code == 23505) {
                return res.status(401).json({
                    message: err.detail
                });
            }
            return next(err);
        });
};
