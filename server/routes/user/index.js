const express = require('express');

const router = express.Router();

const users = require('./users');

const invents = require('./inventorisation/invents');
const actionTypes = require('./inventorisation/action-types');
// const getInventory = require('./inventorisation/inventory');
const getActions = require('./inventorisation/actions');
const getInventActionsHistory = require('./inventorisation/invent-action-history');
const renewal = require('./inventorisation/renewal');
const returned = require('./inventorisation/returned');
const issuance = require('./inventorisation/issuance');
const writeoff = require('./inventorisation/write-off');
const createInvents = require('./inventorisation/create-inventor');
const updateInventor = require('./inventorisation/update-inventor');
const inventorisation = require('./inventorisation/inventorisation');
const updateAction = require('./inventorisation/update-invent-action');

const createCounterparty = require('./purchases/create-counterparty');
const counterparties = require('./purchases/counterparties');
const purchases = require('./purchases/purchases');
const purchaseHistory = require('./purchases/purchase-history');
const total = require('./purchases/get-total');
const createPurchases = require('./purchases/create-purchase');
const updatePurchase = require('./purchases/update-purchase');
// const invoices = require('./purchases/invoices');
// const createWorker = require('./workers/create-worker');

const fuel = require('./vehicles/get-fuels');
const vehicles = require('./vehicles/vehicles');
const vehiclesNumbers = require('./vehicles/vehicles-numbers');
const vehiclesActions = require('./vehicles/vehicles-actions');
const createVehicle = require('./vehicles/create-vehicle');
const getVehicleActionsHistory = require('./vehicles/vehicle-action-history');
const updateVehicle = require('./vehicles/update-vehicle');
const vehicleRenewal = require('./vehicles/renewal');
const vehicleReturned = require('./vehicles/returned');
const vehicleIssuance = require('./vehicles/issuance');
const vehicleWriteoff = require('./vehicles/write-off');
const updateVehicleAction = require('./vehicles/update-vehicle-action');

const workers = require('./workers/workers');
const createWorker = require('./workers/create-worker');
const updateWorkerActivity = require('./workers/update-activity');
const updateWorker = require('./workers/update-worker');

router.get('/users', users);
router.get('/invents', invents);
router.get('/action-types', actionTypes);
router.get('/inventorisation', inventorisation);
// router.get('/inventorisation/:id', getInventory);
router.get('/invent-actions', getActions);
router.get('/invent-action-history', getInventActionsHistory);
router.post('/create-inventor', createInvents);
router.post('/update-inventor', updateInventor);
// router.get('/invent-actions/:id', getInventActions);
router.put('/inventorisation/:id/renewal', renewal);
router.put('/inventorisation/:id/returned', returned);
router.put('/inventorisation/:id/issuance', issuance); // TODO put premiddleware for checking if (issuance < table.value)
router.put('/inventorisation/:id/write-off', writeoff); // TODO put premiddleware for checking if (writeoff < table.value)
router.post('/update-invent-action', updateAction);

router.get('/counterparties', counterparties);
router.get('/purchases', purchases);
router.get('/purchase-history', purchaseHistory);
router.get('/total', total);
// router.get('/invoices', invoices);
router.post('/purchases', createPurchases);
router.post('/update-purchase', updatePurchase);
router.post('/create-counterparty', createCounterparty);

router.get('/workers', workers);
router.post('/create-worker', createWorker);
router.post('/update-worker-activity', updateWorkerActivity);
router.post('/update-worker', updateWorker);

router.get('/fuel', fuel);
router.get('/vehicles', vehicles);
router.get('/vehicles-numbers', vehiclesNumbers);
router.get('/vehicles-actions', vehiclesActions);
router.get('/vehicle-action-history', getVehicleActionsHistory);
router.post('/create-vehicle', createVehicle);
router.post('/update-vehicle', updateVehicle);
router.post('/update-vehicle-action', updateVehicleAction);
router.put('/vehicles/:id/renewal', vehicleRenewal);
router.put('/vehicles/:id/returned', vehicleReturned);
router.put('/vehicles/:id/issuance', vehicleIssuance); // TODO put premiddleware for checking if (issuance < table.value)
router.put('/vehicles/:id/write-off', vehicleWriteoff); // TODO put premiddleware for checking if (writeoff < table.value)

module.exports = router;
