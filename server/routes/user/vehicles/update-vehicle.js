
const db = require('../../../connect');

module.exports = async function updateVehicle(req, res, next) {
    const { id, mark, fuel_id, fuel_limit } = req.body;
    try {
        const data = await db.any(` UPDATE vehicles SET 
                                        mark = ($1),
                                        fuel_id = ($2),
                                        fuel_limit = ($3)
                                    WHERE ID = ($4)`,   
        [mark, fuel_id, fuel_limit || null, id]);
        return res.status(200).json({
            data,
            message: 'invent updated'
        });
    } catch (e) {
        console.error(e);
        return next(e);
    }
};
