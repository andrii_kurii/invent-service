
const db = require('../../../connect');

function helper(limit, req, next, res) {
    const { vehicle_id, created_by, begin_date, end_date, event_id } = req.query;
    const { renewal, issuance, returned, writeOff } = req.query;
    db.any(`select (count(*) + ${limit - 1}) / ${limit} as end 
                from vehicle_actions
                inner join users on users.ID = vehicle_actions.created_by
                inner join vehicles on vehicles.ID = vehicle_actions.vehicle_id
                inner join fuel on fuel.ID = vehicles.fuel_id
            WHERE vehicle_actions.ID IS NOT NULL
                ${begin_date && end_date ? 'AND vehicle_actions.created_at::timestamp BETWEEN ($1)::timestamp AND ($2)::timestamp' : ''}
                ${event_id ? 'AND vehicle_actions.ID = ($5)' : ''}
                ${vehicle_id ? 'AND vehicle_actions.vehicle_id = ($3)' : ''}
                ${created_by ? 'AND vehicle_actions.created_by = ($4)' : ''}
                ${renewal || issuance || returned || writeOff ? ` AND action_type in (
                    0
                    ${renewal ? ", (select id from action_types where name = 'Renewal')" : ''}
                    ${issuance ? ", (select id from action_types where name = 'Issuance')" : ''}
                    ${returned ? ", (select id from action_types where name = 'Returned')" : ''}
                    ${writeOff ? ", (select id from action_types where name = 'Write off')" : ''}
                )` : ''}      
        `, [begin_date, end_date, vehicle_id, created_by, event_id])
        .then((data) => {
            const { end } = data[0];
            res(end);
        })
        .catch((err) => {
            return next(err);
        });
}

module.exports = function getVehiclesActions(req, res, next) {
    const { vehicle_id, created_by, begin_date, end_date, page, limit, fuel_id, worker_id, event_id } = req.query;
    const local_limit = limit || 999;
    const { renewal, issuance, returned, writeoff } = req.query;
    db.any(`select vehicle_actions.*, users.name as creator_name, fuel.name as fuel, vehicles.mark,
                    vehicles.numbers, action_types.name as action_name, workers.name as worker_name
                from vehicle_actions
                inner join users on users.ID = vehicle_actions.created_by
                inner join vehicles on vehicles.ID = vehicle_actions.vehicle_id
                inner join fuel on fuel.ID = vehicles.fuel_id
                inner join action_types on action_types.ID = vehicle_actions.action_type
                LEFT JOIN workers on workers.ID = vehicle_actions.worker_id
            WHERE vehicle_actions.ID IS NOT NULL
                ${begin_date && end_date ? 'AND vehicle_actions.created_at::timestamp BETWEEN ($1)::timestamp AND ($2)::timestamp' : ''}
                ${event_id ? 'AND vehicle_actions.ID = ($7)' : ''}
                ${vehicle_id ? 'AND vehicle_actions.vehicle_id = ($3)' : ''}
                ${fuel_id ? 'AND vehicles.fuel_id = ($4)' : ''}
                ${worker_id ? 'AND vehicle_actions.worker_id = ($5)' : ''}
                ${created_by ? 'AND vehicle_actions.created_by = ($6)' : ''}
                ${renewal || issuance || returned || writeoff ? ` AND action_type in (
                    0
                    ${renewal ? ", (select id from action_types where name = 'Renewal')" : ''}
                    ${issuance ? ", (select id from action_types where name = 'Issuance')" : ''}
                    ${returned ? ", (select id from action_types where name = 'Returned')" : ''}
                    ${writeoff ? ", (select id from action_types where name = 'Write off')" : ''}
                )` : ''}           
            ORDER BY vehicle_actions.created_at DESC
            LIMIT ${local_limit} OFFSET ${((page || 1) - 1) * local_limit}

        `, [begin_date, end_date, vehicle_id, fuel_id, worker_id, created_by, event_id])
        .then((data) => {
            helper(
                local_limit, req, next,
                (end) => {
                    res.status(200)
                        .json({
                            data,
                            message: 'received vehicles actions',
                            end
                        });
                }
            );
        })
        .catch((err) => {
            return next(err);
        });
};
