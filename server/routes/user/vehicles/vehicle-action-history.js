
const db = require('../../../connect');

module.exports = function vehicleActionsHistory(req, res, next) {
    const { id } = req.query;
    db.any(`SELECT vehicle_actions_history.*, fuel.name, vehicles.mark, vehicles.numbers, 
            action_types.name as action_name, workers.name as worker_name, users.name as creator_name 
                FROM vehicle_actions_history 
                INNER JOIN users on users.ID = vehicle_actions_history.created_by
                LEFT JOIN workers on workers.ID = vehicle_actions_history.worker_id
                LEFT JOIN action_types on action_types.ID = vehicle_actions_history.action_type
                inner join vehicles on vehicles.ID = vehicle_actions_history.vehicle_id
                inner join fuel on fuel.ID = vehicles.fuel_id
            where vehicle_action_id = ${id}
            ORDER BY vehicle_actions_history.created_at DESC`)
        .then((data) => {
            res.status(200)
                .json({
                    data,
                    message: 'received action history'
                });
        })
        .catch((err) => {
            return next(err);
        });
};
