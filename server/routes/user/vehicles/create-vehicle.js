const db = require('../../../connect');

module.exports = function createVehicle(req, res, next) {
    const { mark, numbers, fuel_id } = req.body;
    const { authData } = req.body;
    const { user } = authData;
    const { id } = user;
    db.any(
        'INSERT INTO vehicles (mark, numbers, fuel_id, created_by) VALUES (($1), ($2), ($3), ($4)) returning *',
        [mark, numbers, fuel_id, id]
    )
        .then(() => {
            res.status(200)
                .json({
                    message: 'vehicle created'
                });
        })
        .catch((err) => {
            return next(err);
        });
};
