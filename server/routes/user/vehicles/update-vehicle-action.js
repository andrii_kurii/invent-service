
const db = require('../../../connect');

module.exports = async function updateAction(req, res, next) {
    const { vehicle_action_id, vehicle_id, action_type, value, worket_id, authData } = req.body;
    const { user } = authData;
    const { id } = user;
    try {
        const data = await db.any(` UPDATE vehicle_actions SET 
                                        vehicle_id = ($1),
                                        action_type = ($2),
                                        value = ($3),
                                        worker_id = ($4),
                                        created_by = ($5)
                                    WHERE ID = ($6)`,   
        [vehicle_id, action_type, value, worket_id || null, id, vehicle_action_id]);
        res.status(200)
        .json({
            data,
            message: 'action updated'
        });
    } catch (e) {
        console.error(e);
        return next(e);
    }
};
