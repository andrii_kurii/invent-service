const db = require('../../../connect');

module.exports = function putRenewal(req, res, next) {
    const { vehicle_id, renewal } = req.body;
    const { authData } = req.body;
    const { user } = authData;
    const { id } = user;
    db.any(`
        INSERT INTO vehicle_actions (vehicle_id, value, action_type, created_by)
            VALUES ( ($1), ($2), (select id from action_types where name = 'Renewal'), ($3));
    `, [vehicle_id, renewal, id])
        .then(() => {
            res.status(200)
                .json({
                    message: 'vehicle renewal'
                });
        })
        .catch((err) => {
            return next(err);
        });
};
