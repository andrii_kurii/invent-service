const db = require('../../../connect');

module.exports = function putIssuance(req, res, next) {
    const { vehicle_id, issuance, worker_id } = req.body;
    const { authData } = req.body;
    const { user } = authData;
    const { id } = user;
    db.any(`
        INSERT INTO vehicle_actions (vehicle_id, value, action_type, worker_id, created_by)
            VALUES (($1), ($2), (select id from action_types where name = 'Issuance'), ($3), ($4));
    `, [vehicle_id, issuance, worker_id, id])
        .then(() => {
            res.status(200)
                .json({
                    message: 'vehicle issuance'
                });
        })
        .catch((err) => {
            return next(err);
        });
};
