const db = require('../../../connect');

module.exports = function putWriteoff(req, res, next) {
    const { vehicle_id, writeoff } = req.body;
    const { authData } = req.body;
    const { user } = authData;
    const { id } = user;
    db.any(`
        INSERT INTO vehicle_actions (vehicle_id, value, action_type, created_by)
            VALUES ( ($1), ($2), (select id from action_types where name = 'Write off'), ($3));
        `, [vehicle_id, writeoff, id])
        .then(() => {
            res.status(200)
                .json({
                    message: 'vehicle write-off'
                });
        })
        .catch((err) => {
            return next(err);
        });
};
