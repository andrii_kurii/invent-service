
const db = require('../../../connect');

module.exports = function getVehicles(req, res, next) {
    const { vehicle_id } = req.query;
    db.any(`select vehicles.numbers, vehicles.id
                from vehicles
                inner join users on users.ID = vehicles.created_by
                inner join fuel on fuel.ID = vehicles.fuel_id
            ${vehicle_id ? 'WHERE vehicles.id = ($1)' : ''}
            `, [vehicle_id])
        .then((data) => {
            res.status(200)
                .json({
                    data,
                    message: 'received vehicle'
                });
        })
        .catch((err) => {
            return next(err);
        });
};
