
const db = require('../../../connect');

module.exports = function getFuels(req, res, next) {
    db.any('select * from fuel')
        .then((data) => {
            res.status(200)
                .json({
                    data,
                    message: 'received fuel'
                });
        })
        .catch((err) => {
            return next(err);
        });
};
