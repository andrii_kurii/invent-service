
const jwt = require('jsonwebtoken');
const db = require('../../connect');

async function Login(req, res, next) {
    const { login } = req.body;
    try {
        const result = await db.any('select role, id, login from users WHERE login = ($1)', [login]);
        if (result.length) {
            const data = result[0];
            const token = await jwt.sign(
                { user: { id: data.id, login: data.login, role: data.role } },
                process.env.USERS_PRIVATE_TOKEN,
                { expiresIn: parseInt(process.env.USERS_TOKEN_TIMEOUT, 10) },
            );
            res.status(200).json({
                data: { token, ...data },
                message: 'success login'
            });
        } else {
            res.status(401).json({
                message: 'No such user'
            });
        }
    } catch (e) {
        return next(e);
    }
}

module.exports = {
    Login
};
