
const db = require('../../connect');

module.exports = function getUsers(req, res, next) {
    db.any('select id, login, name, role from users')
        .then((data) => {
            res.status(200)
                .json({
                    data,
                    message: 'received users'
                });
        })
        .catch((err) => {
            return next(err);
        });
};
