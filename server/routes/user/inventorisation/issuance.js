const db = require('../../../connect');

module.exports = async function putIssuance(req, res, next) {
    const { inventorisation_id, issuance, worker_id, authData: { user: { id } } } = req.body;
    try {
        const newIssuance = await db.any(`
            INSERT INTO invent_actions (inventorisation_id, value, action_type, worker_id, created_by)
                VALUES (
                    ($1),
                    ($2),
                    (select id from action_types where name = 'Issuance'),
                    ($3),
                    ($4)
                )
            RETURNING *`,
        [inventorisation_id, issuance, worker_id, id]);
        return res.status(200).json({
            message: 'success issuance',
            newIssuance
        });
    } catch (err) {
        return next(err);
    }
};
