
const db = require('../../../connect');

module.exports = async function updateAction(req, res, next) {
    try {
        const { action_type, inventorisation_id, worker_id, value, invent_action_id, authData } = req.body;
        const { user } = authData;
        const { id } = user;
        const updatedAction = await db.any(`
            UPDATE invent_actions SET 
                action_type= ($1),
                inventorisation_id = ($2),
                worker_id = ($3),
                value = ($4),
                created_by = ($5)
            WHERE ID = ($6)
            RETURNING *`,
        [action_type, inventorisation_id, worker_id, value, id, invent_action_id]);
        return res.status(200).json({
            message: 'action updated',
            updatedAction
        });
    } catch (e) {
        return next(e);
    }
};
