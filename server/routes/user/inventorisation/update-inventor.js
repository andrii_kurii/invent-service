
const db = require('../../../connect');

module.exports = async function updateInventor(req, res, next) {
    try {
        const { id, invent_id } = req.body;
        const updatedInventor = await db.any(`
            UPDATE inventorisation SET 
                invent_id = ($1)
            WHERE ID = ($2)
            RETURNING *`,
        [invent_id, id]);
        return res.status(200).json({
            message: 'invent updated',
            updatedInventor
        });
    } catch (e) {
        return next(e);
    }
};
