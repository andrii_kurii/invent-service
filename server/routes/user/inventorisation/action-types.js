
const db = require('../../../connect');

module.exports = async function getInvents(req, res, next) {
    try {
        const actionTypes = await db.any('select * from action_types');
        return res.status(200).json({
            actionTypes,
            message: 'received action types'
        });
    } catch (err) {
        return next(err);
    }
};
