
const db = require('../../../connect');

module.exports = async function getInventorisation(req, res, next) {
    try {
        const { id } = req.query;
        const inventorisation = await db.any(
            `select inventorisation.*, users.name as creator_name, invent.name as invent
                from inventorisation
                inner join invent on invent.ID = inventorisation.invent_id
                inner join users on users.id = inventorisation.created_by
            ${id ? 'WHERE inventorisation.id = ($1)' : ''}
            ORDER BY inventorisation.created_at DESC`,
            [id]
        );
        return res.status(200).json({
            inventorisation,
            message: 'received inventorisation'
        });
    } catch (err) {
        return next(err);
    }
};
