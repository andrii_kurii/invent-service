const db = require('../../../connect');

module.exports = async function putWriteoff(req, res, next) {
    try {
        const { inventorisation_id, writeoff, authData: { user: { id } } } = req.body;
        const newWriteOff = await db.any(`
            INSERT INTO invent_actions (inventorisation_id, value, action_type, created_by)
                VALUES (
                    ($1),
                    ($2),
                    (select id from action_types where name = 'Write off'),
                    ($3))
                RETURNING *`,
        [inventorisation_id, writeoff, id]);
        return res.status(200).json({
            message: 'inventor returned',
            newWriteOff
        });
    } catch (err) {
        return next(err);
    }
};
