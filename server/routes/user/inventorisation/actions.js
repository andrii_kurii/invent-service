
const db = require('../../../connect');

async function helper(limit, req, next) {
    try {
        const { id } = req.query;
        const { worker_id, created_by, begin_date, end_date, event_id } = req.query;
        const result = await db.any(
            `select (count(*) + ($1) - 1) / ($1) as end 
                FROM invent_actions 
                INNER JOIN users on users.ID = invent_actions.created_by
                LEFT JOIN workers on workers.ID = invent_actions.worker_id
                LEFT JOIN action_types on action_types.ID = invent_actions.action_type
                INNER JOIN inventorisation on inventorisation.ID = invent_actions.inventorisation_id
            WHERE invent_actions.ID IS NOT NULL
                ${event_id ? 'AND invent_actions.ID = ($2)' : ''}
                ${begin_date && end_date ? 'AND invent_actions.created_at::timestamp BETWEEN ($3)::timestamp AND ($4)::timestamp' : ''}
                ${id ? 'AND inventorisation_id = ($5)' : ''}
                ${worker_id ? 'AND invent_actions.worker_id = ($6)' : ''}
                ${created_by ? 'AND invent_actions.created_by = ($7)' : ''}
                ${req.query.Renewal || req.query.Returned || req.query.Issuance || req.query['Write off'] ? ` 
                AND action_type in (
                0
                ${req.query.Renewal ? ", (select id from action_types where name = 'Renewal')" : ''}
                ${req.query.Returned ? ", (select id from action_types where name = 'Returned')" : ''}
                ${req.query.Issuance ? ", (select id from action_types where name = 'Issuance')" : ''}
                ${req.query['Write off'] ? ", (select id from action_types where name = 'Write off')" : ''})` : ''}`,
            [limit, event_id, begin_date, end_date, id, worker_id, created_by]
        );
        return result;
    } catch (error) {
        return next(error);
    }
}

module.exports = async function getInventActions(req, res, next) {
    try {
        const { id } = req.query;
        const { worker_id, created_by, begin_date, end_date, page, limit, event_id } = req.query;
        const local_limit = limit || 999;
        const local_page = page || 1;
        const actions = await db.any(
            `SELECT invent_actions.*, inventorisation.name, invent.name as invent_name, action_types.name as action_name, 
            workers.name as worker_name, users.name as creator_name        
                FROM invent_actions 
                INNER JOIN users on users.ID = invent_actions.created_by
                LEFT JOIN workers on workers.ID = invent_actions.worker_id
                LEFT JOIN action_types on action_types.ID = invent_actions.action_type
                INNER JOIN inventorisation on inventorisation.ID = invent_actions.inventorisation_id
                INNER JOIN invent on invent.ID = inventorisation.invent_id
            WHERE invent_actions.ID IS NOT NULL
                ${event_id ? 'AND invent_actions.ID = ($1)' : ''}
                ${begin_date && end_date ? 'AND invent_actions.created_at::timestamp BETWEEN ($2)::timestamp AND ($3)::timestamp' : ''}
                ${id ? 'AND inventorisation_id = ($4)' : ''}
                ${worker_id ? 'AND invent_actions.worker_id = ($5)' : ''}
                ${created_by ? 'AND invent_actions.created_by = ($6)' : ''}
                ${req.query.Renewal || req.query.Returned || req.query.Issuance || req.query['Write off'] ? ` 
                AND action_type in (
                0
                ${req.query.Renewal ? ", (select id from action_types where name = 'Renewal')" : ''}
                ${req.query.Returned ? ", (select id from action_types where name = 'Returned')" : ''}
                ${req.query.Issuance ? ", (select id from action_types where name = 'Issuance')" : ''}
                ${req.query['Write off'] ? ", (select id from action_types where name = 'Write off')" : ''})` : ''}
            ORDER BY invent_actions.created_at DESC
            LIMIT ($7) OFFSET (($8) - 1) * ($7)`,
            [event_id, begin_date, end_date, id, worker_id, created_by, local_limit, local_page]
        );
        const result = await helper(local_limit, req, next);
        const { end } = result[0] || 0;

        return res.status(200).json({
            actions,
            end,
            message: 'received inventorisation actions'
        });
    } catch (err) {
        return next(err);
    }
};
