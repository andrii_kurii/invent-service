const db = require('../../../connect');

module.exports = async function createInventory(req, res, next) {
    try {
        const { name, invent_type, authData: { user: { id } } } = req.body;
        await db.any(
            `INSERT INTO inventorisation (name, invent_id, created_by)
                VALUES (($1), ($2), ($3))`,
            [name, invent_type, id]
        );
        return res.status(200).json({
            message: 'inventor created'
        });
    } catch (err) {
        return next(err);
    }
};
