
const db = require('../../../connect');

module.exports = async function inventActionsHistory(req, res, next) {
    try {
        const { id } = req.query;
        const actionHistory = await db.any(
            `SELECT invent_actions_history.*, inventorisation.name, invent.name as invent_name, 
            action_types.name as action_name, workers.name as worker_name, users.name as creator_name 

                FROM invent_actions_history 
                INNER JOIN users on users.ID = invent_actions_history.created_by
                LEFT JOIN workers on workers.ID = invent_actions_history.worker_id
                LEFT JOIN action_types on action_types.ID = invent_actions_history.action_type
                INNER JOIN inventorisation on inventorisation.ID = invent_actions_history.inventorisation_id
                INNER JOIN invent on invent.ID = inventorisation.invent_id
            where invent_action_id = ($1)
            ORDER BY invent_actions_history.created_at DESC`,
            [id]
        );

        return res.status(200).json({
            data: actionHistory,
            message: 'received action history'
        });
    } catch (err) {
        return next(err);
    }
};
