const db = require('../../../connect');

module.exports = async function putRenewal(req, res, next) {
    try {
        const { inventorisation_id, renewal, authData: { user: { id } } } = req.body;
        const newRenewal = await db.any(`
            INSERT INTO invent_actions (inventorisation_id, value, action_type, created_by)
                VALUES (
                    ($1),
                    ($2),
                    (select id from action_types where name = 'Renewal'),
                    ($3))
            RETURNING *`,
        [inventorisation_id, renewal, id]);
        return res.status(200).json({
            message: 'inventor renewal',
            newRenewal
        });
    } catch (err) {
        console.log(err);
        return next(err);
    }
};
