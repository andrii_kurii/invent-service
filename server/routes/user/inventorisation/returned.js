const db = require('../../../connect');

module.exports = async function putReturned(req, res, next) {
    try {
        const { inventorisation_id, returned, worker_id, authData: { user: { id } } } = req.body;
        const newReturning = await db.any(`
            INSERT INTO invent_actions (inventorisation_id, value, action_type, worker_id, created_by)
                VALUES (
                    ($1),
                    ($2),
                    (select id from action_types where name = 'Returned'),
                    ($3),
                    ($4)
                )
            RETURNING *`,
        [inventorisation_id, returned, worker_id, id]);
        return res.status(200).json({
            message: 'inventor returned',
            newReturning
        });
    } catch (err) {
        return next(err);
    }
};
