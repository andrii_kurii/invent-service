
const db = require('../../../connect');

module.exports = async function getInvents(req, res, next) {
    try {
        const invents = await db.any('select * from invent');
        return res.status(200).json({
            invents,
            message: 'received invents'
        });
    } catch (err) {
        return next(err);
    }
};
