
const db = require('../../../connect');

module.exports = async function createPurchases(req, res, next) {
    try {
        const { counterparty_id, price, bill_date, invoice, description, authData: { user: { id } } } = req.body;
        const newPurchases = await db.any(`
            INSERT INTO purchases (counterparty_id, price, bill_date, invoice, description, created_by)
                VALUES (($1), ($2), ($3), ($4), ($5), ($6))
            RETURNING *`,
        [counterparty_id, price, bill_date, invoice || null, description || null, id]);
        return res.status(200).json({
            message: 'purchase created',
            newPurchases
        });
    } catch (err) {
        return next(err);
    }
};
