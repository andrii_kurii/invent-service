const db = require('../../../connect');

module.exports = async function createCounterparty(req, res, next) {
    try {
        const { name, description, authData: { user: { id } } } = req.body;
        const newCounterparty = await db.any(`
            INSERT INTO counterparties (name, description, created_by)
                VALUES (($1), ($2), ($3))
            RETURNING *`,
        [name, description || null, id]);
        return res.status(200).json({
            message: 'counterparty created',
            newCounterparty
        });
    } catch (err) {
        return next(err);
    }
};
