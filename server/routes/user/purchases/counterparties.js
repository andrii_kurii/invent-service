
const db = require('../../../connect');

module.exports = async function getCounterparties(req, res, next) {
    try {
        const counterparties = await db.any(`
            SELECT counterparties.*, users.name as creator_name
                FROM counterparties
                INNER JOIN users on users.ID = counterparties.created_by
            ORDER BY counterparties.created_at DESC`);
        return res.status(200).json({
            message: 'received counterparties',
            counterparties
        });
    } catch (err) {
        return next(err);
    }
};
