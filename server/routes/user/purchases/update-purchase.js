
const db = require('../../../connect');

module.exports = async function updatePurchase(req, res, next) {
    const { id, counterparty_id, price, bill_date, invoice, description, authData } = req.body;
    const { user } = authData;
    try {
        const updatedPurchase = await db.any(`
            UPDATE purchases SET 
                counterparty_id= ($1),
                price = ($2),
                invoice = ($3),
                description = ($4),
                bill_date = ($5),
                created_by = ($6)
            WHERE ID = ($7)
            RETURNING *`,
        [counterparty_id, price, invoice, description, bill_date, user.id, id]);
        return res.json({
            message: 'action updated',
            updatedPurchase
        });
    } catch (e) {
        return next(e);
    }
};
