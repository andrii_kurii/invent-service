
const db = require('../../../connect');

module.exports = async function purchaseHistory(req, res, next) {
    try {
        const { id } = req.query;
        console.log(id);
        const history = await db.any(`
            SELECT purchases_history.*, users.name as creator_name, counterparties.name
                FROM purchases_history
                INNER JOIN counterparties ON counterparties.ID = purchases_history.counterparty_id
                INNER JOIN users ON users.ID = purchases_history.created_by
            WHERE purchase_id = ($1)
            ORDER BY purchases_history.created_at DESC`,
        [id]);
        return res.json({
            message: 'receive action history',
            history
        });
    } catch (error) {
        return next(error);
    }
};
