
const db = require('../../../connect');

async function helper(limit, invoice, req, next) {
    try {
        const { counterparty_id, created_by, begin_date, end_date, event_id } = req.query;
        const result = await db.any(`
                SELECT (count(*) + ${limit - 1}) / ${limit} as end 
                    FROM purchases
                    INNER JOIN counterparties on counterparties.ID = purchases.counterparty_id
                    INNER JOIN users on users.ID = purchases.created_by
                WHERE purchases.ID IS NOT NULL
                ${begin_date && end_date ? 'AND purchases.created_at::timestamp BETWEEN ($1)::timestamp AND ($2)::timestamp' : ''}
                ${counterparty_id ? 'AND purchases.counterparty_id = ($3)' : ''}
                ${invoice ? "AND purchases.invoice like '%'  || ($4) || '%'" : ''}
                ${created_by ? 'AND purchases.created_by = ($4)' : ''}
                ${event_id ? 'AND purchases.ID = ($5)' : ''}`,
        [begin_date, end_date, counterparty_id, invoice, created_by, event_id]);
        return result;
    } catch (error) {
        return next(error);
    }
}

async function getTotal(req, next) {
    try {
        const { counterparty_id, created_by, begin_date, end_date, page, limit, invoice } = req.query;
        const result = await db.any(`
            SELECT sum(price) AS total
                FROM purchases
            WHERE bill_date::timestamp BETWEEN ($1)::timestamp AND ($2)::timestamp
                ${counterparty_id ? 'AND counterparty_id = ($3)' : ''}
                ${created_by ? 'AND created_by = ($4)' : ''}     
                ${invoice ? "AND invoice LIKE '%'  || ($5) || '%'" : ''}`,
        [begin_date, end_date, counterparty_id, created_by, invoice]);
        const { total } = result[0] || 0;
        return total;
    } catch (error) {
        return next(error);
    }
}

module.exports = async function getPurchases(req, res, next) {
    try {
        const { counterparty_id, invoice, created_by, begin_date, end_date, page, limit, event_id } = req.query;
        const local_page = page || 1;
        const local_limit = limit || 999;
        const purchases = await db.any(`
            SELECT purchases.*, users.name as creator_name, counterparties.name
                FROM purchases
                INNER JOIN counterparties ON counterparties.ID = purchases.counterparty_id
                INNER JOIN users ON users.ID = purchases.created_by
            WHERE purchases.ID IS NOT NULL
                ${begin_date && end_date ? 'AND purchases.created_at::timestamp BETWEEN ($1)::timestamp AND ($2)::timestamp' : ''}
                ${counterparty_id ? 'AND purchases.counterparty_id = ($3)' : ''}
                ${invoice ? "AND invoice like '%'  || ($4) || '%'" : ''}
                ${created_by ? 'AND purchases.created_by = ($5)' : ''}            
                ${event_id ? 'AND purchases.ID = ($6)' : ''}
            ORDER BY purchases.created_at DESC
            LIMIT ($7) OFFSET (($8) - 1) * ($7)`,

        [begin_date, end_date, counterparty_id, invoice, created_by, event_id, local_limit, local_page]);

        const result = await helper(local_limit, invoice, req);
        const total = await getTotal(req, next);
        const { end } = result[0] || 0;
        return res.status(200).json({
            message: 'received inventorisation actions',
            purchases,
            end,
            total
        });
    } catch (error) {
        return next(error);
    }
};
