
const db = require('../../../connect');

module.exports = async function geTotal(req, res, next) {
    try {
        const { counterparty_id, created_by, begin_date, end_date, page, limit, invoice } = req.query;
        const result = await db.any(`
            SELECT sum(price) AS total
                FROM purchases
            WHERE bill_date::timestamp BETWEEN ($1)::timestamp AND ($2)::timestamp
                ${counterparty_id ? 'AND counterparty_id = ($3)' : ''}
                ${created_by ? 'AND created_by = ($4)' : ''}     
                ${invoice ? "AND invoice LIKE '%'  || ($5) || '%'" : ''}`,
        [begin_date, end_date, counterparty_id, created_by, invoice]);
        const { total } = result[0];
        return res.status(200).json({
            message: 'total receiver',
            total: total || 0
        });
    } catch (error) {
        return next(error);
    }
};
