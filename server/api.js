const express = require('express');
const jwt = require('jsonwebtoken');

const validate = require('express-validation');
const user = require('./routes/user');

const router = express.Router();

const queries = require('./routes/user/login');

const comparePasswords = require('./passwords/compare-passwords');

function verifyUserToken(req, res, next) {
    const bearerHeader = req.headers.authorization;
    if (typeof bearerHeader !== 'undefined') {
        const bearer = bearerHeader.split(' ');
        const bearerToken = bearer[1];
        req.token = bearerToken;
        jwt.verify(req.token, process.env.USERS_PRIVATE_TOKEN, (err, authData) => {
            if (err) {
                res.status(403).json({ message: 'bad user token' });
            } else {
                req.body.authData = authData;
                next();
            }
        });
    } else {
        res.status(401).json({ message: 'no token' });
    }
}

function ifAdmin(req, res, next) {
    // const { authData } = req.body;
    // const { user } = authData;
    // const { role } = user;
    // if (role === 'all' || role === 'read') {
    //     next();
    // } else {
    //     res.status(405)
    //         .json({
    //             message: 'no permissions'
    //         });
    // }
}

router.post('/login', comparePasswords, queries.Login); // TODO validation

router.use('/user', verifyUserToken, user); // admin

router.get('/favicon.ico', (req, res) => res.status(200));

module.exports = router;
