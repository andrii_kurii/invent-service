
const bcrypt = require('bcryptjs');


module.exports = function generateHash(password) {
    return new Promise((resolve, reject) => {
        bcrypt.genSalt(10)
            .then((salt) => {
                bcrypt.hash(password, salt)
                    .then((hash) => {
                        resolve(hash);
                    })
                    .catch((err) => {
                        reject(err);
                    });
            })
            .catch((err) => {
                reject(err);
            });
    });
};
