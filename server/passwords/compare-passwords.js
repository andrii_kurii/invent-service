
const bcrypt = require('bcryptjs');
const db = require('../connect');


module.exports = function comparePasswords(req, res, next) {
    const { login, password } = req.body;
    db.any(`SELECT password
            FROM users
            WHERE login = '${login}'`)
        .then((pass) => {
            if (!pass.length) {
                res.status(401)
                    .json({
                        message: 'no such user'
                    });
            } else {
                bcrypt.compare(password, pass[0].password)
                    .then((boolRes) => {
                        if (boolRes) {
                            next();
                        } else {
                            res.status(401)
                                .json({
                                    message: 'no such user'
                                });
                        }
                    });
            }
        })
        .catch((err) => {
            return next(err);
        });
};
