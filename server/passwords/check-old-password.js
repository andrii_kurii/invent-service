
const bcrypt = require('bcryptjs');
const db = require('../connect');


module.exports = function checkOldPassword(req, res, next) {
    const { old_password, authData } = req.body;
    db.any(`SELECT password
            FROM users
            WHERE ID = '${authData.user.id}'`)
        .then((pass) => {
            if (!pass.length) {
                res.status(401)
                    .json({
                        message: 'no such user'
                    });
            } else {
                bcrypt.compare(old_password, pass[0].password)
                    .then((boolRes) => {
                        if (boolRes) {
                            next();
                        } else {
                            res.status(401)
                                .json({
                                    message: 'no such user'
                                });
                        }
                    });
            }
        })
        .catch((err) => {
            return next(err);
        });
};
