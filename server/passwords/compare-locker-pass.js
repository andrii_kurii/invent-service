
const bcrypt = require('bcryptjs');
const db = require('../connect');


module.exports = function compareLockerPass(req, res, next) {
    const { locker, password } = req.body;
    db.any(`SELECT password
            FROM lockers
            WHERE outer_id = '${locker}'`)
        .then((pass) => {
            if (!pass.length) {
                res.status(401)
                    .json({
                        message: 'no such locker'
                    });
            } else {
                bcrypt.compare(password, pass[0].password)
                    .then((boolRes) => {
                        if (boolRes) {
                            next();
                        } else {
                            res.status(401)
                                .json({
                                    message: 'no such locker'
                                });
                        }
                    });
            }
        })
        .catch((err) => {
            return next(err);
        });
};
